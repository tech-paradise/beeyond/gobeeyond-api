const express = require('express');
const morgan = require('morgan');
const mysql = require('mysql');
const myConnection = require('express-myconnection');
const dotenv = require('dotenv');
const app = express();

//load env settings
dotenv.config();

//Settings
app.set('port', process.env.PORT || 3000);
app.set('json spaces',2);

//Middlewares
app.use(morgan('dev'));
app.use(myConnection(mysql, {
	host: process.env.DATABASE_HOST,
	user: process.env.DATABASE_USER,
	password: process.env.DATABASE_PASSWORD,
	port: process.env.DATABASE_PORT,
	database: process.env.DATABASE_NAME 
}, 'single'));


app.use(express.urlencoded({extended: false}));
app.use(express.json());

//Modulo Primer Sprint
app.use('/gobeeyond/api/client', require('./routes/cliente'));

//Modulo Segundo Sprint
app.use('/gobeeyond/api/login', require('./routes/login'));
app.use('/gobeeyond/api/card', require('./routes/tarjeta'));
app.use('/gobeeyond/api/wallet', require('./routes/cartera'));
app.use('/gobeeyond/api/transaction', require('./routes/transacciones'));
app.use('/gobeeyond/api/preference', require('./routes/preferencias'));

//Modulo Tercer Sprint
app.use('/gobeeyond/api/yields', require('./routes/rendimientos'));
app.use('/gobeeyond/api/provider', require('./routes/proveedor'));
app.use('/gobeeyond/api/product', require('./routes/producto'));
app.use('/gobeeyond/api/termsconditions', require('./routes/terminosycondiciones'));
app.use('/gobeeyond/api/paymentprovider', require('./routes/pagoproveedores'));
app.use('/gobeeyond/api/ticket', require('./routes/ticket'));
app.use('/gobeeyond/api/placeclaim', require('./routes/lugardereclamacion'));
app.use('/gobeeyond/api/controlproducts', require('./routes/controlproductos'));
app.use('/gobeeyond/api/branch', require('./routes/sucursales'));
app.use('/gobeeyond/api/game1vs1', require('./routes/juegounouno'));
app.use('/gobeeyond/api/game1vs1pers', require('./routes/juegounounopers'));
app.use('/gobeeyond/api/quinela', require('./routes/quinela'));
app.use('/gobeeyond/api/userquinela', require('./routes/usuarioquiniela'));

//Modulo Excel
app.use('/gobeeyond/api/excel', require('./routes/excel'));

//Modulo Openpay
app.use('/gobeeyond/api/openpay', require('./routes/openpayGo'));

//Modulo para juegos pendientes
app.use('/gobeeyond/api/gamesPends', require('./routes/juegosPendientes'));

//Modulo para agergar amigos
app.use('/gobeeyond/api/friends', require('./routes/amigos'));

//Start the server
app.listen(app.get('port'), () => {
	console.log(`Server on Port ${app.get('port')}`);
});
