const express = require('express');
const router = express.Router();
const _ = require('underscore');
const validate = require('./validaciones');

//Routes
router.post('/add', (req, res) => {
    const validate = validateRegister(req.body, res);
    console.log(req.body);
    if(validate){
        const data = req.body;
        req.getConnection((err, conn) => {
        	conn.query('SELECT Alias FROM cliente INNER JOIN login ON login.Id_login = cliente.Id_login where login.Correo_electronico = ? OR Alias = ?', [data.alias2, data.alias2], (err, aliasExist) => {
                console.log(aliasExist);
	            if (err) res.json(err)
	            else if(aliasExist.length > 0){
					data.status = "pendiente";
					conn.query('INSERT INTO listaAmigos set ?', [data], (err, friends) => {
	                    if (err){
	                        res.json(err);
	                    }else{
	                        res.json({status: "Registrado", message: "Se mandó solicitud de amistad con éxito"})
	                    }
	                });
	            }else{
	                res.status(400).json({status: "false", message: "El alias del contrincante no existe"});			
	            }
       		});
    	});
    }
});

router.get('/getFriends1/:alias', (req, res) => {
	const {alias} = req.params;
    req.getConnection((err, conn) => {
        conn.query('SELECT alias2 FROM listaAmigos WHERE alias1 = ? AND status = \'aceptado\' UNION SELECT alias1 FROM listaAmigos WHERE alias2 = ? AND status = \'aceptado\' ', [alias, alias] , (err, friends) => {
            console.log(friends);
            if (err) {
				console.log(err);
				res.json(err);
			}
            else if(friends.length > 0){
                res.json(friends);
            }
        });
    });
});

router.get('/getFriendsCount/:alias', (req, res) => {
	const {alias} = req.params;
    req.getConnection((err, conn) => {
        conn.query('SELECT COUNT(idlistaAmigos) as countFriends FROM listaAmigos WHERE alias2 = ? AND status = \'pendiente\' ', [alias] , (err, friendsCount) => {
            console.log(friendsCount[0]);
            if (err) {
				console.log(err);
				res.json(err);
			}
            else if(friendsCount.length > 0){
                res.json(friendsCount[0]);
            }
        });
    });
});

router.get('/getFriendsList/:alias', (req, res) => {
	const {alias} = req.params;
    req.getConnection((err, conn) => {
        conn.query('SELECT idlistaAmigos, alias1, status, fecha From listaAmigos WHERE alias2 = ? AND status = \'pendiente\'', [alias] , (err, friendsList) => {
            console.log(friendsList);
            if (err) {
				console.log(err);
				res.json(err);
			}
            else if(friendsList.length > 0){
                res.json(friendsList);
            }
        });
    });
});

router.put('/updateFriends/:id', (req, res) => {
	const {id} = req.params;
	const data = req.body;
	console.log(id);
	console.log(data);
	req.getConnection((err, conn) => {
	 	conn.query('UPDATE listaAmigos set ? WHERE idlistaAmigos = ?', [data, id], (err, lista) => {
		    if (err){
				res.json(err);	
				console.log(err);
			}else{
				res.json({statu: "Actualizado", message: "Se ha actualizado la solicitud de amistad"});
			}
		});
	});
});

/*router.get('/getFriends2/:alias', (req, res) => {
	const {alias} = req.params;
    req.getConnection((err, conn) => {
        conn.query('SELECT alias1 AS alias FROM listaAmigos WHERE alias2 = ?', [alias] , (err, friends2) => {
            console.log(friends2);
            if (err) res.json(err)
            else if(friends2.length > 0){
                res.json(friends2)                                        
            }
        });
    });
});*/

function validateRegister (data, res){
	let isValidate = validate.validateAlias(data.aliasJugador1);
	if(isValidate.validate){
		isValidate = validate.validateAlias(data.aliasJugador2);
		if(isValidate.validate){
			return isValidate.validate;
		}else{
			res.status(400).json(isValidate);	
		}
	}else{
		res.status(400).json(isValidate);
	}
}

module.exports = router;