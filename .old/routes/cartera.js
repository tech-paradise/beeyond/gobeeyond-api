const express = require('express');
const router = express.Router();
const _ = require('underscore');
const validate = require('./validaciones');

//Routes
router.put('/updateWallet/:alias', (req, res) => {
	const validated = validateSaldo(req.params, req.body, res);
	if (validated){
		const {alias} = req.params;
		const data = req.body;
		req.getConnection((err, conn) => {
			conn.query('SELECT cliente.Id_cliente, cartera.Saldo, COUNT(juegospendientes.idJuegoPendiente) as countGames FROM cliente INNER JOIN cartera ON cliente.Id_cliente = cartera.Id_cliente LEFT JOIN juegospendientes ON (juegospendientes.aliasJugador1 = cliente.Alias or juegospendientes.aliasJugador2 = cliente.Alias) and (aceptado = "pendiente") WHERE alias = ?', [alias], (err, cartera) => {
				if (err){
					console.log(err);
					res.json(err);	
				}else{
					data.Saldo = parseInt(cartera[0].Saldo) + parseInt(data.Saldo);
					console.log(data.Saldo);
					console.log(cartera);
					req.getConnection((err, conn) => {
						conn.query('UPDATE cartera set ? WHERE Id_cliente = ?', [data, cartera[0].Id_cliente], (err, clientes) => {
							if (err){
								console.log(err);
								res.json(err);	
							}else{
								res.json({status: "Actualizado", message: "Se ha actualizado el saldo correctamente", saldo: data.Saldo, games: cartera[0].countGames});
							}
						});
					});
				}
			});
		});
	}
});

router.put('/updateYields/:idUser', (req, res) => {
	const validated = validateRend(req.params, req.body, res);
	if (validated){
		const {idUser} = req.params;
		const data = req.body;
		req.getConnection((err, conn) => {
			conn.query('SELECT Rendimiento FROM cartera WHERE Id_cliente = ?', [idUser], (err, cartera) => {
				if (err){
					res.json(err);	
				}else{
					data.Rendimiento = parseInt(cartera[0].Rendimiento) + parseInt(data.Rendimiento);
					req.getConnection((err, conn) => {
						conn.query('UPDATE cartera set ? WHERE Id_cliente = ?', [data, idUser], (err, clientes) => {
							if (err){
								res.json(err);	
							}else{
								res.json({status: "Actualizado", message: "Se ha actualizado el rendimiento correctamente"});
							}
						});
					});
				}
			});
		});
	}
});

router.get('/getWalletId/:idCliente', (req, res) => {
	let {idCliente} = req.params
	req.getConnection((err, conn) => {
		conn.query('SELECT Id_cartera FROM cartera WHERE Id_cliente = ?', [idCliente], (err, cartera) => {
			if (err){
				res.json(err);
			}else{
				res.json(cartera[0].Id_cartera);
			}
		});
	});
});


function validateSaldo(id, data, res){
	console.log(id);
	console.log(data);
	let isValidate = validate.validateAlias(id.alias);
	if(isValidate.validate){
		isValidate = validate.validateMoney(data.Saldo);
		if(isValidate.validate){
			return isValidate.validate;
		}else{
			console.log(isValidate.message)
			res.status(400).json(isValidate.message);
		}
	}else{
		console.log(isValidate.message)
		res.status(400).json(isValidate.message);
	}
}

function validateRend(id, data, res){
	let isValidate = validate.validateId(id.idUser);
	if(isValidate.validate){
		isValidate = validate.validateMoney(data.Rendimiento);
		if(isValidate.validate){
			return isValidate.validate;
		}else{
			res.status(400).json(isValidate.message);
		}
	}else{
		res.status(400).json(isValidate.message);
	}
}

module.exports = router;

