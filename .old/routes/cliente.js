const express = require('express');
const router = express.Router();
const _ = require('underscore');
const validate = require('./validaciones');

var Openpay = require('openpay');
var openpay = new Openpay('mqachzg2e8ohex4xw5p3', 'sk_f4a49a7cd2c1446fa653937477f0c080', false);

router.post('/register', (req, res) => {
	const validated = validateRegister(req.body, res);
	if (validated){
		const {Nombre_cliente, Primer_apel, Segundo_apel, Alias, Fecha_nac, Tel_cel, Password, Pin, Correo_electronico} = req.body;
		const dataCliente = {Nombre_cliente, Primer_apel, Segundo_apel, Alias, Fecha_nac, Tel_cel};
		const dataLogin = {Password, Pin, Correo_electronico};
		req.getConnection((err, conn) => {
			conn.query('SELECT * FROM login WHERE Correo_electronico LIKE ?', [Correo_electronico], (err, login) => {
			if (err){
				res.json(err);
			}else{	
				if(login.length > 0){
					res.json({status: "Existe", message: "El correo ya se encuentra registrado"});
				}else{
					conn.query('SELECT * FROM cliente WHERE Alias LIKE ?', [Alias], (err, clientes) => {
						if (err){
							res.json(err);
						}else{
							if(clientes.length > 0){
								res.json({status: "Existe", message: "El alias ya esta registrado"});
							}else{
								req.getConnection((err, conn) => {
									dataLogin.Tipo_usuario = "Cliente";	
									conn.query('INSERT INTO login set ?', [dataLogin], (err, login) => {
										if (err){
											res.json(err);
										}else{
											req.getConnection((err, conn) => {
												conn.query('SELECT MAX(Id_login) AS max FROM login', (err, loginId) => {
													if (err){
														res.json(err);
													}else{
														dataCliente.Bloqueado = "No"	
														dataCliente.Fecha_alta = new Date();
														dataCliente.Comision = 10.0;
														dataCliente.Experiencia = 0;
														dataCliente.Nivel = 1;
														dataCliente.Status_nivel = "Principiante";
														dataCliente.Id_login = loginId[0].max;
														var isMayor = Math.abs(new Date() - new Date(Fecha_nac)) / 31536000000 ;
														if(isMayor >= 18.0) dataCliente.Menor = "No";
														else dataCliente.Menor = "Si";
														req.getConnection((err, conn) => {
															conn.query('INSERT INTO cliente set ?', [dataCliente], (err, clientes) => {
																if (err){
																	res.json(err);
																}else{
																	syncTables(req, res);
																	var customerRequest  = {
																		'external_id' : clientes.Id_cliente,
																		'name' : Nombre_cliente,
																		'last_name' : Primer_apel + " " + Segundo_apel,
																		'email' : Correo_electronico,
																		'phone_number' : Tel_cel
																	  }

																	openpay.customers.create(customerRequest, function(error, customer) {	
																		if(error){
																			res.status(400).json({status : "error", message : error })																	
																		}else{
																			console.log(customer);
																			req.getConnection((err, conn) => {
																				conn.query('UPDATE cliente set id_openpay = ? WHERE Alias = ?', [customer.id, Alias], (err, clientes) => {
																					if (err){
																						res.json(err);
																					}else{
																						res.json({status: "Registrado", message: "Se registro el usuario con exito"});																																		
																					}
																				});	
																			});																																		
																		}
																	});														
																}
															});	
														});
													}
												});	
											});
										}
									});
								});
							}
						}
					});
				}
			}});
		});
	}
});

router.put('/updateComplement/:alias', (req, res) => {
	console.log(req.body);
	const validated = validateComplement(req.params, req.body, res);
	if (validated){
		const {alias} = req.params;
		const data = req.body;
		req.getConnection((err, conn) => {
			conn.query('UPDATE cliente set ? WHERE Alias = ?', [data, alias], (err, clientes) => {
				if (err){
					res.json(err);	
				}else{
					res.json({status: "Actualizado", message: "Se ha actualizado el registro satisfactoriamente."});
				}
			});
		});
	}
});

router.put('/updateExp/:id', (req, res) => {
	let {Experiencia, Nivel} = req.body;
	if (Experiencia){
		const {id} = req.params;
		const data = req.body;
		req.getConnection((err, conn) => {
			conn.query('SELECT Experiencia FROM cliente WHERE Id_cliente = ?', [id], (err, clientes) => {
				if (err){
					res.json(err);	
				}else{
					data.Experiencia = parseInt(clientes[0].Experiencia) + parseInt(Experiencia);
					//Ver como sera las cuestiones de los niveles
					req.getConnection((err, conn) => {
						conn.query('UPDATE cliente set ? WHERE Id_cliente = ?', [data, id], (err, clientes) => {
							if (err){
								res.json(err);	
							}else{
							res.json({status: "Actualizado", message: "Se ha actualizado la experiencia correctamente"});
							}
						});
					});
				}
			});
		});
	}else{
		res.status(400).json({status: "Error", message: "No se ha ingresado la experiencia"});
	}
});

router.put('/blockUser/:id', (req, res) => {
	const {id} = req.params;
	const {Bloqueado} = req.body;
	if(Bloqueado === "Si"){
		const data = req.body;
		req.getConnection((err, conn) => {
			conn.query('UPDATE cliente set ? WHERE Id_cliente = ?', [data, id], (err, clientes) => {
				if (err){
					res.json(err);	
				}else{
					res.json({status: "Actualizado", message: "Se ha actualizado correctamente"});
				}
			});
		});
	}else{
		res.json({status: "Error", message: "Entrada Invalida"});
	}
});

router.get('/getUserId/:email', (req, res) => {
	req.getConnection((err, conn) => {
		const {email} = req.params
		conn.query('SELECT cliente.Id_cliente, cliente.Nombre_cliente, cliente.Primer_apel, cliente.Segundo_apel, cliente.Alias, cliente.Tel_cel, login.Correo_electronico, cliente.id_openpay, cartera.saldo, COUNT(juegospendientes.idJuegoPendiente) as countGames FROM cliente INNER JOIN login ON login.Id_login = cliente.Id_login INNER JOIN cartera ON cartera.Id_cliente = cliente.Id_cliente LEFT JOIN juegospendientes ON (juegospendientes.aliasJugador1 = cliente.Alias or juegospendientes.aliasJugador2 = cliente.Alias) and (aceptado = "pendiente") WHERE Correo_electronico = ?', [email], (err, cliente) => {
			if (err){
				res.json(err);
			}else{
				console.log(cliente[0]);
				res.json(cliente[0]);
			}
		});
	});
});

router.get('/getUserData/:alias', (req, res) => {
	req.getConnection((err, conn) => {
		const {alias} = req.params
		conn.query('SELECT cliente.Nombre_cliente, cliente.Primer_apel, cliente.Segundo_apel, cliente.Alias, cliente.Fecha_nac, cliente.Tel_cel, login.Correo_electronico, cliente.Ciudad, cliente.Estado, cliente.Pais, cliente.Genero, cliente.Ocupacion FROM cliente inner join login on cliente.Id_login = login.Id_login WHERE cliente.Alias = ?', [alias], (err, data) => {
			if (err){
				res.json(err);
			}else{
				console.log(data);
				res.json(data[0]);
			}
		});
	});
});

router.get('/getMounts/:alias', (req, res) => {
	req.getConnection((err, conn) => {
		const {alias} = req.params
		conn.query('SELECT Saldo, Rendimiento FROM cartera INNER JOIN cliente ON cliente.Id_cliente = cartera.Id_cliente WHERE cliente.Alias = ?', [alias], (err, mounts) => {
			if (err){
				res.json(err);
			}else{
				res.json(mounts[0]);
			}
		});
	});
});

router.get('/getPin/:alias', (req, res) => {
	req.getConnection((err, conn) => {
		const {alias} = req.params;
		conn.query('SELECT Pin FROM login INNER JOIN cliente ON login.Id_login = cliente.Id_login WHERE Alias = ?', [alias], (err, data) => {
			if (err){
				res.json(err);
			}else{
				if(data.length > 0){				
					res.json({status: "Encontrado", Pin: data[0].Pin});							
				}else{
					res.json({status: "No Encontrado", Pin: ""});							
				}
			}
		});
	});
});



router.get('/aliasExist/:alias', (req, res) => {
	req.getConnection((err, conn) => {
		const {alias} = req.params
		const dataCliente = {Nombre_cliente, Primer_apel, Segundo_apel, Alias, Fecha_nac, Tel_cel};
		conn.query('SELECT Alias FROM cliente WHERE Alias = ?', [alias], (err, data) => {
			if (err){
				res.json(err);
			}else{
				if(data.length > 0){
					req.getConnection((err, conn) => {
						conn.query('INSERT INTO juegospendientes set ?', [dataCliente], (err, clientes) => {
							if (err){
								res.json(err);
							}else{
								res.json({status: "Registrado", message: "Se registro el usuario con exito"});
							}
						});		
					});
				}
			}
		});
	});
});

router.post('/validate', (req, res) => {
	const validated = validateRegister(req.body, res);
	if(validated) {
		const {Nombre_cliente, Primer_apel, Segundo_apel, Alias, Fecha_nac, Tel_cel, Password, Pin, Correo_electronico} = req.body;
		const dataCliente = {Nombre_cliente, Primer_apel, Segundo_apel, Alias, Fecha_nac, Tel_cel};
		const dataLogin = {Password, Pin, Correo_electronico};
		req.getConnection((err, conn) => {
			conn.query('SELECT * FROM login WHERE Correo_electronico LIKE ?', [Correo_electronico], (err, login) => {
			if (err){
				res.json(err);
			}else{	
				if(login.length > 0){
					res.json({status: "Existe", message: "El correo ya se encuentra registrado"});
				}else{
					conn.query('SELECT * FROM cliente WHERE Alias LIKE ?', [Alias], (err, clientes) => {
						if (err){
							res.json(err);
						}else{
							if(clientes.length > 0){
								res.json({status: "Existe", message: "El alias ya esta registrado"});
							}else{
								res.json({status: "Correcto", message: "El alias ya esta registrado"});
							}
						}
					});
				}
			}});
		});
	}
});


function syncTables(req, res){
	req.getConnection((err, conn) => {
		conn.query('SELECT MAX(Id_cliente) AS maxCliente FROM cliente', (err, clienteId) => {
			req.getConnection((err, conn) => {
				conn.query('SELECT Id_cliente FROM cartera WHERE Id_cliente LIKE ?', [clienteId[0].maxCliente], (err, clientes2) => {
					if (err){
						res.json(err);
					}else{
						if(clientes2.length > 0){
							res.json({status: "Existe", message: "Cartera ya existente"});
						}else{
							req.getConnection((err, conn) => {
								const dataCartera = {"Saldo" : 0, "Id_cliente" : clienteId[0].maxCliente, "Rendimiento" : 0};
								conn.query('INSERT INTO cartera set ?', [dataCartera], (err, cartera) => {
									if (err){
										res.json(err);
									}else{
										req.getConnection((err, conn) => {
											conn.query('SELECT MAX(Id_cartera) AS maxCartera FROM cartera', (err, carteraId) => {
												req.getConnection((err, conn) => {
													if (err){
														res.json(err);
													}else{
														req.getConnection((err, conn) => {
															const dataTransaccion1 = {"Concepto" : "Deposito", "Monto" : 0, "Fecha_transc" : new Date(), "Id_cartera" : carteraId[0].maxCartera};
															conn.query('INSERT INTO transacciones set ?', [dataTransaccion1], (err, transacciones) => {
																if (err){
																	res.json(err);
																}else{
																	req.getConnection((err, conn) => {
																		const dataTransaccion2 = {"Concepto" : "Ganancia", "Monto" : 0, "Fecha_transc" : new Date(), "Id_cartera" : carteraId[0].maxCartera};
																		conn.query('INSERT INTO transacciones set ?', [dataTransaccion2], (err, transacciones2) => {
																			if (err){
																				res.json(err);
																			}
																		});	
																	});
																}
															});	
														});
													}
												});
											});
										});										
									}
								});	
							});
						}
					}
				});		
			});
		});
	});
}

function validateRegister (data, res){
	let isValidate = validate.validateName(data.Nombre_cliente);
	if(isValidate.validate){
		isValidate = validate.validateName(data.Primer_apel);
		if(isValidate.validate){
			isValidate = validate.validateName(data.Segundo_apel);
			if(isValidate.validate){
				isValidate = validate.validateAlias(data.Alias);
				if(isValidate.validate){
					isValidate = validate.validateFecha(data.Fecha_nac);
					if(isValidate.validate){
						isValidate = validate.validateEmail(data.Correo_electronico);
						if(isValidate.validate){
							isValidate = validate.validateCel(data.Tel_cel);
							if(isValidate.validate){
								isValidate = validate.validateAlias(data.Alias);
								if(isValidate.validate){
									isValidate = validate.validatePassword(data.Password);
									if(isValidate.validate){
										isValidate = validate.validatePin(data.Pin);
										if(isValidate.validate){
											return isValidate.validate;
										}else{
											res.status(400).json(isValidate);	
										}
									}else{
										res.status(400).json(isValidate);
									}
								}else{
									res.status(400).json(isValidate);
								}
							}else{
								res.status(400).json(isValidate);
							}
						}else{
							res.status(400).json(isValidate);
						}
					}else{
						res.status(400).json(isValidate);
					}
				}else{
					res.status(400).json(isValidate);	
				}
			}else{
				res.status(400).json(isValidate);	
			}
		}else{
			res.status(400).json(isValidate);
		}
	}else{
		res.status(400).json(isValidate);
	}
}

function validateComplement (id, data, res){
	let isValidate = validate.validateAlias(id.alias);
	if(isValidate.validate){
		isValidate = validate.validateCel(data.Tel_cel);
		if(isValidate.validate){
			isValidate = validate.validateName(data.Ciudad);
			if(isValidate.validate){
				isValidate = validate.validateName(data.Estado);
				if(isValidate.validate){			
					isValidate = validate.validateName(data.Ocupacion);
					if(isValidate.validate){
						return isValidate.validate;
					}else{
						res.status(400).json(isValidate);
					}						
				}else{
					res.status(400).json(isValidate);
				}
			}else{
				res.status(400).json(isValidate);	
			}
		}else{
			res.status(400).json(isValidate);	
		}
	}else{
		res.status(400).json(isValidate);
	}
}



module.exports = router;
