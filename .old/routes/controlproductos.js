const express = require('express');
const router = express.Router();	
const _ = require('underscore');

//Routes
router.post('/register', (req, res) => {
	const {Codigo, QRuri, Id_reclamacion} = req.body;
	if(Codigo && QRuri && Id_reclamacion){
		const data = req.body;
		data.Fecha = new Date();
		req.getConnection((err, conn) => {
			conn.query('INSERT INTO controlproductosganados set ?', [data], (err, controlproductos) => {
				if (err){
					res.json(err);
				}else{
					res.json({status: "Registrado", message: "Se registro el producto ganado con exito"});
				}
			});
		});	
	}else{
		res.status(500).json({status: "Error", message: "No se ha completado algunos campos"});
	}
});	

router.get('/getControlProductId/:email', (req, res) => {
	let {email} = req.params
	req.getConnection((err, conn) => {
		conn.query('SELECT Id_login FROM login WHERE Correo_electronico = ?', [email], (err, login) => {
			if (err){
				res.json(err);
			}else{
				req.getConnection((err, conn) => {
					conn.query('SELECT Id_proveedor FROM proveedores WHERE Id_login = ?', [login[0].Id_login], (err, proveedores) => {
						if (err){
							res.json(err);
						}else{
							req.getConnection((err, conn) => {
								conn.query('SELECT Id_reclamacion FROM lugarreclamacion WHERE Id_proveedor = ?', [proveedores[0].Id_proveedor], (err, lugarreclamacion) => {
									if (err){
										res.json(err);
									}else{
										req.getConnection((err, conn) => {
											conn.query('SELECT Id_control_producto FROM controlproductosganados WHERE Id_reclamacion = ?', [lugarreclamacion[0].Id_reclamacion], (err, controlproductos) => {
												if (err){
													res.json(err);
												}else{
													res.json(controlproductos);
												}
											});
										});
									}
								});
							});
						}
					});
				});
			}
		});
	});
});

module.exports = router;