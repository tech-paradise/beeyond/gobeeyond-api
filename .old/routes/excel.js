const express = require('express');
const router = express.Router();
const jsonexport = require('jsonexport');
const _ = require('underscore');

//Routes
router.get('/getClients', (req, res) => {
	req.getConnection((err, conn) => {
		conn.query('SELECT * FROM cliente', (err, cliente) => {
			if (err){
				res.json(err);
			}else{
				jsonexport(cliente, function(err, csv){
    				if(err){
    					res.send(err);
    				}else{
    					res.set('Content-Type', 'application/octet-stream');
    					res.attachment('ReportAllClients.csv');
						res.send(csv);
   					}
				});
			}
		});
	});
});

router.get('/getWallets', (req, res) => {
	req.getConnection((err, conn) => {
		conn.query('SELECT * FROM cartera', (err, cartera) => {
			if (err){
				res.json(err);		
			}else{
				jsonexport(cartera, function(err, csv){
    				if(err){
    					res.send(err);
    				}else{
    					res.set('Content-Type', 'application/octet-stream');
    					res.attachment('ReportAllWallets.csv');
						res.send(csv);
   					}
				});
			}
		});
	});
});

router.get('/getLogins', (req, res) => {
	req.getConnection((err, conn) => {
		conn.query('SELECT * FROM login', (err, login) => {
			if (err){
				res.json(err);		
			}else{
				jsonexport(login, function(err, csv){
    				if(err){
    					res.send(err);
    				}else{
    					res.set('Content-Type', 'application/octet-stream');
    					res.attachment('ReportAllLogins.csv');
						res.send(csv);
   					}
				});
			}
		});
	});
});

router.get('/getPreferences', (req, res) => {
	req.getConnection((err, conn) => {
		conn.query('SELECT * FROM preferencias', (err, preferencias) => {
			if (err){
				res.json(err);
			}else{
				jsonexport(preferencias, function(err, csv){
    				if(err){
    					res.send(err);
    				}else{
    					res.set('Content-Type', 'application/octet-stream');
    					res.attachment('ReportAllPreferences.csv');
						res.send(csv);
   					}
				});
			}
		});
	});
});

router.get('/getCards', (req, res) => {
	req.getConnection((err, conn) => {
		conn.query('SELECT * FROM tarjetas', (err, tarjetas) => {
			if (err){
				res.json(err);
			}else{
				jsonexport(tarjetas, function(err, csv){
    				if(err){
    					res.send(err);
    				}else{
    					res.set('Content-Type', 'application/octet-stream');
    					res.attachment('ReportAllCards.csv');
						res.send(csv);
   					}
				});
			}
		});
	});
});

router.get('/getTransactions', (req, res) => {
	req.getConnection((err, conn) => {
		conn.query('SELECT * FROM transacciones', (err, transacciones) => {
			if (err){
				res.json(err);
			}else{
				jsonexport(transacciones, function(err, csv){
    				if(err){
    					res.send(err);
    				}else{
    					res.set('Content-Type', 'application/octet-stream');
    					res.attachment('ReportAllTransaction.csv');
						res.send(csv);
   					}
				});
			}
		});
	});
});

router.get('/getProducts', (req, res) => {
	req.getConnection((err, conn) => {
		conn.query('SELECT * FROM producto', (err, productos) => {
			if (err){
				res.json(err);
			}else{
				jsonexport(productos, function(err, csv){
    				if(err){
    					res.send(err);
    				}else{
    					res.set('Content-Type', 'application/octet-stream');
    					res.attachment('ReportAllProducts.csv');
						res.send(csv);
   					}
				});
			}
		});
	});
});

router.get('/getProvider', (req, res) => {
	req.getConnection((err, conn) => {
		conn.query('SELECT * FROM proveedores', (err, proveedores) => {
			if (err){
				res.json(err);
			}else{
				jsonexport(proveedores, function(err, csv){
    				if(err){
    					res.send(err);
    				}else{
    					res.set('Content-Type', 'application/octet-stream');
    					res.attachment('ReportAllProviders.csv');
						res.send(csv);
   					}
				});
			}
		});
	});
});

router.get('/getYield', (req, res) => {
	req.getConnection((err, conn) => {
		conn.query('SELECT * FROM rendimiento', (err, rendimiento) => {
			if (err){
				res.json(err);
			}else{
				jsonexport(rendimiento, function(err, csv){
    				if(err){
    					res.send(err);
    				}else{
    					res.set('Content-Type', 'application/octet-stream');
    					res.attachment('ReportAllYields.csv');
						res.send(csv);
   					}
				});
			}
		});
	});
});

router.get('/getTermCondition', (req, res) => {
	req.getConnection((err, conn) => {
		conn.query('SELECT * FROM terminosycondiciones', (err, terminosycondiciones) => {
			if (err){
				res.json(err);
			}else{
				jsonexport(terminosycondiciones, function(err, csv){
    				if(err){
    					res.send(err);
    				}else{
    					res.set('Content-Type', 'application/octet-stream');
    					res.attachment('ReportAllTermConditions.csv');
						res.send(csv);
   					}
				});
			}
		});
	});
});

router.get('/getPaymentProvider', (req, res) => {
	req.getConnection((err, conn) => {
		conn.query('SELECT * FROM pago_proveedores', (err, pago_proveedores) => {
			if (err){
				res.json(err);
			}else{
				jsonexport(pago_proveedores, function(err, csv){
    				if(err){
    					res.send(err);
    				}else{
    					res.set('Content-Type', 'application/octet-stream');
    					res.attachment('ReportAllPaymentProvider.csv');
						res.send(csv);
   					}
				});
			}
		});
	});
});

router.get('/getTicket', (req, res) => {
	req.getConnection((err, conn) => {
		conn.query('SELECT * FROM ticket', (err, ticket) => {
			if (err){
				res.json(err);
			}else{
				jsonexport(ticket, function(err, csv){
    				if(err){
    					res.send(err);
    				}else{
    					res.set('Content-Type', 'application/octet-stream');
    					res.attachment('ReportAllTickets.csv');
						res.send(csv);
   					}
				});
			}
		});
	});
});

router.get('/getPlaceClaim', (req, res) => {
	req.getConnection((err, conn) => {
		conn.query('SELECT * FROM lugarreclamacion', (err, lugarreclamacion) => {
			if (err){
				res.json(err);
			}else{
				jsonexport(lugarreclamacion, function(err, csv){
    				if(err){
    					res.send(err);
    				}else{
    					res.set('Content-Type', 'application/octet-stream');
    					res.attachment('ReportAllPlaceClaim.csv');
						res.send(csv);
   					}
				});
			}
		});
	});
});

router.get('/getControlProducts', (req, res) => {
	req.getConnection((err, conn) => {
		conn.query('SELECT * FROM controlproductosganados', (err, controlproductosganados) => {
			if (err){
				res.json(err);
			}else{
				jsonexport(controlproductosganados, function(err, csv){
    				if(err){
    					res.send(err);
    				}else{
    					res.set('Content-Type', 'application/octet-stream');
    					res.attachment('ReportAllControlProducts.csv');
						res.send(csv);
   					}
				});
			}
		});
	});
});

router.get('/getBranch', (req, res) => {
	req.getConnection((err, conn) => {
		conn.query('SELECT * FROM sucursales', (err, sucursales) => {
			if (err){
				res.json(err);
			}else{
				jsonexport(sucursales, function(err, csv){
    				if(err){
    					res.send(err);
    				}else{
    					res.set('Content-Type', 'application/octet-stream');
    					res.attachment('ReportAllBranchs.csv');
						res.send(csv);
   					}
				});
			}
		});
	});
});

router.get('/getGame1vs1', (req, res) => {
	req.getConnection((err, conn) => {
		conn.query('SELECT * FROM juego_uno_uno', (err, juego_uno_uno) => {
			if (err){
				res.json(err);
			}else{
				jsonexport(juego_uno_uno, function(err, csv){
    				if(err){
    					res.send(err);
    				}else{
    					res.set('Content-Type', 'application/octet-stream');
    					res.attachment('ReportAllGames1vs1.csv');
						res.send(csv);
   					}
				});
			}
		});
	});
});

router.get('/getGame1vs1Pers', (req, res) => {
	req.getConnection((err, conn) => {
		conn.query('SELECT * FROM juegopersonalizado', (err, juegopersonalizado) => {
			if (err){
				res.json(err);
			}else{
				jsonexport(juegopersonalizado, function(err, csv){
    				if(err){
    					res.send(err);
    				}else{
    					res.set('Content-Type', 'application/octet-stream');
    					res.attachment('ReportAllGames1vs1Pers.csv');
						res.send(csv);
   					}
				});
			}
		});
	});
});


router.get('/getQuinelas', (req, res) => {
	req.getConnection((err, conn) => {
		conn.query('SELECT * FROM quiniela', (err, quiniela) => {
			if (err){
				res.json(err);
			}else{
				jsonexport(quiniela, function(err, csv){
    				if(err){
    					res.send(err);
    				}else{
    					res.set('Content-Type', 'application/octet-stream');
    					res.attachment('ReportAllQuinelas.csv');
						res.send(csv);
   					}
				});
			}
		});
	});
});

router.get('/getUserQuinelas', (req, res) => {
	req.getConnection((err, conn) => {
		conn.query('SELECT * FROM usuarioquiniela', (err, usuarioquiniela) => {
			if (err){
				res.json(err);
			}else{
				jsonexport(usuarioquiniela, function(err, csv){
    				if(err){
    					res.send(err);
    				}else{
    					res.set('Content-Type', 'application/octet-stream');
    					res.attachment('ReportAllUserQuinelas.csv');
						res.send(csv);
   					}
				});
			}
		});
	});
});

router.get('/getReport', (req, res) => {
	req.getConnection((err, conn) => {
		conn.query('SELECT cliente.Id_cliente AS SKU, cliente.Nombre_cliente AS Nombre, cliente.Primer_apel AS PrimerApellido, cliente.Segundo_apel AS SegundoApellido, cliente.Tel_cel AS Celular, login.Correo_electronico AS Email, CAST(Fecha_alta AS DATE) AS FechaRegistro, CAST(Fecha_alta AS TIME) AS HoraRegistro, FLOOR(DATEDIFF (NOW(), cliente.Fecha_nac)/365) AS Edad, cliente.Genero, cliente.Ocupacion, cliente.Pais, cliente.Estado, Fondeos, FondeosRetiros, cartera.Saldo, (FondeosRetiros/Fondeos*100) AS PorcentajeRetiros, Ganancias, GananciasRetiros, cartera.Rendimiento, (GananciasRetiros/Ganancias*100) AS PorcentajeRendimientos, (Ganancias - Fondeos) AS UtilidadesNetas, ((Ganancias - Fondeos)/Fondeos*100) AS PorcentajeUtilidadesVSFondeos, (Ganancias * 0.10) AS ComisionesGeneradas, PromedioFondeo, PromedioGanancia, ActividadTransaccionX30, ActividadTransaccionX60, ActividadTransaccionX90 FROM cliente INNER JOIN login ON cliente.Id_login = login.Id_login INNER JOIN cartera ON cliente.Id_cliente = cartera.Id_cliente INNER JOIN (SELECT count(CASE WHEN DATEDIFF(NOW(), transacciones.Fecha_transc) <= 30 then 0 end) -2 AS ActividadTransaccionX30, count(CASE WHEN DATEDIFF(NOW(), transacciones.Fecha_transc) <= 60 then 0 end) -2 AS ActividadTransaccionX60, count(CASE WHEN DATEDIFF(NOW(), transacciones.Fecha_transc) <= 90 then 0 end) -2 AS ActividadTransaccionX90, SUM(CASE WHEN transacciones.Concepto = \'Deposito\' THEN transacciones.Monto ELSE 0 END) AS Fondeos, SUM(CASE WHEN transacciones.Concepto = \'Retiro\' THEN transacciones.Monto ELSE 0 END) AS FondeosRetiros, SUM(CASE WHEN transacciones.Concepto = \'Ganancia\' THEN transacciones.Monto ELSE 0 END) AS Ganancias, SUM(CASE WHEN transacciones.Concepto = \'Rendimiento\' THEN transacciones.Monto ELSE 0 END) AS GananciasRetiros, aux.Id_cartera, aux.Id_cliente FROM transacciones INNER JOIN (SELECT cartera.Id_cartera, cliente.Id_cliente FROM cartera INNER JOIN cliente ON cliente.Id_cliente = cartera.Id_cliente) AS aux ON transacciones.Id_cartera = aux.Id_cartera Where transacciones.Concepto = \'Deposito\' OR transacciones.Concepto = \'Retiro\' OR transacciones.Concepto = \'Ganancia\' OR transacciones.Concepto = \'Rendimiento\' GROUP BY aux.Id_cartera) AS aux2 ON cliente.Id_cliente = aux2.Id_cliente INNER JOIN  (SELECT AVG(Monto) AS PromedioFondeo, aux4.Id_cartera, aux4.Id_cliente FROM transacciones INNER JOIN (SELECT cartera.Id_cartera, cliente.Id_cliente FROM cartera INNER JOIN cliente ON cliente.Id_cliente = cartera.Id_cliente) AS aux4 ON transacciones.Id_cartera = aux4.Id_cartera Where transacciones.Concepto = \'Deposito\' GROUP BY aux4.Id_cartera) AS aux3 ON cliente.Id_cliente = aux3.Id_cliente INNER JOIN (SELECT AVG(Monto) AS PromedioGanancia, aux5.Id_cartera, aux5.Id_cliente FROM transacciones INNER JOIN (SELECT cartera.Id_cartera, cliente.Id_cliente FROM cartera INNER JOIN cliente ON cliente.Id_cliente = cartera.Id_cliente) AS aux5 ON transacciones.Id_cartera = aux5.Id_cartera Where transacciones.Concepto = \'Ganancia\' GROUP BY aux5.Id_cartera) AS aux6 ON cliente.Id_cliente = aux6.Id_cliente', (err, cliente) => {
			if (err){
				res.json(err);
			}else{				
				jsonexport(cliente, function(err, csv){
    				if(err){
    					res.send(err);
    				}else{
    					res.set('Content-Type', 'application/octet-stream');
    					res.attachment('ReportAnalytics.csv');
						res.send(csv);
   					}
				});
			}
		});
	});
});

module.exports = router;