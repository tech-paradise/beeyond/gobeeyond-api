const express = require('express');
const router = express.Router();
const _ = require('underscore');
const validate = require('./validaciones');

//Routes
router.post('/games', (req, res) => {
    const validate = validateRegister(req.body, res);
    console.log(req.body);
    if(validate){
        const data = req.body;
        req.getConnection((err, conn) => {
        	conn.query('SELECT Alias FROM cliente WHERE Alias = ?', [data.aliasJugador2], (err, aliasExist) => {
	            if (err) res.json(err)
	            else if(aliasExist.length > 0){
					console.log(aliasExist);
	            	data.estadoJuego = "validacion";
					data.aceptado = "pendiente";
					data.reclamado = "no"
					conn.query('INSERT INTO juegospendientes set ?', [data], (err, games) => {
	                    if (err){
							console.log(err);
	                        res.json(err);
	                    }else{
	                        res.json({status: "Registrado", message: "Se registró el juego pendiente con éxito"})
	                    }
	                });
	            }else{
	                res.json({status: "false", message: "El alias del contrincante no existe"});			
	            }
       		});
    	});
    }
});

router.get('/gamesPends/:alias', (req, res) => {
	const {alias} = req.params;
	req.getConnection((err, conn) => {
		conn.query('SELECT aliasJugador1, aliasJugador2, estadoJuego, aceptado, idJuegoPendiente, mount, idPartido, ganador, reclamado, equipoLocal, equipoVisitante, equipoJugador1, equipoJugador2 FROM juegospendientes WHERE (aliasJugador1 = ? OR aliasJugador2 = ?)  AND aceptado = \'pendiente\'', [alias, alias], (err, data) => {
		    if (err) {
				res.json(err)
			}
		    else if(data.length > 0){
				console.log("EN GAME PENDS");
				console.log(data);
		    	res.json(data);
		    }else{
		        res.json({status: "false", message: "No hay juegos pendientes"});			
		    }
		});
	});
});

router.get('/inGames/:alias', (req, res) => {
	const {alias} = req.params;
	req.getConnection((err, conn) => {
		conn.query('SELECT aliasJugador1, aliasJugador2, estadoJuego, aceptado, idJuegoPendiente, mount, idPartido, ganador, reclamado, equipoLocal, equipoVisitante, equipoJugador1, equipoJugador2 FROM juegospendientes WHERE (aliasJugador1 = ? OR aliasJugador2 = ?)  AND estadoJuego = \'En Juego\'', [alias, alias], (err, inData) => {
		    if (err) {
				res.json(err)
			}
		    else if(inData.length > 0){
				console.log(inData);
		    	res.json(inData);
		    }else{
		        res.json({status: "false", message: "No hay juegos pendientes"});			
		    }
		});
	});
});

router.get('/endGames/:alias', (req, res) => {
	const {alias} = req.params;
	req.getConnection((err, conn) => {
		conn.query('SELECT aliasJugador1, aliasJugador2, estadoJuego, aceptado, idJuegoPendiente, mount, idPartido, ganador, reclamado, equipoLocal, equipoVisitante, equipoJugador1, equipoJugador2 FROM juegospendientes WHERE (aliasJugador1 = ? OR aliasJugador2 = ?)  AND estadoJuego = \'Finalizado\'', [alias, alias], (err, endData) => {
		    if (err) {
				console.log(err);
				res.json(err);
			}
		    else if(endData.length > 0){
				console.log(endData);
		    	res.json(endData);
		    }else{
		        res.json({status: "false", message: "No hay juegos pendientes"});			
		    }
		});
	});
});
	
router.put('/updateGamePends/:id', (req, res) => {
	const {id} = req.params;
	const data = req.body;
	console.log(id);
	console.log(data);
	req.getConnection((err, conn) => {
	 	conn.query('UPDATE juegospendientes set ? WHERE idJuegoPendiente = ?', [data, id], (err, gamesUp) => {
		    if (err){
				res.json(err);	
				console.log(err);
			}else{
				req.getConnection((err, conn) => {
					conn.query('SELECT mount FROM juegospendientes WHERE idJuegoPendiente = ?', [id], (err, mountRes) => {
					   if (err){
						   res.json(err);	
						   console.log(err);
					   }else{
						   console.log(mountRes);
						   res.json({status: "Actualizado", message: "Se ha actualizado el juego correctamente", mount: mountRes[0].mount});
					   }
				   });
			   });
			}
		});
	});
});

router.put('/updateWinner/:id', (req, res) => {
	console.log(req.body);
	const {Id_cliente, Saldo, reclamado, idJuegoPendiente} = req.body;
	const dataRend = {Saldo};
	const dataGame = {reclamado};
	req.getConnection((err, conn) => {
		conn.query('SELECT Saldo FROM cartera WHERE Id_cliente = ?', [Id_cliente], (err, cartera) => {
			if (err){
				res.json(err);	
			}else{
				console.log(cartera);
				dataRend.Saldo = parseInt(cartera[0].Saldo) + parseInt(dataRend.Saldo);
				console.log("Saldo total: "+dataRend.Saldo);
				req.getConnection((err, conn) => {
					conn.query('UPDATE cartera set ? WHERE Id_cliente = ?', [dataRend, Id_cliente], (err, clientes) => {
						if (err){
							res.json(err);	
						}else{
							req.getConnection((err, conn) => {
								conn.query('UPDATE juegospendientes set ? WHERE idJuegoPendiente = ?', [dataGame, idJuegoPendiente], (err, cli) => {
									if (err){
										res.json(err);	
									}else{
										console.log("Saldo total2: "+dataRend.Saldo);
										res.json({status: "Actualizado", message: "Se ha actualizado el rendimiento correctamente", saldo: dataRend.Saldo});
									}
								});
							});
						}
					});
				});
			}
		});
	});
});

router.get('/getEndGame/:alias', (req, res) => {
	const {alias} = req.params;
	req.getConnection((err, conn) => {
		conn.query('SELECT idPartido FROM juegospendientes WHERE ganador IS NULL and idPartido IS NOT NULL', (err, endDataGames) => {
		    if (err) {
				res.json(err)
				console.log(err);
			}
		    else if(endDataGames.length > 0){
				console.log(endDataGames);
		    	res.json(endDataGames);
		    }
		});
	});
});

router.put('/updateEndMatch/:idPartido', (req, res) => {
	const {idPartido} = req.params;
	const data = req.body;
	req.getConnection((err, conn) => {
		conn.query('UPDATE juegospendientes set ? WHERE idPartido = ?', [data, idPartido], (err, updateEndMatch) => {
		    if (err){
				console.log(err)
				res.json(err);	
			}else{
				res.json({status: "Actualizado", message: "Se ha actualizado el resultado del partido"});
			}
		});
	});
});

function validateRegister (data, res){
	let isValidate = validate.validateAlias(data.aliasJugador1);
	if(isValidate.validate){
		isValidate = validate.validateAlias(data.aliasJugador2);
		if(isValidate.validate){
			return isValidate.validate;
		}else{
			res.status(400).json(isValidate);	
		}
	}else{
		res.status(400).json(isValidate);
	}
}

module.exports = router;