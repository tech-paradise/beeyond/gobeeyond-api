const express = require('express');
const router = express.Router();
const _ = require('underscore');

router.post('/register', (req, res) => {
	const {monto, id_cliente1, id_cliente2, id_control_producto} = req.body;
	if (monto && id_cliente1 && id_cliente2 && id_control_producto){
		let data = req.body;
		data.fecha_uno_uno = new Date();
		req.getConnection((err, conn) => {
			conn.query('INSERT INTO juego_uno_uno set ?', [data], (err, juegounouno) => {
				if(err){
					res.json(err);
				}else{
					res.json({status: "Registrado", message: "Se registro la partida con exito"});
				}
			});
		});
	}else{
		res.status(500).json({status: "Error", message: "No se ha completado algun campo"});
	}
});

router.put('/updateResult/:id', (req, res) => {
	const {id} = req.params;
	const {resultado, ganador, perdedor} = req.body;
	if (resultado && ganador && perdedor){
		const data = req.body;
		req.getConnection((err, conn) => {
			conn.query('UPDATE juego_uno_uno set ? WHERE Id_juego_uno_uno = ?', [data, id], (err, juegounouno) => {
				if (err){
					res.json(err);	
				}else{
					res.json({status: "Actualizado", message: "Se ha actualizado los datos del juego uno a uno"});
				}
			});
		});
	}else{
		res.status(500).json({status: "Error", message: "No se ha completado algun campo"});
	}
});

router.get('/getGameId/:id1/:id2', (req, res) => {
	let {id1, id2} = req.params
	req.getConnection((err, conn) => {
		conn.query('SELECT Id_juego_uno_uno FROM juego_uno_uno WHERE id_cliente1 = ? AND id_cliente2 = ?', [id1, id2], (err, juegounouno) => {
			if (err){
				res.json(err);
			}else{
				res.json(juegounouno);
			}
		});
	});
});

module.exports = router;