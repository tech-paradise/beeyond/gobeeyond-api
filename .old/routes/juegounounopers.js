const express = require('express');
const router = express.Router();
const _ = require('underscore');

router.post('/register', (req, res) => {
	const {monto, id_cliente_1, id_cliente_2, id_cliente_3, id_control_producto} = req.body;
	if (monto && id_cliente_1 && id_cliente_2 && id_cliente_3 && id_control_producto){
		let data = req.body;
		data.fecha_personalizado = new Date();
		req.getConnection((err, conn) => {
			conn.query('INSERT INTO juegopersonalizado set ?', [data], (err, juegopersonalizado) => {
				if(err){
					res.json(err);
				}else{
					res.json({status: "Registrado", message: "Se registro la partida con exito"});
				}
			});
		});
	}else{
		res.status(500).json({status: "Error", message: "No se ha completado algun campo"});
	}
});

router.put('/updateResult/:id', (req, res) => {
	const {id} = req.params;
	const {resultado, ganador, perdedor} = req.body;
	if (resultado && ganador && perdedor){
		const data = req.body;
		req.getConnection((err, conn) => {
			conn.query('UPDATE juegopersonalizado set ? WHERE id_juego_personalizado = ?', [data, id], (err, juegopersonalizado) => {
				if (err){
					res.json(err);	
				}else{
					res.json({status: "Actualizado", message: "Se ha actualizado los datos del juego uno a uno"});
				}
			});
		});
	}else{
		res.status(500).json({status: "Error", message: "No se ha completado algun campo"});
	}
});

router.get('/getGameId/:id1/:id2', (req, res) => {
	let {id1, id2} = req.params
	req.getConnection((err, conn) => {
		conn.query('SELECT id_juego_personalizado FROM juegopersonalizado WHERE id_cliente_1 = ? AND id_cliente_2 = ?', [id1, id2], (err, juegopersonalizado) => {
			if (err){
				res.json(err);
			}else{
				res.json(juegopersonalizado);
			}
		});
	});
});

module.exports = router;