const express = require('express');
const router = express.Router();
const _ = require('underscore');

//Routes
router.post('/checkLogin', (req, res) => {
	const {Correo_electronico, Password} = req.body;
	req.getConnection((err, conn) => {
		conn.query('SELECT * FROM login WHERE Correo_electronico = ? AND Password = ?', [Correo_electronico, Password], (err, login) => {
			if (err){
				res.json(err);
			}else{
				if(login.length > 0){
					conn.query('SELECT cliente.Id_cliente, cliente.Nombre_cliente, cliente.Primer_apel, cliente.Segundo_apel, cliente.Alias, cliente.Tel_cel, login.Correo_electronico, cliente.id_openpay, cartera.saldo, COUNT(juegospendientes.idJuegoPendiente) as countGames FROM login INNER JOIN cliente ON login.Id_login = cliente.Id_login INNER JOIN cartera ON cartera.Id_cliente = cliente.Id_cliente LEFT JOIN juegospendientes ON (juegospendientes.aliasJugador1 = cliente.Alias or juegospendientes.aliasJugador2 = cliente.Alias) and (aceptado = "pendiente") WHERE Correo_electronico = ?', [Correo_electronico], (err, login) => {
						if (err){
							res.json(err);
						}else{	
							console.log(login);
							res.json({status: "Correcto", message: "Bienvenido", data: login});						
						}
					});
				}else{
					res.json({status: "Incorrecto", message: "Correo y/o Contraseña inválidas"});
				}
			}
		});
	});
});

module.exports = router;
