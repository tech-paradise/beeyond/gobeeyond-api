const express = require('express');
const router = express.Router();
const _ = require('underscore');

//Routes
router.post('/register', (req, res) => {
	const {Ubicacion_tienda_local, Ciudad_o_Estado, Codigo_postal, Id_proveedor} = req.body;
	if(Ubicacion_tienda_local && Ciudad_o_Estado && Codigo_postal && Id_proveedor){
		const data = req.body;
		req.getConnection((err, conn) => {
			conn.query('INSERT INTO lugarreclamacion set ?', [data], (err, lugarreclamacion) => {
				if (err){
					res.json(err);
				}else{
					res.json({status: "Registrado", message: "Se registro el lugar de reclamacion con exito"});
				}
			});
		});	
	}else{
		res.status(500).json({status: "Error", message: "No se ha completado algunos campos"});
	}
});

router.put('/updatePlaceClaim/:id', (req, res) => {
	const {id} = req.params;
	const {Ubicacion_tienda_local, Ciudad_o_Estado, Codigo_postal} = req.body;
	if(Ubicacion_tienda_local || Ciudad_o_Estado || Codigo_postal){
		const data = req.body;
		req.getConnection((err, conn) => {
			conn.query('UPDATE lugarreclamacion set ? WHERE Id_reclamacion = ?', [data, id], (err, lugarreclamacion) => {
				if (err){
					res.json(err);	
				}else{
					res.json({status: "Actualizado", message: "Se ha actualizado el lugar de reclamacion satisfactoriamente."});
				}
			});
		});
	}else{
		res.status(500).json({status: "Error", message: "No se ha completado algun campo"});
	}
});

router.delete('/deletePlaceClaim/:id', (req, res) => {
	const {id} = req.params;
	req.getConnection((err, conn) => {
		conn.query('DELETE FROM lugarreclamacion WHERE Id_reclamacion = ?', [id], (err, lugarreclamacion) => {
			if (err){
				res.json(err);					
			}else{
				res.json({status: "Eliminado", message: "Se ha eliminado el lugar de reclamacion"});
			}
		});
	});
});

router.get('/getPlaceClaimId/:email', (req, res) => {
	let {email} = req.params
	req.getConnection((err, conn) => {
		conn.query('SELECT Id_login FROM login WHERE Correo_electronico = ?', [email], (err, login) => {
			if (err){
				res.json(err);
			}else{
				req.getConnection((err, conn) => {
					conn.query('SELECT Id_proveedor FROM proveedores WHERE Id_login = ?', [login[0].Id_login], (err, proveedores) => {
						if (err){
							res.json(err);
						}else{
							req.getConnection((err, conn) => {
								conn.query('SELECT Id_reclamacion FROM lugarreclamacion WHERE Id_proveedor = ?', [proveedores[0].Id_proveedor], (err, lugarreclamacion) => {
									if (err){
										res.json(err);
									}else{
										res.json(lugarreclamacion);
									}
								});
							});
						}
					});
				});
			}
		});
	});
});


module.exports = router;