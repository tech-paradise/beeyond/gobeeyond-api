const express = require('express');
const router = express.Router();
const _ = require('underscore');
const validate = require('./validaciones');

var Openpay = require('openpay');
var openpay = new Openpay('mqachzg2e8ohex4xw5p3', 'sk_f4a49a7cd2c1446fa653937477f0c080', false);


//Routes
router.post('/sendPayment', (req, res) => {
  console.log(req.body);
  const {source_id, amount, description, device_session_id, order_id, name, phone_number, email, customerId} = req.body;
  
  var chargeRequest = {
    'source_id' : source_id,
    'method' : 'card',
    'amount' : amount,
    'currency' : 'MXN',
    'description' : description,
    'order_id' : order_id,
    'device_session_id' : device_session_id
  }

 openpay.customers.charges.create(customerId, chargeRequest, function(error, charge) {
   if(error){
     res.status(400).json({status : "error", message : error })
     console.log(error);
   }else{
     console.log(charge);
     res.json({status : "Registrado", message : "charge" });
   }
 });
});

module.exports = router;

