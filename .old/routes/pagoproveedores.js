const express = require('express');
const router = express.Router();
const _ = require('underscore');

//Routes
router.post('/register', (req, res) => {
	const {Numero_cuenta_proveedor, Numero_tarjeta_debito, Cuenta_clabe, Id_proveedor} = req.body;
	if (Numero_cuenta_proveedor && Numero_tarjeta_debito && Cuenta_clabe && Id_proveedor){
		let data = req.body;
		req.getConnection((err, conn) => {
			conn.query('SELECT Numero_cuenta_proveedor FROM pago_proveedores WHERE Numero_cuenta_proveedor = ?', [Numero_cuenta_proveedor], (err, pago_proveedores) => {
				if (err){
					res.json(err);
				}else{
					if (pago_proveedores.length > 0){
						res.json({status: "Existe", message: "El numero de cuenta ya se encuentra registrado"});
					}else{
						req.getConnection((err, conn) => {
							conn.query('INSERT INTO pago_proveedores set ?', [data], (err, clientes) => {
								if (err){
									res.json(err);
								}else{
									res.json({status: "Registrado", message: "Se registro el pago de proveedores con exito"});
								}
							});
						});
					}
				}
			});
		});
	}else{
		res.status(500).json({status: "Error", message: "No se ha completado algun campo"});
	}
});

router.put('/updatePaymentProvider/:id', (req, res) => {
	const {id} = req.params;
	const {Numero_cuenta_proveedor, Numero_tarjeta_debito, Cuenta_clabe} = req.body;
	if (Numero_cuenta_proveedor || Numero_tarjeta_debito || Cuenta_clabe){
		const data = req.body;
		req.getConnection((err, conn) => {
			conn.query('UPDATE pago_proveedores set ? WHERE Id_pago_proveedores = ?', [data, id], (err, pago_proveedores) => {
				if (err){
					res.json(err);	
				}else{
					res.json({status: "Actualizado", message: "Se ha actualizado los datos del pago de proveedores"});
				}
			});
		});
	}else{
		res.status(500).json({status: "Error", message: "No se ha completado algun campo"});
	}
});

router.delete('/deletePaymentProvider/:id', (req, res) => {
	const {id} = req.params;
	req.getConnection((err, conn) => {
		conn.query('DELETE FROM pago_proveedores WHERE Id_pago_proveedores = ?', [id], (err, pago_proveedores) => {
			if (err){
				res.json(err);					
			}else{
				res.json({status: "Eliminado", message: "Se ha eliminado el pago del proveedor"});
			}
		});
	});
});

module.exports = router;