const express = require('express');
const router = express.Router();
const _ = require('underscore');

//Routes
router.post('/register', (req, res) => {
	const {Deporte, Deportista, Id_cliente} = req.body;
	let data = req.body;
	if (Deporte && Deportista && Id_cliente){
		req.getConnection((err, conn) => {
			conn.query('INSERT INTO preferencias set ?', [data], (err, preferencias) => {
				if (err){
					res.json(err);
				}else{
					res.json({status: "Registrado", message: "Se ha registrado su preferencias"});
				}
			});
		});
	}else{
		res.status(500).json({status: "Error", message: "No se ha completado algun campo"});
	}
});

router.put('/updatePreference/:id', (req, res) => {
	const {id} = req.params;
	const {Deporte, Deportista} = req.body;
	if (Deporte || Deportista){
		const data = req.body;
		req.getConnection((err, conn) => {
			conn.query('UPDATE preferencias set ? WHERE Id_preferencias_equip = ?', [data, id], (err, preferencias) => {
				if (err){
					res.json(err);	
				}else{
					res.json({status: "Actualizado", message: "Se ha actualizado su preferencias"});
				}
			});
		});
	}else{
		res.status(500).json({status: "Error", message: "No se ha completado algun campo"});
	}
});

module.exports = router;

