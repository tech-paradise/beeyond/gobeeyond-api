const express = require('express');
const router = express.Router();
const _ = require('underscore');

router.post('/register', (req, res) => {
	const {Numero_articulo, Descripcion, Precio, Disponibilidad, Id_proveedor} = req.body;
	if(Numero_articulo && Descripcion && Precio && Disponibilidad && Id_proveedor){
		const data = req.body;
		req.getConnection((err, conn) => {
			conn.query('SELECT * FROM producto WHERE Numero_articulo LIKE ?', [Numero_articulo], (err, producto) => {
				if(err){
					res.json(err)
				}else{
					if(producto.length > 0){
						res.json({status: "Existe", message: "El producto ya esta registrado"});
					}else{
						req.getConnection((err, conn) => {
							conn.query('INSERT INTO producto set ?', [data], (err, productos) => {
								if (err){
									res.json(err);
								}else{
									res.json({status: "Registrado", message: "Se registro el producto con exito"});
								}
							});
						});
					}
				}
			});
		});
	}else{
		res.status(500).json({status: "Error", message: "No se ha completado algunos campos"});
	}
});

router.put('/updateProduct/:id', (req, res) => {
	const {id} = req.params;
	const {Numero_articulo, Descripcion, Precio, Disponibilidad} = req.body;
	if (Numero_articulo || Descripcion || Precio || Disponibilidad){
		const data = req.body;
		req.getConnection((err, conn) => {
			conn.query('UPDATE producto set ? WHERE Id_producto = ?', [data, id], (err, producto) => {
				if (err){
					res.json(err);	
				}else{
					res.json({status: "Actualizado", message: "Se ha actualizado el producto satisfactoriamente."});
				}
			});
		});
	}else{
		res.status(500).json({status: "Error", message: "No se ha completado algun campo"});
	}
});

router.delete('/deleteProduct/:id', (req, res) => {
	const {id} = req.params;
	req.getConnection((err, conn) => {
		conn.query('DELETE FROM producto WHERE Id_producto = ?', [id], (err, producto) => {
			if (err){
				res.json(err);					
			}else{
				res.json({status: "Eliminado", message: "Se ha eliminado el producto"});
			}
		});
	});
});

router.get('/getProductId/:Num_art', (req, res) => {
	let {Num_art} = req.params
	req.getConnection((err, conn) => {
		conn.query('SELECT Id_producto FROM producto WHERE Numero_articulo = ?', [Num_art], (err, numArt) => {
			if (err){
				res.json(err);
			}else{
				res.json(numArt[0].Id_producto);
			}
		});
	});
});

module.exports = router;