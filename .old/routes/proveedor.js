const express = require('express');
const router = express.Router();
const _ = require('underscore');

//Routes
router.post('/register', (req, res) => {
	const {Nombre_proveedor, Direccion_proveedor, telefono, codigo_postal, Correo_electronico, Password, Pin} = req.body;
	if(Nombre_proveedor && Direccion_proveedor && telefono && codigo_postal && Correo_electronico && Password && Pin){
		let dataProveedor = {Nombre_proveedor, Direccion_proveedor, telefono, codigo_postal};
		let dataLogin = {Password, Pin, Correo_electronico};
		req.getConnection((err, conn) => {
			conn.query('SELECT * FROM login WHERE Correo_electronico like ?', [Correo_electronico], (err, login) => {
				if (err){
					res.json(err);
				}else{
					if(login.length > 0){
						res.json({status: "Existe", message: "El correo ya esta registrado"});
					}else{
						req.getConnection((err, conn) => {
							dataLogin.Tipo_usuario = "Proveedor";
							conn.query('INSERT INTO login set ?', [dataLogin], (err, login) => {
								if(err){
									res.json(err);
								}else{
									req.getConnection((err, conn) => {
										conn.query('SELECT MAX(Id_login) AS max FROM login', (err, loginId) => {
											if(err){
												res.json(err);
											}else{
												dataProveedor.Bloqueado = "No";
												dataProveedor.Fecha_registro = new Date();
												dataProveedor.Id_login = loginId[0].max;
												req.getConnection((err, conn) => {
													conn.query('INSERT INTO proveedores set ?', [dataProveedor], (err, proveedores) => {
														if (err){
															res.json(err);
														}else{
															res.json({status: "Registrado", message: "Se ha registrado al proveedor"});
														}
													});
												});
											}
										});
									});
								}
							});
						});
					}
				}
			});
		});
	}else{
		res.status(500).json({status: "Error", message: "No se ha completado algunos campos"});
	}
});

router.put('/updateComplement/:id', (req, res) => {
	const {id} = req.params;
	const {Direccion_proveedor, telefono, codigo_postal} = req.body;
	if (Direccion_proveedor || telefono || codigo_postal){
		const data = req.body;
		req.getConnection((err, conn) => {
			conn.query('UPDATE proveedores set ? WHERE Id_proveedor = ?', [data, id], (err, proveedores) => {
				if (err){
					res.json(err);	
				}else{
					res.json({status: "Actualizado", message: "Se ha actualizado el proveedor satisfactoriamente."});
				}
			});
		});
	}else{
		res.status(500).json({status: "Error", message: "No se ha completado algun campo"});
	}
});

router.put('/blockProvider/:id', (req, res) => {
	const {id} = req.params;
	const {Bloqueado} = req.body;
	if(Bloqueado == 'Si'){
		const data = req.body;
		req.getConnection((err, conn) => {
			conn.query('UPDATE proveedores set ? WHERE Id_proveedor = ?', [data, id], (err, clientes) => {
				if (err){
					res.json(err);	
				}else{
					res.json({status: "Actualizado", message: "Se ha actualizado correctamente"});
				}
			});
		});
	}
});

router.get('/getProviderId/:email', (req, res) => {
	let {email} = req.params
	req.getConnection((err, conn) => {
		conn.query('SELECT Id_login FROM login WHERE Correo_electronico = ?', [email], (err, login) => {
			if (err){
				res.json(err);
			}else{
				req.getConnection((err, conn) => {
					conn.query('SELECT Id_proveedor FROM proveedores WHERE Id_login = ?', [login[0].Id_login], (err, proveedor) => {
						if (err){
							res.json(err);
						}else{
							console.log(proveedor);
							res.json(proveedor);
						}
					});
				});
			}
		});
	});
});

module.exports = router;

						