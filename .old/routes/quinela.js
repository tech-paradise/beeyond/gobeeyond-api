const express = require('express');
const router = express.Router();
const _ = require('underscore');

router.post('/register', (req, res) => {
	const {Contador_apuestas_A, Contador_apuestas_B, Contador_apuestas_C, Total_A, Total_B, Total_C} = req.body;
	if (Contador_apuestas_A && Contador_apuestas_B && Contador_apuestas_C && Total_A && Total_B && Total_C){
		let data = req.body;	
		req.getConnection((err, conn) => {
			conn.query('INSERT INTO quiniela set ?', [data], (err, quiniela) => {
				if(err){
					res.json(err);
				}else{
					res.json({status: "Registrado", message: "Se registro la quiniela con exito"});
				}
			});
		});
	}else{
		res.status(500).json({status: "Error", message: "No se ha completado algun campo"});
	}
});

router.put('/updateResult/:id', (req, res) => {
	const {id} = req.params;
	const {Contador_apuestas_A, Contador_apuestas_B, Contador_apuestas_C, Total_A, Total_B, Total_C} = req.body;
	if (Contador_apuestas_A || Contador_apuestas_B || Contador_apuestas_C || Total_A || Total_B || Total_C){
		const data = req.body;
		req.getConnection((err, conn) => {
			conn.query('UPDATE quiniela set ? WHERE Id_quiniela = ?', [data, id], (err, juegounouno) => {
				if (err){
					res.json(err);	
				}else{
					res.json({status: "Actualizado", message: "Se ha actualizado los datos de la quiniela"});
				}
			});
		});
	}else{
		res.status(500).json({status: "Error", message: "No se ha completado algun campo"});
	}
});

router.get('/getGameId/:id1', (req, res) => {
	let {id1} = req.params
	req.getConnection((err, conn) => {
		conn.query('SELECT Id_quiniela FROM quiniela WHERE Id_usuarioQuiniela = ?', [id1], (err, quiniela) => {
			if (err){
				res.json(err);
			}else{
				res.json(quiniela);
			}
		});
	});
});

module.exports = router;