const express = require('express');
const router = express.Router();
const _ = require('underscore');

router.post('/register', (req, res) => {
	const {Rendimiento, Id_cliente} = req.body;
	let data = req.body;
	if (Rendimiento && Id_cliente){
		req.getConnection((err, conn) => {
			conn.query('INSERT INTO rendimiento set ?', [data], (err, rendimiento) => {
				if (err){
					res.json(err);
				}else{
					res.json({status: "Registrado", message: "Se ha registrado el rendimiento"});
				}
			});
		});
	}else{
		res.status(500).json({status: "Error", message: "No se ha completado algun campo"});
	}
});

router.put('/updateYields/:idUser', (req, res) => {
	const {idUser} = req.params;
	const {Rendimiento} = req.body;
	if (Rendimiento){
		const data = req.body;
		req.getConnection((err, conn) => {
			conn.query('SELECT Rendimiento FROM rendimiento WHERE Id_cliente = ?', [idUser], (err, rendimiento) => {
				if (err){
					res.json(err);	
				}else{
					data.Rendimiento = parseInt(rendimiento[0].Rendimiento) + parseInt(Rendimiento);
					req.getConnection((err, conn) => {
						conn.query('UPDATE rendimiento set ? WHERE Id_cliente = ?', [data, idUser], (err, rendimiento) => {
							if (err){
								res.json(err);	
							}else{
							res.json({status: "Actualizado", message: "Se ha actualizado el rendimiento correctamente"});
							}
						});
					});
				}
			});
		});
	}else{
		res.status(500).json({status: "Error", message: "No se ha ingresado la experiencia"});
	}
});

module.exports = router;