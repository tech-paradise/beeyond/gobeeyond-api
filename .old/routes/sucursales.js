const express = require('express');
const router = express.Router();
const _ = require('underscore');

//Routes
router.post('/register', (req, res) => {
	const {id_producto, id_control_producto} = req.body;
	if(id_producto && id_control_producto){
		const data = req.body;
		req.getConnection((err, conn) => {
			conn.query('INSERT INTO sucursales set ?', [data], (err, sucursales) => {
				if (err){
					res.json(err);
				}else{
					res.json({status: "Registrado", message: "Se registro la sucursal con exito"});
				}
			});
		});	
	}else{
		res.status(500).json({status: "Error", message: "No se ha completado algunos campos"});
	}
});	


module.exports = router;