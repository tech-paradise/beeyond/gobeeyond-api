const express = require('express');
const router = express.Router();
const _ = require('underscore');
const validate = require('./validaciones');

var Openpay = require('openpay');
var openpay = new Openpay('mqachzg2e8ohex4xw5p3', 'sk_f4a49a7cd2c1446fa653937477f0c080', false);

//Routes
//TODO: CORREGIR SI YA ESTA REGISTRADA LA TARJETA 
router.post('/register', (req, res) => {
	req.getConnection((errs, conn) => {
		conn.query('SELECT * FROM tarjetas WHERE token_id LIKE ?', [req.body.token_id], (err, tarjetas) => {
		if (err){
			res.json(errs);
		}else{	
			if(tarjetas.length > 0){
				res.json({status: "Existe", message: "La tarjeta ya esta registrada"});
			}else{

				var data = {"Id_cliente" : req.body.Id_cliente};
				var cardRequest = {'token_id' : req.body.token_id, 'device_session_id' : req.body.device_session_id};

				openpay.customers.cards.create(req.body.customerId, cardRequest, function(error, card){
					console.log(card);
					if(error) {
						console.log(error)
						res.json(error);
					}
					else{
						data.token_id = card.id;
						console.log(data);
						req.getConnection((err, conn) => {
							console.log(err);
							conn.query('INSERT INTO tarjetas set ?', [data], (err2, tarjetas2) => {
								if (err2){
									console.log("en error");
									console.log(err2);
									res.status(400).json(err2);
								}else{
									console.log("en success");
									console.log(tarjetas2);
									res.json({status: "Registrado", message: "Se ha registrado la tarjeta"});
								}
							});
						});
					}					
				});
			}
		}});
	});
});

router.put('/updateCard/:id', (req, res) => {
	const validated = validateUpdate(req.params, req.body, res);
	if (validated){
		const {id} = req.params;
		const data = req.body;
		req.getConnection((err, conn) => {
			conn.query('UPDATE tarjetas set ? WHERE Id_tarjeta = ?', [data, id], (err, tarjetas) => {
				if (err){
					res.json(err);	
				}else{
					res.json({status: "Actualizado", message: "Se ha actualizado los datos de la tarjeta"});
				}
			});
		});
	}
});

router.get('/getCardsByUser/:id', (req, res) => {
	openpay.customers.cards.list(req.params.id, function(error, list){
		if(error){
			console.log(error);
			res.status(400).json(error);
		}else{
			var data = [];
			for (var i = 0; i < list.length; i++){
				data.push({"id": list[i].id, "card_number": list[i].card_number, "holder_name": list[i].holder_name, "expiration_year": list[i].expiration_year, "expiration_month": list[i].expiration_month, "bank_name": list[i].bank_name, "type": list[i].type, "brand": list[i].brand});
			}
			console.log(data);
			res.status(200).json(data);
		}
	});
});

router.delete('/deleteCard/:id', (req, res) => {
	const {id} = req.params;
	req.getConnection((err, conn) => {
		conn.query('DELETE FROM tarjetas WHERE Id_tarjeta = ?', [id], (err, tarjetas) => {
			if (err){
				res.json(err);					
			}else{
				res.json({status: "Eliminado", message: "Se ha eliminado la tarjeta"});
			}
		});
	});
}); 

function validateUpdate(id, data, res){
	let isValidate = validate.validateId(id.id);
	if(isValidate.validate){
		isValidate = validate.validateCount(data.NumeroCuenta);
		if(isValidate.validate){
			isValidate = validate.validateName(data.Nombre_titular);
			if(isValidate.validate){
				isValidate = validate.validateTipoCuenta(data.TipoCuenta);
				if(isValidate.validate){
					isValidate = validate.validateCVV(data.CVV);
					if(isValidate.validate){
						isValidate = validate.validateFechaTarjeta(data.FechaVencimiento);
						if(isValidate.validate){
							return isValidate.validate;
						}else{
							res.status(400).json(isValidate);
						}
					}else{
						res.status(400).json(isValidate);
					}
				}else{
					res.status(400).json(isValidate);
				}
			}else{
				res.status(400).json(isValidate);	
			}
		}else{
			res.status(400).json(isValidate);
		}
	}else{
		res.status(400).json(isValidate);
	}
}

module.exports = router;

