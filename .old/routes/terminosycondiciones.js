const express = require('express');
const router = express.Router();
const _ = require('underscore');

router.post('/register', (req, res) => {
	const {URI_terminos_condiciones, aceptarterminos, Id_cliente} = req.body;
	if (URI_terminos_condiciones && aceptarterminos && Id_cliente){
		let data = req.body;
		data.fecha_terminos = new Date();
		req.getConnection((err, conn) => {
			conn.query('INSERT INTO terminosycondiciones set ?', [data], (err, terminosycondiciones) => {
				if(err){
					res.json(err);
				}else{
					res.json({status: "Registrado", message: "Se registro el termino y condiciones con exito"});
				}
			});
		});
	}else{
		res.status(500).json({status: "Error", message: "No se ha completado algun campo"});
	}
});

module.exports = router;