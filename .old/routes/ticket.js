const express = require('express');
const router = express.Router();
const _ = require('underscore');

//Routes
router.post('/register', (req, res) => {
	const {NumTicket, Concepto, Monto, Nombre_cliente, id_producto, Id_cliente} = req.body;
	if (NumTicket && Concepto && Monto && Nombre_cliente && id_producto >= 0 && Id_cliente){
		let data = req.body;
			req.getConnection((err, conn) => {
				data.Fecha_emision = new Date();
				conn.query('INSERT INTO ticket set ?', [data], (err, ticket) => {
					if (err){
						res.json(err);
					}else{
						res.json({status: "Registrado", message: "Se registro el ticket con exito"});
					}
				});
		});
	}else{
		res.status(500).json({status: "Error", message: "No se ha completado algun campo"});
	}
});

module.exports = router;