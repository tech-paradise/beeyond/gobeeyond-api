const express = require('express');
const router = express.Router();
const _ = require('underscore');
const validate = require('./validaciones');

//Routes
router.post('/register', (req, res) => {
	const validated = validateTransaccion(req.body, res);
	if (validated){
		let data = req.body;
		data.Fecha_transc = new Date();
		req.getConnection((err, conn) => {
			conn.query('INSERT INTO transacciones set ?', [data], (err, transacciones) => {
				if (err){
					res.json(err);
				}else{
					res.json({status: "Registrado", message: "Se ha registrado la transaccion"});
				}
			});
		});
	}
});

function validateTransaccion(data, res){
	let isValidate = concepto(data.Concepto);
	if(isValidate.validate){
		isValidate = validate.validateMoney(data.Monto);
		if(isValidate.validate){
			isValidate = validate.validateId(data.Id_cartera);
			if(isValidate.validate){
				return isValidate.validate;
			}else{
				res.status(400).json(isValidate.message);	
			}
		}else{
			res.status(400).json(isValidate.message);
		}
	}else{
		res.status(400).json(isValidate.message);
	}
}


function concepto(concept){
	if(concept === "Deposito") return {"validate" : true, "message" : "Correcto"};
	else{
		if(concept === "Retiro") return {"validate" : true, "message" : "Correcto"};
		else{
			if(concept === "Ganancia") return {"validate" : true, "message" : "Correcto"};
			else{
				if(concept === "Rendimiento") return {"validate" : true, "message" : "Correcto"};
				else return {"validate" : false, "message" : "Concepto Invalido, unicamente se acepta Deposito, Retiro, Ganancia, Rendimiento"};
			}
		}
	}
}

module.exports = router;

