const express = require('express');
const router = express.Router();
const _ = require('underscore');

router.post('/register', (req, res) => {
	const {Apuesta_por_cliente, id_quiniela, id_cliente} = req.body;
	if (Apuesta_por_cliente && id_quiniela && id_cliente){
		let data = req.body;	
		req.getConnection((err, conn) => {
			conn.query('INSERT INTO usuarioquiniela set ?', [data], (err, usuarioquiniela) => {
				if(err){
					res.json(err);
				}else{
					res.json({status: "Registrado", message: "Se registro el usuario en la quiniela con exito"});
				}
			});
		});
	}else{
		res.status(500).json({status: "Error", message: "No se ha completado algun campo"});
	}
});

router.get('/getGameId/:id1', (req, res) => {
	let {id1} = req.params
	req.getConnection((err, conn) => {
		conn.query('SELECT id_usuario_quiniela FROM usuarioquiniela WHERE id_cliente = ?', [id1], (err, usuarioquiniela) => {
			if (err){
				res.json(err);
			}else{
				res.json(usuarioquiniela);
			}
		});
	});
});

module.exports = router;