module.exports = {
	validateName : function(data){
		console.log(data);
		if(data === "") return {"validate" : false, "message" : "Nombre Vacio"};
		else{
			const dataExpression = /^[A-Za-zÑñáéíóúÁÉÍÓÚ]{1}('?\s?[A-Za-zÑñáéíóúÁÉÍÓÚ]){1,50}$/;
			const isValidate = dataExpression.test(data);
			if(!isValidate) return {"validate" : false, "message" : "Nombre Invalido, el nombre debe de contener de 2 a 50 letras, sin caracteres especiales ni numeros"};
 			else return {"validate" : true, "message" : "Correcto"};
		}
	},
	validateAlias : function(data){
		if(data === "") return {"validate" : false, "message" : "Alias Vacio"};
		else{
			const AliasExpression = /^[a-zA-Z0-9_-]{3,15}$/;
			const isValidate = AliasExpression.test(data);
			if(!isValidate) return {"validate" : false, "message" : "Alias Invalido, el alias debe contener de 3 a 15 caracteres, unicamente se acepta numeros, _, -, y letras"};
			else return {"validate" : true, "message" : "Correcto"};
		}
	},

	validateFecha : function(data){
		if(data === "") return {"validate" : false, "message" : "Fecha Vacia"};
		else{
			const FechaExpression = /^([12]\d{3}-([1-9]|0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))$/;
			const isValidate = FechaExpression.test(data);
			if(!isValidate) return {"validate" : false, "message" : "Fecha Invalida, debe estar aaaa-mm-dd"};
			else return {"validate" : true, "message" : "Correcto"};
		}
	},

	validateEmail : function(data){
		if(data === "") return {"validate" : false, "message" : "Email Vacio"};
		else{
			const EmailExpression = /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/;
			const isValidate = EmailExpression.test(data);
			if(!isValidate) return {"validate" : false, "message" : "Email Invalido"};
			else return {"validate" : true, "message" : "Correcto"};
		}
	},

	validateCel : function(data){
		if(data === "") return {"validate" : false, "message" : "Celular Vacio"};
		else{
			const CelExpression = /^\+?[0-9]{10}$/;
			const isValidate = CelExpression.test(data);
			if(!isValidate) return {"validate" : false, "message" : "Celular Invalido, debe contener el numero de extension del pais"};
			else return {"validate" : true, "message" : "Correcto"};
		}
	},

	validatePassword : function(data){
		if(data === "") return {"validate" : false, "message" : "Contraseña Vacio"};
		else{
			const PasswordExpression = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{4,40}$/;
			const isValidate = PasswordExpression.test(data);
			if(!isValidate) return {"validate" : false, "message" : "Contraseña Invalida, debe contener al menos una Letra y un Numero, minimo 4 caracteres"};
			else return {"validate" : true, "message" : "Correcto"};
		}
	},

	validatePin : function(data){
		if(data === "") return {"validate" : false, "message" : "Pin Vacio"};
		else{
			const PinExpression = /^[0-9]{4,6}$/;
			const isValidate = PinExpression.test(data);
			if(!isValidate) return {"validate" : false, "message" : "Pin Invalido, debe ser unicamente númerico de 4 a 6 numeros"};
			else return {"validate" : true, "message" : "Correcto"};
		}
	},

	validateCount : function(data){
		if(data === "") return {"validate" : false, "message" : "Numero de Cuenta Vacio"};
		else{
			const CountExpression = /^(?:4\d{3}|5[1-5]\d{2}|6011|3[47]\d{2})([-\s]?)\d{4}\1\d{4}\1\d{3,4}$/;
			const isValidate = CountExpression.test(data);
			if(!isValidate) return {"validate" : false, "message" : "Numero de Cuenta Invalido"};
			else return {"validate" : true, "message" : "Correcto"};
		}
	},

	validateCVV : function(data){
		if(data === "") return {"validate" : false, "message" : "CVV Vacio"};
		else{
			const CVVExpression = /^[0-9]{3}$/;
			const isValidate = CVVExpression.test(data);
			if(!isValidate) return {"validate" : false, "message" : "CVV Invalido"};
			else return {"validate" : true, "message" : "Correcto"};
		}
	},

	validateFechaTarjeta : function(data){
		if(data === "") return {"validate" : false, "message" : "Fecha Vacia"};
		else{
			const FechaExpression = /^(0[1-9]|1[0-2])-(2[0-9]{3})$/;
			const isValidate = FechaExpression.test(data);
			if(!isValidate) return {"validate" : false, "message" : "Fecha Invalida, debe estar mm-aaaa"};
			else return {"validate" : true, "message" : "Correcto"};
		}
	},

	validateTipoCuenta : function(data){
		if(data === "") return {"validate" : false, "message" : "Fecha Vacia"};
		else if(data === "Visa") return {"validate" : true, "message" : "Correcto"};
		else if(data === "Mastercard") return {"validate" : true, "message" : "Correcto"};
		else if(data === "American Express") return {"validate" : true, "message" : "Correcto"};
		else return {"validate" : false, "message" : "Tipo de Cuenta Invalida, Sole se admite Visa, Mastercard, American Express"};
	},

	validateMoney : function(data){
		if(data === "") return {"validate" : false, "message" : "Monto Vacio"};
		else{
			const MoneyExpression = /^\-?([2-9][0-9]|[1-9][0-9]{2,5})$/;
			const isValidate = MoneyExpression.test(data);
			if(!isValidate) return {"validate" : false, "message" : "Monto Invalido"};
			else return {"validate" : true, "message" : "Correcto"};
		}
	},

	validateId : function(data){
		if(data === "") return {"validate" : false, "message" : "ID Vacio"};
		else{
			const IdExpression = /^[0-9]+$/;
			const isValidate = IdExpression.test(data);
			if(!isValidate) return {"validate" : false, "message" : "ID Invalido"};
			else return {"validate" : true, "message" : "Correcto"};
		}
	}, 

	validateURL : function(data){
		if(data === "") return {"validate" : false, "message" : "Url Vacio"};
		else{
			const UrlExpression = /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/;
			const isValidate = UrlExpression.test(data);
			if(!isValidate) return {"validate" : false, "message" : "Url Invalido"};
			else return {"validate" : true, "message" : "Correcto"};
		}
	}
}



//Producteca, Europea, Mercado Libre, Walmart, Rappy