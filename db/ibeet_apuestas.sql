-- MySQL dump 10.16  Distrib 10.1.13-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: bee_connect_apuestas
-- ------------------------------------------------------
-- Server version 5.7.24

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cartera`
--

DROP TABLE IF EXISTS `cartera`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cartera` (
  `Id_cartera` int(11) NOT NULL AUTO_INCREMENT,
  `Saldo` float(9,2) DEFAULT NULL,
  `Id_cliente` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id_cartera`),
  KEY `fk_cartera_cliente_1` (`Id_cliente`),
  CONSTRAINT `fk_cartera_cliente_1` FOREIGN KEY (`Id_cliente`) REFERENCES `cliente` (`Id_cliente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cartera`
--

LOCK TABLES `cartera` WRITE;
/*!40000 ALTER TABLE `cartera` DISABLE KEYS */;
/*!40000 ALTER TABLE `cartera` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cliente`
--

DROP TABLE IF EXISTS `cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cliente` (
  `Id_cliente` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre_cliente` varchar(50) NOT NULL COMMENT 'Nombre real de cliente ',
  `Primer_apel` varchar(255) NOT NULL,
  `Segundo_apel` varchar(255) NOT NULL,
  `Alias` varchar(255) DEFAULT NULL,
  `ImageURI` varchar(255) DEFAULT NULL,
  `Fecha_nac` date DEFAULT NULL COMMENT 'Fecha de nacimiento de cliente ',
  `Tel_cel` varchar(15) NOT NULL COMMENT 'Nùmero telefonico celular ',
  `Pais` varchar(50) DEFAULT NULL,
  `Estado` varchar(255) DEFAULT NULL,
  `Ciudad` varchar(255) DEFAULT NULL,
  `Experiencia` varchar(255) DEFAULT NULL COMMENT 'Para subir de nivel se requiere obtener experiencia ',
  `Nivel` varchar(255) DEFAULT NULL COMMENT 'De acuerdo a la experiencia aumenta el nivel',
  `Status_nivel` varchar(255) DEFAULT NULL,
  `Comision` float(4,2) DEFAULT NULL,
  `Fecha_alta` datetime(6) DEFAULT NULL,
  `Genero` varchar(255) DEFAULT NULL,
  `Ocupacion` varchar(255) DEFAULT NULL,
  `Bloqueado` varchar(2) DEFAULT NULL,
  `Menor` varchar(2) DEFAULT NULL,
  `Correo_alternativo` varchar(255) DEFAULT NULL,
  `Id_login` int(11) DEFAULT NULL,
  `Id_equipos_pref` int(11) DEFAULT NULL COMMENT 'Información de equipos guardados en preferencias del usuario',
  `Id_analitica` int(11) DEFAULT NULL COMMENT 'Aun por definir  ?',
  `id_terminos` int(11) DEFAULT NULL,
  `Id_preferencia` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id_cliente`),
  KEY `fk_cliente_terminosycondiciones_1` (`id_terminos`),
  KEY `fk_cliente_login_1` (`Id_login`),
  KEY `fk_cliente_preferencias_1` (`Id_equipos_pref`),
  CONSTRAINT `fk_cliente_login_1` FOREIGN KEY (`Id_login`) REFERENCES `login` (`Id_login`),
  CONSTRAINT `fk_cliente_preferencias_1` FOREIGN KEY (`Id_equipos_pref`) REFERENCES `preferencias` (`Id_preferencias_equip`),
  CONSTRAINT `fk_cliente_terminosycondiciones_1` FOREIGN KEY (`id_terminos`) REFERENCES `terminosycondiciones` (`id_terminos`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cliente`
--

LOCK TABLES `cliente` WRITE;
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;
/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `controlproductosganados`
--

DROP TABLE IF EXISTS `controlproductosganados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `controlproductosganados` (
  `Id_control_producto` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id enlace a la tabla',
  `Id_reclamacion` int(11) NOT NULL COMMENT 'id enlace a la tabla de proveedores',
  `Codigo` varchar(255) NOT NULL COMMENT 'Codigo de secuencia para cupón o reclamo de producto',
  `Fecha` datetime(6) NOT NULL COMMENT 'Fecha en la que se genero el cupón',
  `QRuri` varchar(255) NOT NULL COMMENT 'URL del QR generado',
  PRIMARY KEY (`Id_control_producto`),
  KEY `fk_controlproductosganados_lugarreclamacion_1` (`Id_reclamacion`),
  CONSTRAINT `fk_controlproductosganados_lugarreclamacion_1` FOREIGN KEY (`Id_reclamacion`) REFERENCES `lugarreclamacion` (`Id_reclamacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `controlproductosganados`
--

LOCK TABLES `controlproductosganados` WRITE;
/*!40000 ALTER TABLE `controlproductosganados` DISABLE KEYS */;
/*!40000 ALTER TABLE `controlproductosganados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `deportes`
--

DROP TABLE IF EXISTS `deportes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deportes` (
  `Id_deporte` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre_deporte` varchar(255) DEFAULT NULL,
  `NombreEquipo` varchar(255) DEFAULT NULL,
  `NombreDeportista` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id_deporte`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `deportes`
--

LOCK TABLES `deportes` WRITE;
/*!40000 ALTER TABLE `deportes` DISABLE KEYS */;
/*!40000 ALTER TABLE `deportes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `factura`
--

DROP TABLE IF EXISTS `factura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `factura` (
  `id_factura` int(11) NOT NULL AUTO_INCREMENT,
  `numeracion` varchar(255) DEFAULT NULL,
  `fecha_emision` date DEFAULT NULL,
  `datos_emisor` varchar(255) DEFAULT NULL,
  `datos_receptor` varchar(255) DEFAULT NULL,
  `concepto` varchar(255) DEFAULT NULL,
  `tipo_impositivo` float(8,2) DEFAULT NULL,
  `codigo_postal` varchar(50) DEFAULT NULL,
  `nombre_fiscal` varchar(255) DEFAULT NULL,
  `codigo_identificacion_fiscal` varchar(255) DEFAULT NULL,
  `id_cliente` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_factura`),
  KEY `fk_factura_cliente_1` (`id_cliente`),
  CONSTRAINT `fk_factura_cliente_1` FOREIGN KEY (`id_cliente`) REFERENCES `cliente` (`Id_cliente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `factura`
--

LOCK TABLES `factura` WRITE;
/*!40000 ALTER TABLE `factura` DISABLE KEYS */;
/*!40000 ALTER TABLE `factura` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `juego_uno_uno`
--

DROP TABLE IF EXISTS `juego_uno_uno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `juego_uno_uno` (
  `Id_juego_uno_uno` int(11) NOT NULL AUTO_INCREMENT,
  `monto` float(8,2) DEFAULT NULL,
  `fecha_uno_uno` datetime(1) DEFAULT NULL COMMENT 'Variable que guarda el status true o false /si gana o pierde',
  `resultado` varchar(255) DEFAULT NULL,
  `ganador` varchar(50) DEFAULT NULL COMMENT 'Fecha en la que se realizo la apuesta',
  `perdedor` varchar(50) DEFAULT NULL,
  `estado_juego` tinyint(1) DEFAULT NULL,
  `id_cliente1` int(11) DEFAULT NULL,
  `id_cliente2` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id_juego_uno_uno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `juego_uno_uno`
--

LOCK TABLES `juego_uno_uno` WRITE;
/*!40000 ALTER TABLE `juego_uno_uno` DISABLE KEYS */;
/*!40000 ALTER TABLE `juego_uno_uno` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `juegopersonalizado`
--

DROP TABLE IF EXISTS `juegopersonalizado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `juegopersonalizado` (
  `id_juego_personalizado` int(11) NOT NULL AUTO_INCREMENT,
  `monto` float(8,2) DEFAULT NULL,
  `fecha_personalizado` datetime(6) DEFAULT NULL,
  `resultado` varchar(50) DEFAULT NULL,
  `ganador` varchar(50) DEFAULT NULL,
  `perdedor` varchar(50) DEFAULT NULL,
  `id_control_producto` int(11) DEFAULT NULL,
  `id_cliente_1` int(11) DEFAULT NULL,
  `id_cliente_2` int(11) DEFAULT NULL,
  `id_cliente_3` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_juego_personalizado`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `juegopersonalizado`
--

LOCK TABLES `juegopersonalizado` WRITE;
/*!40000 ALTER TABLE `juegopersonalizado` DISABLE KEYS */;
/*!40000 ALTER TABLE `juegopersonalizado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_partidas_compartidas_usuario`
--

DROP TABLE IF EXISTS `log_partidas_compartidas_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_partidas_compartidas_usuario` (
  `id_score` int(11) NOT NULL AUTO_INCREMENT,
  `score` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_score`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_partidas_compartidas_usuario`
--

LOCK TABLES `log_partidas_compartidas_usuario` WRITE;
/*!40000 ALTER TABLE `log_partidas_compartidas_usuario` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_partidas_compartidas_usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `login`
--

DROP TABLE IF EXISTS `login`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login` (
  `Id_login` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Clave primaria logeo',
  `Password` varchar(255) DEFAULT NULL COMMENT 'Contraseña de usuario',
  `Pin` varchar(255) DEFAULT NULL COMMENT 'Pin de 9 digitos  numericos',
  `Tipo_usuario` varchar(50) DEFAULT NULL,
  `Correo_electronico` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id_login`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `login`
--

LOCK TABLES `login` WRITE;
/*!40000 ALTER TABLE `login` DISABLE KEYS */;
/*!40000 ALTER TABLE `login` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lugarreclamacion`
--

DROP TABLE IF EXISTS `lugarreclamacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lugarreclamacion` (
  `Id_reclamacion` int(11) NOT NULL AUTO_INCREMENT,
  `Ubicacion_tienda_local` varchar(255) DEFAULT NULL,
  `Ciudad_o_Estado` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id_reclamacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lugarreclamacion`
--

LOCK TABLES `lugarreclamacion` WRITE;
/*!40000 ALTER TABLE `lugarreclamacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `lugarreclamacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pago_proveedores`
--

DROP TABLE IF EXISTS `pago_proveedores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pago_proveedores` (
  `Id_pago_proveedores` int(11) NOT NULL AUTO_INCREMENT,
  `Numero_cuenta_proveedor` int(18) NOT NULL,
  `Numero_tarjeta_débito` int(18) DEFAULT NULL,
  `Cuenta_clabe` int(18) DEFAULT NULL,
  `Id_proveedor` int(11) NOT NULL,
  PRIMARY KEY (`Id_pago_proveedores`),
  KEY `fk_pago_proveedores_proveedores_1` (`Id_proveedor`),
  CONSTRAINT `fk_pago_proveedores_proveedores_1` FOREIGN KEY (`Id_proveedor`) REFERENCES `proveedores` (`Id_proveedor`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pago_proveedores`
--

LOCK TABLES `pago_proveedores` WRITE;
/*!40000 ALTER TABLE `pago_proveedores` DISABLE KEYS */;
/*!40000 ALTER TABLE `pago_proveedores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `preferencias`
--

DROP TABLE IF EXISTS `preferencias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `preferencias` (
  `Id_preferencias_equip` int(50) NOT NULL AUTO_INCREMENT,
  `Deporte` varchar(50) DEFAULT NULL,
  `Deportista` varchar(50) DEFAULT NULL,
  `Id_cliente` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id_preferencias_equip`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `preferencias`
--

LOCK TABLES `preferencias` WRITE;
/*!40000 ALTER TABLE `preferencias` DISABLE KEYS */;
/*!40000 ALTER TABLE `preferencias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `producto`
--

DROP TABLE IF EXISTS `producto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `producto` (
  `Id_producto` int(11) NOT NULL AUTO_INCREMENT,
  `Numero_articulo` int(11) DEFAULT NULL,
  `Descripcion` varchar(255) DEFAULT NULL,
  `Precio` float(5,2) DEFAULT NULL,
  `IP` int(11) DEFAULT NULL COMMENT 'Inventario perpetuo fisico',
  `Id_proveedor` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id_producto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `producto`
--

LOCK TABLES `producto` WRITE;
/*!40000 ALTER TABLE `producto` DISABLE KEYS */;
/*!40000 ALTER TABLE `producto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proveedores`
--

DROP TABLE IF EXISTS `proveedores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proveedores` (
  `Id_proveedor` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre_proveedor` varchar(50) DEFAULT NULL,
  `Direccion_proveedor` varchar(120) DEFAULT NULL,
  `Fecha_registro` datetime(6) DEFAULT NULL,
  `Id_login` int(11) DEFAULT NULL,
  `Id_reclamacion` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id_proveedor`),
  KEY `fk_proveedores_login_1` (`Id_login`),
  CONSTRAINT `fk_proveedores_login_1` FOREIGN KEY (`Id_login`) REFERENCES `login` (`Id_login`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proveedores`
--

LOCK TABLES `proveedores` WRITE;
/*!40000 ALTER TABLE `proveedores` DISABLE KEYS */;
/*!40000 ALTER TABLE `proveedores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quiniela`
--

DROP TABLE IF EXISTS `quiniela`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quiniela` (
  `Id_quiniela` int(11) NOT NULL AUTO_INCREMENT,
  `Contador_apuestas_A` int(11) DEFAULT NULL,
  `Contador_apuestas_B` int(11) DEFAULT NULL,
  `Contador_apuestas_C` int(11) DEFAULT NULL,
  `Total_A` float(8,2) DEFAULT NULL,
  `Total_B` float(8,2) DEFAULT NULL,
  `Total_C` float(8,2) DEFAULT NULL,
  `Ganador_equipo_o_deportista` varchar(50) DEFAULT NULL,
  `Id_usuarioQuiniela` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id_quiniela`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quiniela`
--

LOCK TABLES `quiniela` WRITE;
/*!40000 ALTER TABLE `quiniela` DISABLE KEYS */;
/*!40000 ALTER TABLE `quiniela` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rendimiento`
--

DROP TABLE IF EXISTS `rendimiento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rendimiento` (
  `id_rendimiento` int(11) NOT NULL AUTO_INCREMENT,
  `Rendimiento_total` float(8,2) DEFAULT NULL,
  `Id_cliente` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_rendimiento`),
  KEY `fk_rendimiento_cliente_1` (`Id_cliente`),
  CONSTRAINT `fk_rendimiento_cliente_1` FOREIGN KEY (`Id_cliente`) REFERENCES `cliente` (`Id_cliente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rendimiento`
--

LOCK TABLES `rendimiento` WRITE;
/*!40000 ALTER TABLE `rendimiento` DISABLE KEYS */;
/*!40000 ALTER TABLE `rendimiento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sucursales`
--

DROP TABLE IF EXISTS `sucursales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sucursales` (
  `id_sucursal` int(11) NOT NULL AUTO_INCREMENT,
  `id_producto` int(11) DEFAULT NULL,
  `id_control_producto` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_sucursal`),
  KEY `fk_sucursales_controlproductosganados_1` (`id_control_producto`),
  CONSTRAINT `fk_sucursales_controlproductosganados_1` FOREIGN KEY (`id_control_producto`) REFERENCES `controlproductosganados` (`Id_control_producto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sucursales`
--

LOCK TABLES `sucursales` WRITE;
/*!40000 ALTER TABLE `sucursales` DISABLE KEYS */;
/*!40000 ALTER TABLE `sucursales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tarjetas`
--

DROP TABLE IF EXISTS `tarjetas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tarjetas` (
  `Id_tarjeta` int(255) NOT NULL AUTO_INCREMENT,
  `NumeroCuenta` varchar(255) DEFAULT NULL COMMENT 'Numero de cuenta a 18 digitos',
  `Nombre_titular` varchar(255) DEFAULT NULL,
  `TipoCuenta` varchar(255) DEFAULT NULL COMMENT 'Crédito,débito,etc',
  `Marca` varchar(255) DEFAULT NULL COMMENT 'Visa,Mastercard,American express, etc',
  `CVV` int(11) DEFAULT NULL,
  `FechaVencimiento` date DEFAULT NULL COMMENT 'Fecha de vencimiento de la tarjeta ',
  `Id_cliente` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id_tarjeta`),
  KEY `fk_tarjetas_cliente_1` (`Id_cliente`),
  CONSTRAINT `fk_tarjetas_cliente_1` FOREIGN KEY (`Id_cliente`) REFERENCES `cliente` (`Id_cliente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tarjetas`
--

LOCK TABLES `tarjetas` WRITE;
/*!40000 ALTER TABLE `tarjetas` DISABLE KEYS */;
/*!40000 ALTER TABLE `tarjetas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `terminosycondiciones`
--

DROP TABLE IF EXISTS `terminosycondiciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `terminosycondiciones` (
  `id_terminos` int(11) NOT NULL AUTO_INCREMENT,
  `info_terminos_condiciones` varchar(255) NOT NULL,
  `aceptarterminos` tinyint(3) NOT NULL,
  PRIMARY KEY (`id_terminos`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `terminosycondiciones`
--

LOCK TABLES `terminosycondiciones` WRITE;
/*!40000 ALTER TABLE `terminosycondiciones` DISABLE KEYS */;
/*!40000 ALTER TABLE `terminosycondiciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transacciones`
--

DROP TABLE IF EXISTS `transacciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transacciones` (
  `Id_transc` int(11) NOT NULL AUTO_INCREMENT,
  `Concepto` varchar(20) DEFAULT NULL,
  `Monto` float(9,2) DEFAULT NULL,
  `Fecha_transc` date DEFAULT NULL,
  `Id_cartera` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id_transc`),
  KEY `fk_transacciones_cartera_1` (`Id_cartera`),
  CONSTRAINT `fk_transacciones_cartera_1` FOREIGN KEY (`Id_cartera`) REFERENCES `cartera` (`Id_cartera`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transacciones`
--

LOCK TABLES `transacciones` WRITE;
/*!40000 ALTER TABLE `transacciones` DISABLE KEYS */;
/*!40000 ALTER TABLE `transacciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarioquiniela`
--

DROP TABLE IF EXISTS `usuarioquiniela`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarioquiniela` (
  `id_usuario_quiniela` int(11) NOT NULL AUTO_INCREMENT,
  `Apuesta_por_cliente` float(8,2) DEFAULT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `id_quiniela` int(11) DEFAULT NULL,
  `id_cliente` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_usuario_quiniela`),
  KEY `fk_usuarioquiniela_quiniela_1` (`id_quiniela`),
  KEY `fk_usuarioquiniela_cliente_1` (`id_usuario`),
  CONSTRAINT `fk_usuarioquiniela_cliente_1` FOREIGN KEY (`id_usuario`) REFERENCES `cliente` (`Id_cliente`),
  CONSTRAINT `fk_usuarioquiniela_quiniela_1` FOREIGN KEY (`id_quiniela`) REFERENCES `quiniela` (`Id_quiniela`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarioquiniela`
--

LOCK TABLES `usuarioquiniela` WRITE;
/*!40000 ALTER TABLE `usuarioquiniela` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuarioquiniela` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-02-13 16:13:15
