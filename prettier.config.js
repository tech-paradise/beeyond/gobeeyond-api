module.exports = {
  singleQuote: true,
  trailingComma: 'es5',
  tabs: true,
  tabWidth: 4,
  printWidth: 80
};
