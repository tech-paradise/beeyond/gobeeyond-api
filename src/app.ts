import { Server } from './server/Server';
import { config } from 'dotenv';
import { getLogger, Logger } from 'log4js';
import { createConnection } from 'typeorm';
// Archivo Main

// variables de entorno
if (process.env.NODE_ENV !== 'production') {
    config();
}

// Logger
const LOG: Logger = getLogger('Main');
LOG.level = 'all';

export const SERVER_INIT = async (): Promise<Server> => {
    try {
        LOG.debug('Creando Conexion a Base de datos');
        await createConnection();
        LOG.debug('Conexion Establecida');
    }catch (e) {
        LOG.error('No se pudo establcer conexion:' + e);
    }
    return Server.getServer();
};

if (process.env.NODE_ENV !== 'test') {
    SERVER_INIT().then(server => {
        server.start(process.env.PORT, () =>
            LOG.info(`Servidor en: ${process.env.HOST}:${process.env.PORT}`)
        );
    })
        .catch(error => LOG.error(error));
}
