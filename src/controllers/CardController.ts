import { Request, Response } from 'express';
import { getRepository, QueryFailedError } from 'typeorm';
import { CatalogoTarjetas } from '../entity/CatalogoTarjetas';
import { getLogger, Logger } from 'log4js';
import { promisify } from 'util';
import { Usuario } from '../entity/Usuario';

const openpay = require('openpay');

export default class CardController {
    private readonly LOG: Logger = getLogger('CardController');
    private OPENPAY: any;

    constructor() {
        this.OPENPAY = new openpay(
            process.env.OPENPAY_ID,
            process.env.OPENPAY_SECRET,
            process.env.OPENPAY_PRODUCTION === 'true'
        );
    }

    async register(req: Request, res: Response): Promise<Response> {
        const {
            token,
            idDeviceSession,
            userSession,
        }: {
            token: string;
            idDeviceSession: string;
            userSession: Usuario;
        } = req.body;

        try {
            const asyncCreateCard = promisify(
                this.OPENPAY.customers.cards.create
            ).bind(this.OPENPAY.customers.cards);

            const cardOpenpay = await asyncCreateCard(userSession.idOpenpay, {
                token_id: token,
                device_session_id: idDeviceSession,
            });

            const card = {
                idUsuario: userSession.idUsuario,
                tokenId: cardOpenpay.id,
            } as CatalogoTarjetas;

            const cardRepository = getRepository(CatalogoTarjetas);
            await cardRepository.save(card);

            this.LOG.debug('Saved card');

            return res.json({
                message: 'Tarjeta registrada correctamente',
                id: card.idCatalogoTarjetas,
            });
        } catch (error) {
            this.LOG.error('ERROR almacenando tarjeta: ', error);
            return res
                .status(500)
                .json({ message: 'Error almacenando tarjeta' });
        }
    }

    async updateCard(req: Request, res: Response): Promise<Response> {
        const idTarjeta: number = Number(req.params.id);
        const { token }: { token: string } = req.body;

        const cardRepository = getRepository(CatalogoTarjetas);

        // OPENPAY UPDATECARD is done at the front end
        // const asyncCreasteCard = promisify(this.OPENPAY.customers.cards.update).bind(this.OPENPAY.customers.cards);

        try {
            await cardRepository.update(idTarjeta, { tokenId: token });
            this.LOG.info('Tarjeta actualizada');
        } catch (error) {
            this.LOG.error(error);
        }

        return res.json({});
    }

    async getCardsOfUser(req: Request, res: Response): Promise<Response> {
        const { userSession }: { userSession: Usuario } = req.body;

        const promiseListCards = promisify(
            this.OPENPAY.customers.cards.list
        ).bind(this.OPENPAY.customers.cards);

        const cardRepository = getRepository(CatalogoTarjetas);

        try {
            let cardsResult: Array<Object> = [];

            let cardsInDB = await cardRepository.find({
                idUsuario: userSession.idUsuario,
            });

            let listOpenpay = await promiseListCards(userSession.idOpenpay);
            for (const cardOpenpay of listOpenpay) {
                cardsResult.push({
                    id: cardsInDB.find(
                        (cardDB: CatalogoTarjetas) =>
                            cardDB.tokenId == cardOpenpay.id
                    )?.idCatalogoTarjetas,
                    token: cardOpenpay.id,
                    cardNumber: cardOpenpay.card_number,
                    holderName: cardOpenpay.holder_name,
                    expirationYear: cardOpenpay.expiration_year,
                    expirationMonth: cardOpenpay.expiration_month,
                    bankName: cardOpenpay.bank_name,
                    type: cardOpenpay.type,
                    brand: cardOpenpay.brand,
                });
            }

            this.LOG.info('Tarjetas obtenidas');
            return res.json(cardsResult);
        } catch (error) {
            this.LOG.error(error);
            return res
                .status(500)
                .json({ message: 'Error recuperando información' });
        }
    }

    async deleteCard(req: Request, res: Response): Promise<Response> {
        const idCard = req.params.id;
        const { userSession }: { userSession: Usuario } = req.body;
        const cardsRepository = getRepository(CatalogoTarjetas);

        try {
            const tarjetaParaEliminar = await cardsRepository.findOneOrFail(
                idCard
            );

            await cardsRepository.delete(idCard);

            const promiseDeleteCard = promisify(
                this.OPENPAY.customers.cards.delete
            ).bind(this.OPENPAY.customers.cards);

            await promiseDeleteCard(
                userSession.idOpenpay,
                tarjetaParaEliminar.tokenId
            );

            this.LOG.info('Card deleted');
			return res.json({ message: 'Tarjeta eliminada' });
			
        } catch (error) {
            this.LOG.error(error);
            return res
                .status(500)
                .json({ message: 'Error eliminando tarjeta' });
        }
    }
}
