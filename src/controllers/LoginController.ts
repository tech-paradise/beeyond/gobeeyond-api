import { Response, Request } from "express";
import { getRepository } from "typeorm";
import { Login } from "../entity/Login";
import { compare } from "bcryptjs";
import { sign } from "jsonwebtoken";
import {TokenInfo} from "../interfaces/TokenInfo";


export class LoginController {
    private readonly LOGIN_SERVICE =
        getRepository(Login);

    constructor() {}
    async getLogin(req: Request, res: Response): Promise <Response>{
        const {
            email,
            password
        } : {
            email: string,
            password: string
        } = req.body;

        const login = await this.LOGIN_SERVICE.findOne({
            where : {
                correoElectronico: email
            },
            relations:['usuarios']
        });

        if (!login){
            return res.status(404).json({
                message: 'No existe Usuario con este correo'
            });
        }

        if (! await compare(password,login.password)){
            return res.status(404).json({
                message: 'Password Incorrecto'
            });
        }

        const tokenInfo: TokenInfo = {
            id: login.idLogin,
            user: login.usuarios[0].idUsuario,
            email: login.correoElectronico
        };
        const seed = process.env.TOKEN_SEDD || '';
        const expired = process.env.TOKEN_EXPIRED || '1d';

        const token = sign(
            tokenInfo,
            seed,
            {
                expiresIn: expired
            }
        );



        return res.json({
            message: 'Logeo exitoso',
            user: tokenInfo.user,
            token
        });
    }
}
