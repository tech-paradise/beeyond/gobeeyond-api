import { Request, Response } from 'express';
import { getLogger, Logger } from 'log4js';
import { promisify } from 'util';


const openpay = require('openpay');

export class OpenpayController {
    private readonly LOG: Logger = getLogger('OpenpayController');
    private OPENPAY: any;

    constructor() {
        this.OPENPAY = new openpay(
            process.env.OPENPAY_ID,
            process.env.OPENPAY_SECRET,
            process.env.OPENPAY_PRODUCTION === 'true'
        );
    }

    sendPayment({
        idCustomer,
        idSource,
        amount,
        description,
        idDeviceSession,
        idOrder,
        method,
        currency,
    }: {
        idCustomer: string;
        idSource: string;
        amount: number;
        description: string;
        idDeviceSession: string;
        idOrder: string;
        method: string;
        currency: string;
    }): Promise<any> {
        const promiseChargeCustomer = promisify(
            this.OPENPAY.charges.create
        ).bind(this.OPENPAY.charges);

        return promiseChargeCustomer(idCustomer, {
            source_id: idSource,
            method,
            amount,
            currency,
            description,
            order_id: idOrder,
            device_session_id: idDeviceSession,
        });
    }
}
