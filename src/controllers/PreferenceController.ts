import { Request, Response } from 'express';
import { getRepository, QueryFailedError } from 'typeorm';
import { CatalogoPreferencias } from '../entity/CatalogoPreferencias';
import { getLogger, Logger } from 'log4js';

export class PreferenceController {
    private readonly LOG: Logger = getLogger('PreferenceController');

    constructor() {}

    async register(req: Request, res: Response): Promise<Response> {
        this.LOG.debug('Registrando preferencia');
        // TODO: take id user from session and not from
        const { deporte, deportista, idUser } = req.body;

        const preferenciasRepository = getRepository(CatalogoPreferencias);
        const preferencia = new CatalogoPreferencias();
        preferencia.idUsuario = idUser;
        preferencia.deporte = deporte;
        preferencia.deportista = deportista;

        try {
            this.LOG.info('Saving preferencia');
            await preferenciasRepository.save(preferencia);
            return res.status(200).json({
                message: 'Preferencia guardada',
                id: preferencia.idCatalogoPreferencias,
            });
        } catch (error) {
            this.LOG.error(error);
            return res
                .status(500)
                .json({ message: 'Error al almacenar preferencia' });
        }
    }

    async getList(req: Request, res: Response): Promise<Response> {
        this.LOG.debug('Obteniendo preferencia');

        // TODO: take id user from session and not from params
        const idUser: number = Number(req.params.id);
        const preferenciasRepository = getRepository(CatalogoPreferencias);

        try {
            const listOfPreferences = await preferenciasRepository.find({
                idUsuario: idUser,
            });

            return res.status(200).json(listOfPreferences);
        } catch (error) {
            this.LOG.error(error);
            return res
                .status(500)
                .json({ message: 'Error al buscar preferencias' });
        }
    }

    async update(req: Request, res: Response): Promise<Response> {
        this.LOG.debug('Actualizando preferencia');
        // TODO: take id user from session and not from body
        const { deporte, deportista, idPreferencia } = req.body;
        const preferenciasRepository = getRepository(CatalogoPreferencias);

        try {
            this.LOG.info('Saving preferencia');
            await preferenciasRepository.update(idPreferencia, {
                deporte,
                deportista,
            });
            return res.status(200).json({
                message: 'Preferencia actualizada',
            });
        } catch (error) {
            this.LOG.error(error);
            return res
                .status(500)
                .json({ message: 'Error al actualizar preferencia' });
        }
    }

    async delete(req: Request, res: Response): Promise<Response> {
        this.LOG.debug('Deleting preferencia preferencia');
        // TODO: take id user from session and not from body
        const idPreferencia = req.params.id;
        const preferenciasRepository = getRepository(CatalogoPreferencias);

        try {
            this.LOG.info('Deleting preferencia');
            await preferenciasRepository.delete(idPreferencia);
            return res.status(200).json({
                message: 'Preferencia eliminada',
            });
        } catch (error) {
            this.LOG.error(error);
            return res
                .status(500)
                .json({ message: 'Error al eliminar preferencia' });
        }
    }
}
