import { Request, Response } from 'express';
import { getRepository } from 'typeorm';
import { Producto } from '../entity/Producto';
import { getLogger, Logger } from 'log4js';

export class ProductController {
    private readonly LOG: Logger = getLogger('ProductController');

    constructor() {}

    async getList(req: Request, res: Response): Promise<Response> {
        // TODO consider pagination in the future
        const repoProducts = getRepository(Producto);

        try {
            this.LOG.debug('Searching for products');
            const products = await repoProducts.find();
			return res.json(products);
			
        } catch (error) {
            this.LOG.error(error);
            return res
                .status(500)
                .json({ message: 'Error al realizar busqueda' });
        }
    }

    async getOne(req: Request, res: Response): Promise<Response> {
        const idProducto = Number(req.params.id);
        const repoProductos = getRepository(Producto);

        try {
            this.LOG.debug('Looking for producto');
            const productoEncontrado = await repoProductos.find({ idProducto });
			return res.status(200).json({ productoEncontrado });
			
        } catch (error) {
            return res
                .status(500)
                .json({ message: 'Error al buscar producto' });
        }
    }
}
