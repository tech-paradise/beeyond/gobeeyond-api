import { Request, Response } from 'express';
import { Terminosycondiciones } from '../entity/Terminosycondiciones';
import { getRepository } from 'typeorm';

import { getLogger, Logger } from 'log4js';

export class TermsConditionsController {
    private readonly LOG: Logger = getLogger('PreferenceController');

    constructor() {}

    async register(req: Request, res: Response): Promise<Response> {
        // TODO: take user ID from session when it's been implemented
        const { uri, acepta, fecha, userId } = req.body;

        const terminos = new Terminosycondiciones();
        terminos.uriTerminosCondiciones = uri;
        terminos.aceptarterminos = acepta;
        terminos.fechaTerminos = fecha;
        terminos.idUsuario = userId;

        const repoTerminos = getRepository(Terminosycondiciones);

        try {
            this.LOG.debug('Recording Terms&Conditions record');
            await repoTerminos.save(terminos);
            return res.status(200).json({
                message: 'Terminos y condiciones creado',
                id: terminos.idTerminos,
			});
			
        } catch (error) {
            this.LOG.error(error);
            return res.status(500).json({
                message: 'Error almacenando registro de terminos y condiciones',
            });
        }
    }

    async get(req: Request, res: Response): Promise<Response> {
        const idTerminos: number = Number(req.params.id);
        const repoTerminos = getRepository(Terminosycondiciones);

        try {
            const recordTerminos = await repoTerminos.find({ idTerminos });
			return res.status(200).json(recordTerminos);
			
        } catch (error) {
            this.LOG.error(error);
            return res.status(500).json({ message: 'Error buscando registro' });
        }
    }

    async delete(req: Request, res: Response): Promise<Response> {
        const idTerminos: number = Number(req.params.id);
        const repoTerminos = getRepository(Terminosycondiciones);

        try {
            const recordTerminos = await repoTerminos.delete(idTerminos);
			return res.status(200).json({ message: 'Eliminado correctamente' });
			
        } catch (error) {
            this.LOG.error(error);
            return res
                .status(500)
                .json({ message: 'Error al eliminar registro' });
        }
    }
}
