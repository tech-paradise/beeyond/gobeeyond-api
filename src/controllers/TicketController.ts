import { Request, Response } from 'express';
import { Ticket } from '../entity/Ticket';
import { getRepository } from 'typeorm';
import { getLogger, Logger } from 'log4js';

export class TicketController {
    private readonly LOG: Logger = getLogger('TicketController');
    constructor() {}

    async getWithId(req: Request, res: Response): Promise<Response> {
        const idTicket: number = Number(req.params.id);
        const repoTicket = getRepository(Ticket);

        try {
            this.LOG.debug('Searching for ticket with id', idTicket);
            const ticket = await repoTicket.findOneOrFail({ idTicket });
            return res.json({ ticket });
        } catch (error) {
            this.LOG.error(error);
            return res.status(500).json({ message: 'Ticket no creencontrado' });
        }
    }
}
