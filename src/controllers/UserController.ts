import { Request, Response } from 'express';
import { Login } from '../entity/Login';
import { Usuario} from '../entity/Usuario';
import { LEVEL } from "../entity/types/UserLevel";
import { getRepository } from 'typeorm';
import { TYPE } from "../entity/types/UserType";
import { STATUS } from  "../entity/types/GameStatus"
import { getLogger, Logger } from "log4js";
import { promisify } from "util";
import { UserDTO } from "../interfaces/UserDTO";
import { compare, genSaltSync, hashSync } from "bcryptjs";


const openpay: any =require("openpay");

export class UserController {
    private readonly LOG: Logger = getLogger('UserController');
    private readonly USER_SERVICE = getRepository(Usuario);
    private LOGIN_SERVICE = getRepository(Login);

    constructor() {}

    async register(req: Request, res: Response): Promise<Response> {
        const {
            nombreCliente,
            primerApel,
            segundoApel,
            alias,
            fechaNac,
            correoElectronico,
            telCel,
            password,
            pin,
        }: {
            nombreCliente: string;
            primerApel: string;
            segundoApel: string | undefined;
            alias: string;
            fechaNac: string;
            correoElectronico: string;
            telCel: string;
            password: string;
            pin: string;
        } = req.body;

        const emailFound = await this.LOGIN_SERVICE.findOne({
            where: {correoElectronico}
        });

        if (emailFound) {
            return res.status(200).json({
                message: 'Correo ya existente'
            });
        }

        const aliasFound = await this.USER_SERVICE.findOne({
            where: {alias}
        });
        if (aliasFound) {
            return res.status(200).json({
                message: 'Alias ya existente'
            });
        }

        const cryptoSalt = genSaltSync(
            Number(process.env.CRYPT_SALT || '10')
        );

        const loginData = new Login();
        // TODO ENCODEAR PASSWORD
        loginData.password = hashSync(password, cryptoSalt);
        loginData.correoElectronico = correoElectronico;
        loginData.tipoUsuario = TYPE.CLIENTE.toString();
        loginData.status = 1;

        const userData = new Usuario();
        userData.nombreCliente = nombreCliente;
        userData.primerApel = primerApel;
        userData.segundoApel = segundoApel;
        userData.alias = alias;
        userData.fechaNac = fechaNac;
        userData.telCel = telCel;
        userData.pin = Number( pin);
        userData.fechaAlta = new Date();
        userData.statusNivel = LEVEL.PRINCIPIANTE.toString();
        userData.comision = 10;
        userData.experiencia = '0';
        userData.nivel = '1';
        userData.idLogin2 = loginData;

        let userCreated: Usuario;
        try {
            this.LOG.info('Creando user');
            userCreated = await this.USER_SERVICE.save(userData);
        } catch (error) {
            this.LOG.error(error);
            return res.status(500).json({ message: 'Usuario No creado' });
        }

        try{
            this.LOG.info('Registrando Datos en OpenPay');
            const oPay = new openpay(
                process.env.OPENPAY_ID,
                process.env.OPENPAY_SECRET,
                process.env.OPENPAY_PRODUCTION === 'true');

            const oPayCustomerData = {
                'external_id' : userCreated.idUsuario,
                'name' : userCreated.nombreCliente,
                'last_name' : `${userCreated.primerApel} ${userCreated.segundoApel}`,
                'email' : userCreated.idLogin2.correoElectronico,
                'phone_number' : userCreated.telCel
            };
            const createCustomer = promisify(oPay.customers.create);
            const customerCreated = await createCustomer(oPayCustomerData);
            userCreated.idOpenpay = customerCreated.id;
            await this.USER_SERVICE.save(userCreated);
        } catch (e) {
            this.LOG.error(e);
            return res.status(202).json({message: 'Usuario Creado, datos pasarela no validos'})
        }

        return res.status(201).json({ message: 'Usuario creado' });
    }

    async updateDataApp(req: Request, res: Response): Promise<Response> {
        // TODO VALIDATE ID LOGIN FROM JWT
        const {experiencia, bloqueado, userSession: user} :
            {experiencia: string, bloqueado: boolean, userSession: Usuario} =req.body;

        try {
            if(experiencia){
                user.experiencia = user.experiencia
                    ? `${ Number(user.experiencia) + Number(experiencia)}`
                    : experiencia;
            }
            if (typeof (bloqueado) !== "undefined" ){
                user.idLogin2.status = bloqueado ? 1 : 0;
            }

            await this.USER_SERVICE.save(user);

        }catch (e) {

            this.LOG.error(e);
            return res.status(500).json({
                message: 'No se pudo actualizar los datos'
            })

        }

        return res.json({
            message: 'Datos Actualizados',
            data: {
                experiencia: user.experiencia,
                bloqueado: user.idLogin2.status
            }

        });
    }

    async updateInfo(req: Request, res: Response): Promise<Response> {
        const {
            nombreCliente,
            primerApel,
            segundoApel,
            fechaNac,
            telCel,
            pais,
            ciudad,
            ocupacion,
            userSession: user
        }:{
            nombreCliente: string | undefined,
            primerApel: string | undefined,
            segundoApel: string | undefined,
            fechaNac: string | undefined,
            telCel: string | undefined,
            pais: string | undefined,
            ciudad:string | undefined,
            ocupacion: string | undefined,
            userSession: Usuario
        } = req.body;

        try {

            Object.assign(user,{
                nombreCliente,
                primerApel,
                segundoApel,
                fechaNac,
                telCel,
                pais,
                ciudad,
                ocupacion
            });
            await this.USER_SERVICE.save(user);

        }catch (e) {

            this.LOG.error(e);
            res.status(500).json({
                message: 'No se pudo actualizar los datos',
            });

        }

        return res.json({
            message: 'Información de usuario actualizada',
            data: {
                nombreCliente: user.nombreCliente,
                primerApel: user.primerApel,
                segundoApel: user.segundoApel,
                fechaNac: user.fechaNac,
                telCel: user.telCel,
                pais: user.pais,
                ciudad: user.ciudad,
                ocupacion: user.ocupacion
            }
        });
    }

    async updateAccount(req: Request, res: Response): Promise<Response> {
        const {
            alias,
            correoElectronico,
            pin,
            imageUri,
            correoAlternativo,
            password,
            oldPassword,
            userSession: user
        } : {
            alias: string | undefined,
            correoElectronico: string | undefined,
            pin: string | undefined,
            imageUri: string | undefined,
            correoAlternativo: string | undefined,
            password: string | undefined,
            oldPassword: string | undefined,
            userSession: Usuario
        } = req.body;

        try {
            if (correoElectronico){
                const emailFound = await this.LOGIN_SERVICE.findOne({
                    where: {correoElectronico}
                });

                if (emailFound) {
                    return res.status(200).json({
                        message: 'Correo ya existente'
                    });
                }

                user.idLogin2.correoElectronico = correoElectronico;
            }

            if (alias){
                const aliasFound = await this.USER_SERVICE.findOne({
                    where: {alias}
                });
                if (aliasFound) {
                    return res.status(200).json({
                        message: 'Alias ya existente'
                    });
                }

                user.alias=alias
            }

            if (oldPassword && password){

                if (! await compare(oldPassword,user.idLogin2.password)){
                    return res.status(403).json({
                        message : 'Password anterior incorrecto'
                    });
                }

                const cryptoSalt = genSaltSync(
                    Number(process.env.CRYPT_SALT || '10')
                );

                user.idLogin2.password = hashSync(password, cryptoSalt);
            }

            Object.assign(user,{
                pin,
                imageUri,
                correoAlternativo,
            });

            await this.USER_SERVICE.save(user);

        } catch (e) {
            this.LOG.error(e);
            res.status(500).json({
                message: 'No se pudo actualizar los datos',
            });
        }

        return res.json({
            message: 'Cuenta de usuario actualizada',
            data:{
                alias: user.alias,
                imageUri: user.imageUri,
                correoAlternativo: user.correoAlternativo,
                correoElectronico: user.idLogin2.correoElectronico
            }
        });
    }

    async getUserId(req: Request, res: Response): Promise<Response> {
        // TODO VALIDATE ID LOGIN FROM JWT
        const {userSession} :
            {userSession: Usuario}= req.body;


        const user = await this.USER_SERVICE.findOne({
            where: {
                idUsuario: userSession.idUsuario
            },
            relations: ['idLogin2','juegoDeportes','juegoDeportes.idPartido2','juegoPersonalizados']
        });

        const pedingGames = user?.juegoDeportes
            .filter(game =>
                game.idPartido2.status === STATUS.PENDIENTE
            ).length;

        const pendigCustomGames = user?.juegoPersonalizados
            .filter(game =>
                game.estadoJuego === STATUS.PENDIENTE
            ).length;

        const userInfo = {} as UserDTO ;

        Object.assign(userInfo,{
            nombre: user?.nombreCliente,
            paterno: user?.primerApel,
            materno: user?.segundoApel,
            alias: user?.alias,
            nacimiento: user?.fechaNac,
            telefonico: user?.telCel,
            email: user?.idLogin2.correoElectronico,
            emailAlternativo: user?.correoAlternativo,
            imageUri: user?.imageUri,
            pais:user?.pais,
            ciudad: user?.ciudad,
            ocupacion: user?.ocupacion,
            experiencia: user?.experiencia,
            juegosPartidosPendientes: pedingGames,
            juegosPersonalizadosPendientes: pendigCustomGames
        });

        return res.status(200).json({userInfo});
    }
}
