import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn
} from "typeorm";
import { Usuario } from "./Usuario";

@Index("fk_catalogo_direcciones_usuario", ["idUsuario"], {})
@Entity("catalogo_direcciones", { schema: "gobeeyond_develop" })
export class CatalogoDirecciones {
  @PrimaryGeneratedColumn({
    type: "int",
    name: "id_catalogo_direcciones",
    unsigned: true
  })
  public idCatalogoDirecciones?: number;

  @Column("varchar", { name: "pais", length: 50 })
  public pais?: string;

  @Column("varchar", { name: "Nombre completo", length: 50 })
  public nombreCompleto?: string;

  @Column("varchar", { name: "calle_direccion", length: 100 })
  public calleDireccion?: string;

  @Column("varchar", { name: "departamento", length: 50 })
  public departamento?: string;

  @Column("varchar", { name: "ciudad", length: 50 })
  public ciudad?: string;

  @Column("varchar", { name: "estado", length: 50 })
  public estado?: string;

  @Column("varchar", { name: "CPP", length: 100 })
  public cpp?: string;

  @Column("varchar", { name: "telefono_celular", length: 20 })
  public telefonoCelular?: string;

  @Column("int", { name: "id_usuario" })
  public idUsuario?: number;

  @ManyToOne(
    () => Usuario,
    usuario => usuario.catalogoDirecciones,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "id_usuario", referencedColumnName: "idUsuario" }])
  public idUsuario2?: Usuario;

  public constructor(init?: Partial<CatalogoDirecciones>) {
    Object.assign(this, init);
  }
}
