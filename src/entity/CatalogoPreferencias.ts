import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn
} from "typeorm";
import { Usuario } from "./Usuario";

@Index("fk_catalogo_preferencias_usuario_1", ["idUsuario"], {})
@Entity("catalogo_preferencias", { schema: "gobeeyond_develop" })
export class CatalogoPreferencias {
  @PrimaryGeneratedColumn({ type: "int", name: "Id_catalogo_preferencias" })
  public idCatalogoPreferencias?: number;

  @Column("varchar", { name: "Deporte", length: 50 })
  public deporte?: string;

  @Column("varchar", { name: "Deportista", length: 50 })
  public deportista?: string;

  @Column("int", { name: "id_usuario" })
  public idUsuario?: number;

  @ManyToOne(
    () => Usuario,
    usuario => usuario.catalogoPreferencias,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "id_usuario", referencedColumnName: "idUsuario" }])
  public idUsuario2?: Usuario;

  public constructor(init?: Partial<CatalogoPreferencias>) {
    Object.assign(this, init);
  }
}
