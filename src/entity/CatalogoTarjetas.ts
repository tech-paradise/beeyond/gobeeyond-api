import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn
} from "typeorm";
import { Usuario } from "./Usuario";

@Index("fk_catalogo_tarjetas_usuario_1", ["idUsuario"], {})
@Entity("catalogo_tarjetas", { schema: "gobeeyond_develop" })
export class CatalogoTarjetas {
  @PrimaryGeneratedColumn({ type: "int", name: "Id_catalogo_tarjetas" })
  public idCatalogoTarjetas?: number;

  @Column("int", { name: "Id_usuario" })
  public idUsuario?: number;

  @Column("varchar", { name: "token_id", length: 50 })
  public tokenId?: string;

  @ManyToOne(
    () => Usuario,
    usuario => usuario.catalogoTarjetas,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "Id_usuario", referencedColumnName: "idUsuario" }])
  public idUsuario2?: Usuario;

  public constructor(init?: Partial<CatalogoTarjetas>) {
    Object.assign(this, init);
  }
}
