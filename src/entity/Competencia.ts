import { Column, Entity, OneToMany } from "typeorm";
import { JuegoPersonalizado } from "./JuegoPersonalizado";

@Entity("competencia", { schema: "gobeeyond_develop" })
export class Competencia {
  @Column("int", { primary: true, name: "id_competencia" })
  public idCompetencia?: number;

  @Column("varchar", { name: "descripcion", length: 50 })
  public descripcion?: string;

  @Column("varchar", { name: "resultado", length: 50 })
  public resultado?: string;

  @Column("varchar", { name: "estado_juego", length: 50 })
  public estadoJuego?: string;

  @OneToMany(
    () => JuegoPersonalizado,
    juegoPersonalizado => juegoPersonalizado.idCompetencia2
  )
  public juegoPersonalizados?: JuegoPersonalizado[];

  public constructor(init?: Partial<Competencia>) {
    Object.assign(this, init);
  }
}
