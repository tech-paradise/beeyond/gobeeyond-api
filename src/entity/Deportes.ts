import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity("deportes", { schema: "gobeeyond_develop" })
export class Deportes {
  @PrimaryGeneratedColumn({ type: "int", name: "Id_deporte" })
  public idDeporte?: number;

  @Column("varchar", { name: "Nombre_deporte", nullable: true, length: 255 })
  public nombreDeporte?: string | null;

  @Column("varchar", { name: "NombreEquipo", nullable: true, length: 255 })
  public nombreEquipo?: string | null;

  @Column("varchar", { name: "NombreDeportista", nullable: true, length: 255 })
  public nombreDeportista?: string | null;

  public constructor(init?: Partial<Deportes>) {
    Object.assign(this, init);
  }
}
