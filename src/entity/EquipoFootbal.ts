import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { PartidoFootball } from "./PartidoFootball";

@Entity("equipo_footbal", { schema: "gobeeyond_develop" })
export class EquipoFootbal {
  @PrimaryGeneratedColumn({ type: "int", name: "id_equipo" })
  public idEquipo?: number;

  @Column("int", { name: "id_api" })
  public idApi?: number;

  @Column("varchar", { name: "nombre", length: 50 })
  public nombre?: string;

  @Column("varchar", { name: "logo", length: 50 })
  public logo?: string;

  @OneToMany(
    () => PartidoFootball,
    partidoFootball => partidoFootball.idEquipoLocal2
  )
  public partidoFootballs?: PartidoFootball[];

  @OneToMany(
    () => PartidoFootball,
    partidoFootball => partidoFootball.idEquipoRemoto2
  )
  public partidoFootballs2?: PartidoFootball[];

  public constructor(init?: Partial<EquipoFootbal>) {
    Object.assign(this, init);
  }
}
