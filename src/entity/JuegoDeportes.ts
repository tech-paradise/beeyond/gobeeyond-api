import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn
} from "typeorm";
import { PartidoFootball } from "./PartidoFootball";
import { Retos } from "./Retos";
import { Ticket } from "./Ticket";
import { Usuario } from "./Usuario";
import { MontoRecabado } from "./MontoRecabado";
import { PremioMonetarioPagado } from "./PremioMonetarioPagado";

@Index("fk_juego_deportes_usuario_1", ["idUsuario1"], {})
@Index("fk_juego_deportes_partido_1", ["idPartido"], {})
@Index("fk_juego_deportes_retos_1", ["idRetos"], {})
@Index("fk_juego_deportes_ticket_1", ["idTicket"], {})
@Entity("juego_deportes", { schema: "gobeeyond_develop" })
export class JuegoDeportes {
  @PrimaryGeneratedColumn({ type: "int", name: "Id_juego_deportes" })
  public idJuegoDeportes?: number;

  @Column("varchar", { name: "tipo_apuesta", length: 40 })
  public tipoApuesta: string;

  @Column("varchar", { name: "tipo_premio", length: 40 })
  public tipoPremio: string;

  @Column("varchar", { name: "descripcion", length: 80 })
  public descripcion: string;

  @Column("timestamp", { name: "fecha", default: () => "CURRENT_TIMESTAMP" })
  public fecha: Date;

  @Column("int", { name: "id_ganador" })
  public idGanador?: number;

  @Column("int", { name: "id_retos" })
  public idRetos?: number;

  @Column("int", { name: "id_usuario1" })
  public idUsuario1: number;

  @Column("int", { name: "id_usuario2" })
  public idUsuario2: number;

  @Column("int", { name: "id_ticket" })
  public idTicket: number;

  @Column("int", { name: "id_partido" })
  public idPartido: number;

  @ManyToOne(
    () => PartidoFootball,
    partidoFootball => partidoFootball.juegoDeportes,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "id_partido", referencedColumnName: "idPartido" }])
  public idPartido2: PartidoFootball;

  @ManyToOne(
    () => Retos,
    retos => retos.juegoDeportes,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "id_retos", referencedColumnName: "idRetos" }])
  public idRetos2: Retos;

  @ManyToOne(
    () => Ticket,
    ticket => ticket.juegoDeportes,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "id_ticket", referencedColumnName: "idTicket" }])
  public idTicket2: Ticket;

  @ManyToOne(
    () => Usuario,
    usuario => usuario.juegoDeportes,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT", cascade: true }
  )
  @JoinColumn([{ name: "id_usuario1", referencedColumnName: "idUsuario" }])
  public idUsuario?: Usuario;

  @OneToMany(
    () => MontoRecabado,
    montoRecabado => montoRecabado.idJuegoDeportes2
  )
  public montoRecabados?: MontoRecabado[];

  @OneToMany(
    () => PremioMonetarioPagado,
    premioMonetarioPagado => premioMonetarioPagado.idJuegoDeportes2
  )
  public premioMonetarioPagados?: PremioMonetarioPagado[];

  public constructor(init?: Partial<JuegoDeportes>) {
    Object.assign(this, init);
  }
}
