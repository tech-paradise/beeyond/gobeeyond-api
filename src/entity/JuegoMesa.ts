import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { UsuarioJuegoMesa } from "./UsuarioJuegoMesa";

@Entity("juego__mesa", { schema: "gobeeyond_develop" })
export class JuegoMesa {
  @PrimaryGeneratedColumn({ type: "int", name: "id_juego_mesa" })
  public idJuegoMesa?: number;

  @Column("int", { name: "Contador_apuestas_A", nullable: true })
  public contadorApuestasA?: number | null;

  @Column("int", { name: "Contador_apuestas_B", nullable: true })
  public contadorApuestasB?: number | null;

  @Column("int", { name: "Contador_apuestas_C", nullable: true })
  public contadorApuestasC?: number | null;

  @Column("float", { name: "Total_A", nullable: true, precision: 8, scale: 2 })
  public totalA?: number | null;

  @Column("float", { name: "Total_B", nullable: true, precision: 8, scale: 2 })
  public totalB?: number | null;

  @Column("float", { name: "Total_C", nullable: true, precision: 8, scale: 2 })
  public totalC?: number | null;

  @Column("varchar", {
    name: "Ganador_equipo_o_deportista",
    nullable: true,
    length: 50
  })
  public ganadorEquipoODeportista?: string | null;

  @Column("int", { name: "Id_usuario_juego_mesa", nullable: true })
  public idUsuarioJuegoMesa?: number | null;

  @OneToMany(
    () => UsuarioJuegoMesa,
    usuarioJuegoMesa => usuarioJuegoMesa.idJuegoMesa2
  )
  public usuarioJuegoMesas?: UsuarioJuegoMesa[];

  public constructor(init?: Partial<JuegoMesa>) {
    Object.assign(this, init);
  }
}
