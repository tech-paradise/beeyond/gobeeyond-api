import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn
} from "typeorm";
import { Competencia } from "./Competencia";
import { Retos } from "./Retos";
import { Ticket } from "./Ticket";
import { Usuario } from "./Usuario";
import { MontoRecabado } from "./MontoRecabado";
import { PremioMonetarioPagado } from "./PremioMonetarioPagado";

@Index("fk_juego_personalizado_retos_1", ["idRetos"], {})
@Index("fk_juego_personalizado_usuario_1", ["idUsuario_1"], {})
@Index("fk_juego_personalizado_ticket_1", ["idTicket"], {})
@Index("fk_juego_personalizado_competencia_1", ["idCompetencia"], {})
@Entity("juego_personalizado", { schema: "gobeeyond_develop" })
export class JuegoPersonalizado {
  @PrimaryGeneratedColumn({ type: "int", name: "id_juego_personalizado" })
  public idJuegoPersonalizado?: number;

  @Column("varchar", { name: "tipo_apuesta", length: 40 })
  public tipoApuesta?: string;

  @Column("varchar", { name: "tipo_premio", length: 40 })
  public tipoPremio?: string;

  @Column("varchar", { name: "descripcion", length: 40 })
  public descripcion?: string;

  @Column("varchar", { name: "estado_juego", length: 40 })
  public estadoJuego?: string;

  @Column("timestamp", { name: "fecha_personalizado", nullable: true })
  public fechaPersonalizado?: Date | null;

  @Column("varchar", { name: "resultado", nullable: true, length: 50 })
  public resultado?: string | null;

  @Column("int", { name: "id_ganador", nullable: true })
  public idGanador?: number | null;

  @Column("int", { name: "id_retos", nullable: true })
  public idRetos?: number | null;

  @Column("int", { name: "id_ticket", nullable: true })
  public idTicket?: number | null;

  @Column("int", { name: "id_usuario_1" })
  public idUsuario_1?: number;

  @Column("int", { name: "id_usuario_2" })
  public idUsuario_2?: number;

  @Column("int", { name: "arbitro" })
  public arbitro?: number;

  @Column("int", { name: "id_competencia" })
  public idCompetencia?: number;

  @ManyToOne(
    () => Competencia,
    competencia => competencia.juegoPersonalizados,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([
    { name: "id_competencia", referencedColumnName: "idCompetencia" }
  ])
  public idCompetencia2?: Competencia;

  @ManyToOne(
    () => Retos,
    retos => retos.juegoPersonalizados,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "id_retos", referencedColumnName: "idRetos" }])
  public idRetos2?: Retos;

  @ManyToOne(
    () => Ticket,
    ticket => ticket.juegoPersonalizados,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "id_ticket", referencedColumnName: "idTicket" }])
  public idTicket2?: Ticket;

  @ManyToOne(
    () => Usuario,
    usuario => usuario.juegoPersonalizados,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "id_usuario_1", referencedColumnName: "idUsuario" }])
  public idUsuario?: Usuario;

  @OneToMany(
    () => MontoRecabado,
    montoRecabado => montoRecabado.idJuegoPersonalizado2
  )
  public montoRecabados?: MontoRecabado[];

  @OneToMany(
    () => PremioMonetarioPagado,
    premioMonetarioPagado => premioMonetarioPagado.idJuegoPersonalizado2
  )
  public premioMonetarioPagados?: PremioMonetarioPagado[];

  public constructor(init?: Partial<JuegoPersonalizado>) {
    Object.assign(this, init);
  }
}
