import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { PartidoFootball } from "./PartidoFootball";

@Entity("liga_football", { schema: "gobeeyond_develop" })
export class LigaFootball {
  @PrimaryGeneratedColumn({ type: "int", name: "id_liga" })
  public idLiga?: number;

  @Column("int", { name: "id_api" })
  public idApi?: number;

  @Column("varchar", { name: "nombre", length: 50 })
  public nombre?: string;

  @Column("varchar", { name: "pais", length: 50 })
  public pais?: string;

  @Column("varchar", { name: "temporada", length: 11 })
  public temporada?: string;

  @Column("date", { name: "fecha_inicio" })
  public fechaInicio?: string;

  @Column("date", { name: "fecha_final" })
  public fechaFinal?: string;

  @Column("varchar", { name: "logo", length: 50 })
  public logo?: string;

  @Column("varchar", { name: "flag", length: 50 })
  public flag?: string;

  @Column("tinyint", { name: "activo" })
  public activo?: number;

  @Column("varchar", { name: "tipo", length: 50 })
  public tipo?: string;

  @OneToMany(
    () => PartidoFootball,
    partidoFootball => partidoFootball.idLigaFootball2
  )
  public partidoFootballs?: PartidoFootball[];

  public constructor(init?: Partial<LigaFootball>) {
    Object.assign(this, init);
  }
}
