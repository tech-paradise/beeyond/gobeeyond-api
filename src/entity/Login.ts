import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Proveedores } from "./Proveedores";
import { Usuario } from "./Usuario";

@Entity("login", { schema: "gobeeyond_develop" })
export class Login {
  @PrimaryGeneratedColumn({ type: "int", name: "Id_login" })
  public idLogin: number;

  @Column("varchar", { name: "Password", length: 255 })
  public password: string;

  @Column("varchar", { name: "Correo_electronico", length: 50 })
  public correoElectronico: string;

  @Column("varchar", { name: "Tipo_usuario", length: 50 })
  public tipoUsuario: string;

  @Column("tinyint", { name: "status" })
  public status: number;

  @OneToMany(
    () => Proveedores,
    proveedores => proveedores.idLogin2
  )
  public proveedores: Proveedores[];

  @OneToMany(
    () => Usuario,
    usuario => usuario.idLogin2
  )
  public usuarios: Usuario[];
  }

