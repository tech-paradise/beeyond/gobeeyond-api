import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { JuegoDeportes } from "./JuegoDeportes";
import { JuegoPersonalizado } from "./JuegoPersonalizado";
import { Usuario } from "./Usuario";

@Index("fk_monto_recabado_usuario_1", ["idUsuario"], {})
@Index("fk_monto_recabado_juego_deportes_1", ["idJuegoDeportes"], {})
@Index("fk_monto_recabado_juego_personalizado_1", ["idJuegoPersonalizado"], {})
@Entity("monto_recabado", { schema: "gobeeyond_develop" })
export class MontoRecabado {
  @Column("int", { primary: true, name: "id_monto_recabado" })
  public idMontoRecabado?: number;

  @Column("float", { name: "monto_recabado", precision: 9, scale: 2 })
  public montoRecabado?: number;

  @Column("varchar", { name: "status", length: 11 })
  public status?: string;

  @Column("varchar", { name: "folio_openpay", length: 80 })
  public folioOpenpay?: string;

  @Column("int", { name: "id_usuario" })
  public idUsuario?: number;

  @Column("int", { name: "id_juego_deportes" })
  public idJuegoDeportes?: number;

  @Column("int", { name: "id_juego_personalizado" })
  public idJuegoPersonalizado?: number;

  @ManyToOne(
    () => JuegoDeportes,
    juegoDeportes => juegoDeportes.montoRecabados,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([
    { name: "id_juego_deportes", referencedColumnName: "idJuegoDeportes" }
  ])
  public idJuegoDeportes2?: JuegoDeportes;

  @ManyToOne(
    () => JuegoPersonalizado,
    juegoPersonalizado => juegoPersonalizado.montoRecabados,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([
    {
      name: "id_juego_personalizado",
      referencedColumnName: "idJuegoPersonalizado"
    }
  ])
  public idJuegoPersonalizado2?: JuegoPersonalizado;

  @ManyToOne(
    () => Usuario,
    usuario => usuario.montoRecabados,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "id_usuario", referencedColumnName: "idUsuario" }])
  public idUsuario2?: Usuario;

  public constructor(init?: Partial<MontoRecabado>) {
    Object.assign(this, init);
  }
}
