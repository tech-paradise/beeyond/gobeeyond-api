import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn
} from "typeorm";
import { Proveedores } from "./Proveedores";

@Index("fk_pago_proveedores_proveedores_1", ["idProveedor"], {})
@Entity("pago_proveedores", { schema: "gobeeyond_develop" })
export class PagoProveedores {
  @PrimaryGeneratedColumn({ type: "int", name: "Id_pago_proveedores" })
  public idPagoProveedores?: number;

  @Column("float", { name: "monto", precision: 9, scale: 2 })
  public monto?: number;

  @Column("varchar", { name: "folio_openpay", length: 100 })
  public folioOpenpay?: string;

  @Column("timestamp", { name: "fecha", default: () => "CURRENT_TIMESTAMP" })
  public fecha?: Date;

  @Column("int", { name: "Id_proveedor" })
  public idProveedor?: number;

  @Column("int", { name: "id_ticket" })
  public idTicket?: number;

  @ManyToOne(
    () => Proveedores,
    proveedores => proveedores.pagoProveedores,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "Id_proveedor", referencedColumnName: "idProveedor" }])
  public idProveedor2?: Proveedores;

  public constructor(init?: Partial<PagoProveedores>) {
    Object.assign(this, init);
  }
}
