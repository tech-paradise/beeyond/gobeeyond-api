import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { JuegoDeportes } from "./JuegoDeportes";

@Entity("partido", { schema: "gobeeyond_develop" })
export class Partido {
  @PrimaryGeneratedColumn({ type: "int", name: "id_partido" })
  public idPartido?: number;

  @Column("varchar", { name: "equipo_1", length: 50 })
  public equipo_1?: string;

  @Column("varchar", { name: "equipo2", length: 50 })
  public equipo2?: string;

  @Column("varchar", { name: "marcador", length: 40 })
  public marcador?: string;

  @Column("varchar", { name: "status", length: 50 })
  public status?: string;

  @Column("varchar", { name: "resultado", length: 10 })
  public resultado?: string;

  @OneToMany(
    () => JuegoDeportes,
    juegoDeportes => juegoDeportes.idPartido2
  )
  public juegoDeportes?: JuegoDeportes[];

  public constructor(init?: Partial<Partido>) {
    Object.assign(this, init);
  }
}
