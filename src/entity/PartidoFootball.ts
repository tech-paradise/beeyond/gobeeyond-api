import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn
} from "typeorm";
import { JuegoDeportes } from "./JuegoDeportes";
import { EquipoFootbal } from "./EquipoFootbal";
import { LigaFootball } from "./LigaFootball";

@Index("fk_id_liga_id_liga_football", ["idLigaFootball"], {})
@Index("fk_equipo_local_id_equipo", ["idEquipoLocal"], {})
@Index("fk_equipo_remoto_id_equipo", ["idEquipoRemoto"], {})
@Entity("partido_football", { schema: "gobeeyond_develop" })
export class PartidoFootball {
  @PrimaryGeneratedColumn({ type: "int", name: "id_partido" })
  public idPartido?: number;

  @Column("int", { name: "id_api" })
  public idApi?: number;

  @Column("int", { name: "id_liga_football" })
  public idLigaFootball?: number;

  @Column("timestamp", {
    name: "fecha_evento",
    default: () => "CURRENT_TIMESTAMP"
  })
  public fechaEvento?: Date;

  @Column("varchar", { name: "status", length: 50 })
  public status?: string;

  @Column("bigint", { name: "tiempo_transcurrido", unsigned: true })
  public tiempoTranscurrido?: string;

  @Column("int", { name: "id_equipo_local" })
  public idEquipoLocal?: number;

  @Column("int", { name: "id_equipo_remoto" })
  public idEquipoRemoto?: number;

  @Column("int", { name: "goles_equipo_local" })
  public golesEquipoLocal?: number;

  @Column("int", { name: "goles_equipo_remoto" })
  public golesEquipoRemoto?: number;

  @Column("varchar", { name: "equipo_ganador", length: 50 })
  public equipoGanador?: string;

  @Column("varchar", { name: "estadio", length: 50 })
  public estadio?: string;

  @Column("varchar", { name: "resultado", length: 10 })
  public resultado?: string;

  @Column("varchar", { name: "ronda", length: 50 })
  public ronda?: string;

  @OneToMany(
    () => JuegoDeportes,
    juegoDeportes => juegoDeportes.idPartido2
  )
  public juegoDeportes?: JuegoDeportes[];

  @ManyToOne(
    () => EquipoFootbal,
    equipoFootbal => equipoFootbal.partidoFootballs,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "id_equipo_local", referencedColumnName: "idEquipo" }])
  public idEquipoLocal2?: EquipoFootbal;

  @ManyToOne(
    () => EquipoFootbal,
    equipoFootbal => equipoFootbal.partidoFootballs2,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "id_equipo_remoto", referencedColumnName: "idEquipo" }])
  public idEquipoRemoto2?: EquipoFootbal;

  @ManyToOne(
    () => LigaFootball,
    ligaFootball => ligaFootball.partidoFootballs,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "id_liga_football", referencedColumnName: "idLiga" }])
  public idLigaFootball2?: LigaFootball;

  public constructor(init?: Partial<PartidoFootball>) {
    Object.assign(this, init);
  }
}
