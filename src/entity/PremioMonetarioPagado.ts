import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { JuegoDeportes } from "./JuegoDeportes";
import { JuegoPersonalizado } from "./JuegoPersonalizado";
import { Usuario } from "./Usuario";

@Index("fk_premio_monetario_pagado_juego_deportes_1", ["idJuegoDeportes"], {})
@Index(
  "fk_premio_monetario_pagado_juego_personalizado_1",
  ["idJuegoPersonalizado"],
  {}
)
@Index("fk_premio_monetario_pagado_usuario_1", ["idUsuario"], {})
@Entity("premio_monetario_pagado", { schema: "gobeeyond_develop" })
export class PremioMonetarioPagado {
  @Column("int", { primary: true, name: "id_premio_monetario_pagado" })
  public idPremioMonetarioPagado?: number;

  @Column("float", { name: "monto", precision: 9, scale: 2 })
  public monto?: number;

  @Column("varchar", { name: "status", length: 11 })
  public status?: string;

  @Column("varchar", { name: "folio_openpay", length: 80 })
  public folioOpenpay?: string;

  @Column("int", { name: "id_usuario" })
  public idUsuario?: number;

  @Column("int", { name: "id_juego_deportes" })
  public idJuegoDeportes?: number;

  @Column("int", { name: "id_juego_personalizado" })
  public idJuegoPersonalizado?: number;

  @ManyToOne(
    () => JuegoDeportes,
    juegoDeportes => juegoDeportes.premioMonetarioPagados,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([
    { name: "id_juego_deportes", referencedColumnName: "idJuegoDeportes" }
  ])
  public idJuegoDeportes2?: JuegoDeportes;

  @ManyToOne(
    () => JuegoPersonalizado,
    juegoPersonalizado => juegoPersonalizado.premioMonetarioPagados,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([
    {
      name: "id_juego_personalizado",
      referencedColumnName: "idJuegoPersonalizado"
    }
  ])
  public idJuegoPersonalizado2?: JuegoPersonalizado;

  @ManyToOne(
    () => Usuario,
    usuario => usuario.premioMonetarioPagados,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "id_usuario", referencedColumnName: "idUsuario" }])
  public idUsuario2?: Usuario;

  public constructor(init?: Partial<PremioMonetarioPagado>) {
    Object.assign(this, init);
  }
}
