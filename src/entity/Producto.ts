import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn
} from "typeorm";
import { Proveedores } from "./Proveedores";
import { Ticket } from "./Ticket";

@Index("fk_producto_proveedores_1", ["idProveedor"], {})
@Entity("producto", { schema: "gobeeyond_develop" })
export class Producto {
  @PrimaryGeneratedColumn({ type: "int", name: "Id_producto" })
  public idProducto?: number;

  @Column("varchar", { name: "SKU", length: 50 })
  public sku?: string;

  @Column("varchar", { name: "Descripcion", length: 100 })
  public descripcion?: string;

  @Column("float", { name: "Precio", precision: 5, scale: 2 })
  public precio?: number;

  @Column("varchar", { name: "disponibilidad", length: 50 })
  public disponibilidad?: string;

  @Column("int", { name: "Id_proveedor" })
  public idProveedor?: number;

  @ManyToOne(
    () => Proveedores,
    proveedores => proveedores.productos,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "Id_proveedor", referencedColumnName: "idProveedor" }])
  public idProveedor2?: Proveedores;

  @OneToMany(
    () => Ticket,
    ticket => ticket.idProducto2
  )
  public tickets?: Ticket[];

  public constructor(init?: Partial<Producto>) {
    Object.assign(this, init);
  }
}
