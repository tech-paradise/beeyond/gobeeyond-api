import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn
} from "typeorm";
import { PagoProveedores } from "./PagoProveedores";
import { Producto } from "./Producto";
import { Login } from "./Login";

@Index("fk_proveedores_login_1", ["idLogin"], {})
@Entity("proveedores", { schema: "gobeeyond_develop" })
export class Proveedores {
  @PrimaryGeneratedColumn({ type: "int", name: "Id_proveedor" })
  public idProveedor?: number;

  @Column("varchar", { name: "Nombre_proveedor", length: 50 })
  public nombreProveedor?: string;

  @Column("varchar", { name: "cc_clabe", length: 50 })
  public ccClabe?: string;

  @Column("varchar", { name: "cc_cuenta", length: 50 })
  public ccCuenta?: string;

  @Column("varchar", { name: "Direccion_proveedor", length: 120 })
  public direccionProveedor?: string;

  @Column("varchar", { name: "telefono", length: 20 })
  public telefono?: string;

  @Column("int", { name: "codigo_postal" })
  public codigoPostal?: number;

  @Column("timestamp", {
    name: "Fecha_registro",
    default: () => "CURRENT_TIMESTAMP"
  })
  public fechaRegistro?: Date;

  @Column("int", { name: "Id_login" })
  public idLogin?: number;

  @OneToMany(
    () => PagoProveedores,
    pagoProveedores => pagoProveedores.idProveedor2
  )
  public pagoProveedores?: PagoProveedores[];

  @OneToMany(
    () => Producto,
    producto => producto.idProveedor2
  )
  public productos?: Producto[];

  @ManyToOne(
    () => Login,
    login => login.proveedores,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "Id_login", referencedColumnName: "idLogin" }])
  public idLogin2?: Login;

  public constructor(init?: Partial<Proveedores>) {
    Object.assign(this, init);
  }
}
