import { Column, Entity, OneToMany } from "typeorm";
import { JuegoDeportes } from "./JuegoDeportes";
import { JuegoPersonalizado } from "./JuegoPersonalizado";

@Entity("retos", { schema: "gobeeyond_develop" })
export class Retos {
  @Column("int", { primary: true, name: "id_retos" })
  public idRetos?: number;

  @Column("varchar", { name: "nombre", length: 40 })
  public nombre?: string;

  @Column("varchar", { name: "descripcion", length: 80 })
  public descripcion?: string;

  @OneToMany(
    () => JuegoDeportes,
    juegoDeportes => juegoDeportes.idRetos2
  )
  public juegoDeportes?: JuegoDeportes[];

  @OneToMany(
    () => JuegoPersonalizado,
    juegoPersonalizado => juegoPersonalizado.idRetos2
  )
  public juegoPersonalizados?: JuegoPersonalizado[];

  public constructor(init?: Partial<Retos>) {
    Object.assign(this, init);
  }
}
