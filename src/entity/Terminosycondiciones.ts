import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn
} from "typeorm";
import { Usuario } from "./Usuario";

@Index("fk_terminosycondiciones_usuario_1", ["idUsuario"], {})
@Entity("terminosycondiciones", { schema: "gobeeyond_develop" })
export class Terminosycondiciones {
  @PrimaryGeneratedColumn({ type: "int", name: "id_terminos" })
  public idTerminos?: number;

  @Column("varchar", { name: "URI_terminos_condiciones", length: 255 })
  public uriTerminosCondiciones?: string;

  @Column("tinyint", { name: "aceptarterminos", nullable: true })
  public aceptarterminos?: number | null;

  @Column("timestamp", {
    name: "fecha_terminos",
    default: () => "CURRENT_TIMESTAMP"
  })
  public fechaTerminos?: Date;

  @Column("int", { name: "Id_usuario" })
  public idUsuario?: number;

  @ManyToOne(
    () => Usuario,
    usuario => usuario.terminosycondiciones,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "Id_usuario", referencedColumnName: "idUsuario" }])
  public idUsuario2?: Usuario;

  public constructor(init?: Partial<Terminosycondiciones>) {
    Object.assign(this, init);
  }
}
