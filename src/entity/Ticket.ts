import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn
} from "typeorm";
import { JuegoDeportes } from "./JuegoDeportes";
import { JuegoPersonalizado } from "./JuegoPersonalizado";
import { Producto } from "./Producto";
import { Usuario } from "./Usuario";

@Index("fk_ticket_usuario_1", ["idUsuario"], {})
@Index("fk_ticket_producto_1", ["idProducto"], {})
@Entity("ticket", { schema: "gobeeyond_develop" })
export class Ticket {
  @PrimaryGeneratedColumn({ type: "int", name: "Id_ticket" })
  public idTicket?: number;

  @Column("varchar", { name: "NumTicket", length: 50 })
  public numTicket?: string;

  @Column("varchar", { name: "Tipo_apuesta", length: 50 })
  public tipoApuesta?: string;

  @Column("varchar", { name: "Tipo_premio", length: 50 })
  public tipoPremio?: string;

  @Column("varchar", { name: "codigo_qr", length: 255 })
  public codigoQr?: string;

  @Column("timestamp", {
    name: "Fecha_emision",
    default: () => "CURRENT_TIMESTAMP"
  })
  public fechaEmision?: Date;

  @Column("varchar", { name: "status", length: 50 })
  public status?: string;

  @Column("varchar", { name: "Nombre_usuario", length: 50 })
  public nombreUsuario?: string;

  @Column("int", { name: "id_producto" })
  public idProducto?: number;

  @Column("int", { name: "Id_usuario" })
  public idUsuario?: number;

  @OneToMany(
    () => JuegoDeportes,
    juegoDeportes => juegoDeportes.idTicket2
  )
  public juegoDeportes?: JuegoDeportes[];

  @OneToMany(
    () => JuegoPersonalizado,
    juegoPersonalizado => juegoPersonalizado.idTicket2
  )
  public juegoPersonalizados?: JuegoPersonalizado[];

  @ManyToOne(
    () => Producto,
    producto => producto.tickets,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "id_producto", referencedColumnName: "idProducto" }])
  public idProducto2?: Producto;

  @ManyToOne(
    () => Usuario,
    usuario => usuario.tickets,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "Id_usuario", referencedColumnName: "idUsuario" }])
  public idUsuario2?: Usuario;

  public constructor(init?: Partial<Ticket>) {
    Object.assign(this, init);
  }
}
