import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn
} from "typeorm";
import { CatalogoDirecciones } from "./CatalogoDirecciones";
import { CatalogoPreferencias } from "./CatalogoPreferencias";
import { CatalogoTarjetas } from "./CatalogoTarjetas";
import { JuegoDeportes } from "./JuegoDeportes";
import { JuegoPersonalizado } from "./JuegoPersonalizado";
import { MontoRecabado } from "./MontoRecabado";
import { PremioMonetarioPagado } from "./PremioMonetarioPagado";
import { Terminosycondiciones } from "./Terminosycondiciones";
import { Ticket } from "./Ticket";
import { Login } from "./Login";
import { UsuarioJuegoMesa } from "./UsuarioJuegoMesa";

@Index("fk_usuario_login_1", ["idLogin"], {})
@Entity("usuario", { schema: "gobeeyond_develop" })
export class Usuario {
  @PrimaryGeneratedColumn({ type: "int", name: "Id_usuario" })
  public idUsuario: number;

  @Column("varchar", { name: "Nombre_cliente", length: 50 })
  public nombreCliente?: string;

  @Column("varchar", { name: "Primer_apel", length: 50 })
  public primerApel?: string;

  @Column("varchar", { name: "Segundo_apel", nullable: true, length: 50 })
  public segundoApel?: string | null;

  @Column("varchar", { name: "Alias", nullable: true, length: 50 })
  public alias?: string | null;

  @Column("varchar", { name: "ImageURI", nullable: true, length: 255 })
  public imageUri?: string | null;

  @Column("date", { name: "Fecha_nac" })
  public fechaNac?: string;

  @Column("varchar", { name: "Tel_cel", length: 15 })
  public telCel?: string;

  @Column("int", { name: "Pin" })
  public pin?: number;

  @Column("varchar", { name: "Pais", nullable: true, length: 50 })
  public pais?: string | null;

  @Column("varchar", { name: "Estado", nullable: true, length: 50 })
  public estado?: string | null;

  @Column("varchar", { name: "Ciudad", nullable: true, length: 50 })
  public ciudad?: string | null;

  @Column("varchar", { name: "Experiencia", length: 50 })
  public experiencia?: string;

  @Column("varchar", { name: "Nivel", length: 50 })
  public nivel?: string;

  @Column("varchar", { name: "Status_nivel", length: 50 })
  public statusNivel?: string;

  @Column("float", { name: "Comision", nullable: true, precision: 4, scale: 2 })
  public comision?: number | null;

  @Column("timestamp", {
    name: "Fecha_alta",
    default: () => "CURRENT_TIMESTAMP"
  })
  public fechaAlta?: Date;

  @Column("varchar", { name: "Genero", nullable: true, length: 50 })
  public genero?: string | null;

  @Column("varchar", { name: "Ocupacion", nullable: true, length: 50 })
  public ocupacion?: string | null;

  @Column("varchar", { name: "Correo_alternativo", nullable: true, length: 50 })
  public correoAlternativo?: string | null;

  @Column("int", { name: "Id_login" })
  public idLogin?: number;

  @Column("varchar", { name: "id_openpay", nullable: true, length: 255 })
  public idOpenpay?: string | null;

  @OneToMany(
    () => CatalogoDirecciones,
    catalogoDirecciones => catalogoDirecciones.idUsuario2
  )
  public catalogoDirecciones: CatalogoDirecciones[];

  @OneToMany(
    () => CatalogoPreferencias,
    catalogoPreferencias => catalogoPreferencias.idUsuario2
  )
  public catalogoPreferencias: CatalogoPreferencias[];

  @OneToMany(
    () => CatalogoTarjetas,
    catalogoTarjetas => catalogoTarjetas.idUsuario2
  )
  public catalogoTarjetas: CatalogoTarjetas[];

  @OneToMany(
    () => JuegoDeportes,
    juegoDeportes => juegoDeportes.idUsuario
  )
  public juegoDeportes: JuegoDeportes[];

  @OneToMany(
    () => JuegoPersonalizado,
    juegoPersonalizado => juegoPersonalizado.idUsuario
  )
  public juegoPersonalizados: JuegoPersonalizado[];

  @OneToMany(
    () => MontoRecabado,
    montoRecabado => montoRecabado.idUsuario2
  )
  public montoRecabados: MontoRecabado[];

  @OneToMany(
    () => PremioMonetarioPagado,
    premioMonetarioPagado => premioMonetarioPagado.idUsuario2
  )
  public premioMonetarioPagados: PremioMonetarioPagado[];

  @OneToMany(
    () => Terminosycondiciones,
    terminosycondiciones => terminosycondiciones.idUsuario2
  )
  public terminosycondiciones: Terminosycondiciones[];

  @OneToMany(
    () => Ticket,
    ticket => ticket.idUsuario2
  )
  public tickets: Ticket[];

  @ManyToOne(
    () => Login,
    login => login.usuarios,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT", cascade: true}
  )
  @JoinColumn([{ name: "Id_login", referencedColumnName: "idLogin" }])
  public idLogin2: Login;

  @OneToMany(
    () => UsuarioJuegoMesa,
    usuarioJuegoMesa => usuarioJuegoMesa.idUsuario2
  )
  public usuarioJuegoMesas: UsuarioJuegoMesa[];

  public constructor(init?: Partial<Usuario>) {
    Object.assign(this, init);
  }
}
