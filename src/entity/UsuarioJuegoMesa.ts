import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn
} from "typeorm";
import { Usuario } from "./Usuario";
import { JuegoMesa } from "./JuegoMesa";

@Index("fk_usuario_juego_mesa_usuario_1", ["idUsuario"], {})
@Index("usuario_juego_mesa_FK", ["idJuegoMesa"], {})
@Entity("usuario_juego_mesa", { schema: "gobeeyond_develop" })
export class UsuarioJuegoMesa {
  @PrimaryGeneratedColumn({ type: "int", name: "id_usuario_juego_mesa" })
  public idUsuarioJuegoMesa?: number;

  @Column("float", { name: "Apuesta_por_cliente", precision: 8, scale: 2 })
  public apuestaPorCliente?: number;

  @Column("int", { name: "id_usuario" })
  public idUsuario?: number;

  @Column("int", { name: "id_juego_mesa" })
  public idJuegoMesa?: number;

  @ManyToOne(
    () => Usuario,
    usuario => usuario.usuarioJuegoMesas,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "id_usuario", referencedColumnName: "idUsuario" }])
  public idUsuario2?: Usuario;

  @ManyToOne(
    () => JuegoMesa,
    juegoMesa => juegoMesa.usuarioJuegoMesas,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "id_juego_mesa", referencedColumnName: "idJuegoMesa" }])
  public idJuegoMesa2?: JuegoMesa;

  public constructor(init?: Partial<UsuarioJuegoMesa>) {
    Object.assign(this, init);
  }
}
