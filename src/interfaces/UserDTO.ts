export interface UserDTO {
    nombre: string;
    paterno: string;
    materno?: string;
    alias: string | null;
    nacimiento: string | null;
    telefonico: string;
    email: string;
    emailAlternativo: string | null;
    imageUri: string | null;
    pais:string | null;
    ciudad: string | null;
    ocupacion: string | null;
    experiencia: string | null;
    juegosPartidosPendientes: number;
    juegosPersonalizadosPendientes: number;

}
