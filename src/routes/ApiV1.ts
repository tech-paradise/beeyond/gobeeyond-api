import { Request, Response, Router } from 'express';
import * as RoutesNames from './RoutesNames';
import { UsersRoutes } from './UserRoutes';
import { CardRoutes } from './CardRoutes';
import { OpenpayRoutes } from './OpenpayRoutes';
import { PreferencesRoutes } from './PreferenceRoutes';
import { TermsConditionsRoutes } from './TermsCondtionsRoutes';
import { ProductRoutes } from './ProductRoutes';
import {LoginRoutes} from "./LoginRoutes";
import { TicketRoutes } from "./TicketRoutes";
export class ApiV1 {
    private readonly ROUTER= Router();
    private readonly USER_ROUTE = new UsersRoutes();
    private readonly CARD_ROUTE = new CardRoutes();
    private readonly OPENPAY_ROUTE = new OpenpayRoutes();
    private readonly PREFERENCES_ROUTE = new PreferencesRoutes();
	private readonly TERMCONDITIONS_ROUTE = new TermsConditionsRoutes();
	private readonly PRODUCTS_ROUTE = new ProductRoutes();
	private readonly LOGIN_ROUTE = new LoginRoutes();
	private readonly TICKETS_ROUTE = new TicketRoutes();

    constructor() {
        this.init();
    }

    private init(): void {
        this.ROUTER.get(RoutesNames.ROOT,
            (req: Request, res: Response) =>
                res.json({
                    message: 'Api works',
                })
        );

        this.ROUTER.use(
            RoutesNames.USERS,
            this.LOGIN_ROUTE.getRoutes()
        );

        this.ROUTER.use(
            RoutesNames.USERS,
            this.USER_ROUTE.getRoutes()
        );

        this.ROUTER.use(
            RoutesNames.CARDS,
            this.CARD_ROUTE.getRoutes()
        );

        this.ROUTER.use(
            RoutesNames.OPENPAY,
            this.OPENPAY_ROUTE.getRoutes()
        );

        this.ROUTER.use(
            RoutesNames.PREFERENCES,
            this.PREFERENCES_ROUTE.getRoutes()
        );

        this.ROUTER.use(
            RoutesNames.TERMS,
            this.TERMCONDITIONS_ROUTE.getRoutes()
		);
		
		this.ROUTER.use(
            RoutesNames.PRODUCTS,
            this.PRODUCTS_ROUTE.getRoutes()
		);
		
		this.ROUTER.use(
            RoutesNames.TICKETS,
            this.TICKETS_ROUTE.getRoutes()
        );
    }

    getRouter(): Router {
        return this.ROUTER;
    }
}
