import { Request, Response, Router } from 'express';
import { param, body, ValidationChain } from 'express-validator';
import * as RoutesNames from './RoutesNames';
import { validateParams } from '../utils/ValidateUtils';
import CardController from '../controllers/CardController';
import { validateToken } from '../utils/AuthenticationUtils';

enum TypeValidation {
    REGISTER,
    UPDATE,
    LIST,
    DELETE,
}

export class CardRoutes {
    private readonly ROUTER: Router;
    private readonly CONTROLLER: CardController;
    private readonly ROUTE_NAME: string;

    constructor() {
        this.ROUTER = Router();
        this.CONTROLLER = new CardController();
        this.ROUTE_NAME = RoutesNames.API_V1 + RoutesNames.CARDS;
        this.init();
    }

    private init(): void {
        this.ROUTER.post(
            RoutesNames.ROOT,
            validateToken,
            this.getValidation(TypeValidation.REGISTER),
            validateParams(this.ROUTE_NAME + RoutesNames.ROOT),
            (req: Request, res: Response) => this.CONTROLLER.register(req, res)
        );

        this.ROUTER.put(
            RoutesNames.ROOT + RoutesNames.PARAMS.ID,
            validateToken,
            this.getValidation(TypeValidation.UPDATE),
            validateParams(this.ROUTE_NAME + RoutesNames.UPDATE),
            (req: Request, res: Response) =>
                this.CONTROLLER.updateCard(req, res)
        );

        this.ROUTER.get(
            RoutesNames.ROOT,
            validateToken,
            this.getValidation(TypeValidation.LIST),
            validateParams(this.ROUTE_NAME + RoutesNames.PARAMS.ID),
            (req: Request, res: Response) =>
                this.CONTROLLER.getCardsOfUser(req, res)
        );

        this.ROUTER.delete(
            RoutesNames.PARAMS.ID,
            validateToken,
            this.getValidation(TypeValidation.DELETE),
            validateParams(this.ROUTE_NAME),
            (req: Request, res: Response) =>
                this.CONTROLLER.deleteCard(req, res)
        );
    }

    private getValidation(method: TypeValidation): ValidationChain[] {
        switch (method) {
            case TypeValidation.REGISTER:
                return [
                    body('token', ' Formato incorrecto').isString(),
                    body('idDeviceSession', ' Formato incorrecto')
                        .trim()
                        .isString()
                        .isAlphanumeric(),
                ];

            case TypeValidation.UPDATE:
                return [
                    param('id', 'Formato incorrecto')
                        .trim()
                        .isNumeric(),
                    body('token')
                        .trim()
                        .isString(),
                ];

            case TypeValidation.LIST:
                return [];

            case TypeValidation.DELETE:
                return [
                    param('id', 'Formato incorrecto')
                        .trim()
                        .isNumeric(),
                ];
            default:
                return [];
        }
    }

    getRoutes(): Router {
        return this.ROUTER;
    }
}
