import { Request, Response, Router } from 'express';
import { body, ValidationChain } from 'express-validator';
import { LOGIN, API_V1 } from './RoutesNames';
import { validateParams } from "../utils/ValidateUtils";
import { LoginController } from "../controllers/LoginController";

enum TypeValidation {
    LOGIN,
}

export class LoginRoutes {
    private readonly ROUTER: Router;
    private readonly ROUTE_NAME: string;
    private readonly LOGIN_CONTROLLER: LoginController;

    constructor() {
        this.ROUTER = Router();
        this.ROUTE_NAME = API_V1 + LOGIN;
        this.LOGIN_CONTROLLER = new LoginController();
        this.init();
    }

    private init(): void {
        this.ROUTER.post(LOGIN,
            this.getValidation(TypeValidation.LOGIN),
            validateParams(this.ROUTE_NAME + LOGIN),
            (req: Request, res: Response) =>
            this.LOGIN_CONTROLLER.getLogin( req, res )
        );

    }

    private getValidation(method: TypeValidation): ValidationChain[] {
        switch (method) {
            case TypeValidation.LOGIN:
                return [
                    body("email", "Formato incorrecto")
                        .trim().isEmail(),
                    body("password", "Formato incorrecto").isString().trim()

                ];
            default:
                return [];
        }
    }

    getRoutes(): Router {
        return this.ROUTER;
    }
}
