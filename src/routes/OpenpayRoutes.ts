import { Request, Response, Router } from 'express';
import { body, ValidationChain } from 'express-validator';
import * as RoutesNames from './RoutesNames';
import { validateParams } from '../utils/ValidateUtils';
import { OpenpayController } from '../controllers/OpenpayController';

enum TypeValidation {
    PAY,
}

export class OpenpayRoutes {
    private readonly ROUTER: Router;
    private readonly CONTROLLER: OpenpayController;
    private readonly ROUTE_NAME: string;

    constructor() {
        this.ROUTER = Router();
        this.CONTROLLER = new OpenpayController();
        this.ROUTE_NAME = RoutesNames.API_V1 + RoutesNames.OPENPAY;
        this.init();
    }

    private init(): void {
        this.ROUTER.get(RoutesNames.ROOT, (req: Request, res: Response) =>
            res.json({
                message: 'Client Api works!!!',
            })
        );
    }

    private getValidation(method: TypeValidation): ValidationChain[] {
        switch (method) {
            case TypeValidation.PAY:
                return [];
            default:
                return [];
        }
    }

    getRoutes(): Router {
        return this.ROUTER;
    }
}
