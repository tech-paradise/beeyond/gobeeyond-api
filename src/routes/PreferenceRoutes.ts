import { Request, Response, Router } from 'express';
import { param, body, ValidationChain } from 'express-validator';
import * as RoutesNames from './RoutesNames';
import { validateParams } from '../utils/ValidateUtils';
import { PreferenceController } from '../controllers/PreferenceController';

enum TypeValidation {
    REGISTER,
    UPDATE,
    DELETE,
    LIST,
}

export class PreferencesRoutes {
    private readonly ROUTER: Router;
    private readonly CONTROLLER: PreferenceController;
    private readonly ROUTE_NAME: string;

    constructor() {
        this.ROUTER = Router();
        this.CONTROLLER = new PreferenceController();
        this.ROUTE_NAME = RoutesNames.API_V1 + RoutesNames.PREFERENCES;
        this.init();
    }

    private init(): void {
		this.ROUTER.get(RoutesNames.ROOT,
            (req: Request, res: Response) =>  res.json({message: 'root!'})
		);

        this.ROUTER.post(
            RoutesNames.ROOT,
            this.getValidation(TypeValidation.REGISTER),
            validateParams(this.ROUTE_NAME + RoutesNames.ROOT),
            (req: Request, res: Response) => this.CONTROLLER.register(req, res)
		);
		
		this.ROUTER.get(
            RoutesNames.PARAMS.ID,
            this.getValidation(TypeValidation.LIST),
            validateParams(this.ROUTE_NAME + RoutesNames.ROOT),
            (req: Request, res: Response) => this.CONTROLLER.getList(req, res)
        );

        this.ROUTER.put(
            RoutesNames.ROOT,
        	this.getValidation(TypeValidation.UPDATE),
            validateParams(this.ROUTE_NAME + RoutesNames.UPDATE),
        	(req: Request, res: Response) => this.CONTROLLER.update(req, res)
        );

        this.ROUTER.delete(
            RoutesNames.PARAMS.ID,
        	this.getValidation(TypeValidation.DELETE),
            validateParams(this.ROUTE_NAME + RoutesNames.ROOT),
        	(req: Request, res: Response) => this.CONTROLLER.delete(req, res)
        );
    }

    private getValidation(method: TypeValidation): ValidationChain[] {
        switch (method) {
            case TypeValidation.REGISTER:
                return [
                    body('deporte', ' Formato incorrecto')
                        .trim()
						.isString()
                        .isLength({ min: 2, max: 50 }),
                    body('deportista', ' Formato incorrecto')
                        .trim()
						.isString()
                        .isLength({ min: 2, max: 150 }),
                ];
            case TypeValidation.UPDATE:
                return [
                    body('idPreferencia', 'Formato incorrecto').isNumeric(),
                    body('deporte', ' Formato incorrecto')
                        .trim()
                        .isString()
                        .isLength({ min: 2, max: 50 }),
                    body('deportista', ' Formato incorrecto')
                        .trim()
                        .isString()
                        .isLength({ min: 2, max: 150 })
                ];

            case TypeValidation.DELETE:
                return [
                    param('id', 'Formato incorrector').isNumeric(),
                ];

			//TODO: delete this validation after implemeting session
            case TypeValidation.LIST:
				return [
                    param('id', 'Formato incorrecto').isNumeric(),
                ];
            default:
                return [];
        }
    }

    getRoutes(): Router {
        return this.ROUTER;
    }
}
