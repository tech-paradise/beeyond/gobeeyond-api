import { Request, Response, Router } from 'express';
import { body, ValidationChain, param } from 'express-validator';
import * as RoutesNames from './RoutesNames';
import { validateParams } from '../utils/ValidateUtils';
import { ProductController } from "../controllers/ProductController";

enum TypeValidation {
    GET,
}

export class ProductRoutes {
    private readonly ROUTER: Router;
	private readonly ROUTE_NAME: string;
	private readonly CONTROLLER: ProductController

    constructor() {
        this.ROUTER = Router();
		this.ROUTE_NAME = RoutesNames.API_V1 + RoutesNames.PRODUCTS;
		this.CONTROLLER = new ProductController();
		this.init();
    }

    private init(): void {
		this.ROUTER.get(RoutesNames.ROOT, 
			(req: Request, res: Response) => this.CONTROLLER.getList(req, res)
		);
		
		this.ROUTER.get(RoutesNames.PARAMS.ID,
            this.getValidation(TypeValidation.GET),
			validateParams(this.ROUTE_NAME + RoutesNames.ROOT),
			(req: Request, res: Response) => this.CONTROLLER.getOne(req, res)
        );
    }

    private getValidation(method: TypeValidation): ValidationChain[] {
        switch (method) {
            case TypeValidation.GET:
                return [param('id', 'Formato Incorrecto').isNumeric()];
            default:
                return [];
        }
    }

    getRoutes(): Router {
        return this.ROUTER;
    }
}
