export const ROOT = '/';
export const API_V1 = '/api/v1';

// Names for modules
export const USERS = '/usuario';
export const USER_INFO = '/info';
export const USER_ACCOUNT = '/account';
export const LOGIN = '/session';
export const CARDS = '/tarjetas';
export const WALLET = '/carteras';
export const TRANSACTION = '/transacciones';

export const YIELD = '/rendimientos';
export const PROVIDER = '/proveedores';
export const PRODUCTS = '/productos';
export const TERMS = '/terminos-y-condiciones';
export const PROVIDERPAYMENT = '/pago-proveedores';

export const TICKETS = '/tickets';
export const CLAIMENTPLACE = '/lugar-de-reclamacion';
export const PRODUCTSCONTROL = '/control-productos';
export const BRANCHS = '/sucursales';
export const GAME1VS1 = '/juegounouno';

export const GAME1VS1CUSTOM = '/juego-uno-a-uno-pers';
export const QUINIELA = '/quiniela';
export const QUINIELAUSER = '/usuario-quiniela';

export const EXCEL = '/excel';
export const OPENPAY = '/openpay';
export const PENDINGGAMES = '/juegos-pendientes';
export const FRIENDS = '/amigos';
export const PREFERENCES = '/preferencias';

// General actions
export const REGISTRO = '/register';
export const SEND_PAYMENT = '/enviar-pago';
export const UPDATE = '/update';
export const UPDATE_COMPLEMENT = '/update-complement';
export const UPDATE_EXP = '/update-exp';
export const BLOCK = '/block';
export const ID = '/id';
export const AMMOUNTS = '/ammounts';
export const PIN = '/pin';
export const USER_DATA = '/user-data';
export const ALIAS_EXIST = '/alias-exist';
export const VALIDATE = '/validate';
export const LIST = '/list';
export const DELETE = '/delete';

export const PARAMS = {
    ID: '/:id',
};
