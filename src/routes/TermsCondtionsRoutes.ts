import { Request, Response, Router } from 'express';
import { body, ValidationChain, param } from 'express-validator';
import * as RoutesNames from './RoutesNames';
import { validateParams } from '../utils/ValidateUtils';
import { TermsConditionsController } from '../controllers/TermsConditionsController';

enum TypeValidation {
    REGISTER,
    GET,
    DELETE,
}

export class TermsConditionsRoutes {
    private readonly ROUTER: Router;
    private readonly ROUTE_NAME: string;
    private readonly CONTROLLER: TermsConditionsController;

    constructor() {
        this.ROUTER = Router();
        this.ROUTE_NAME = RoutesNames.API_V1 + RoutesNames.TERMS;
        this.CONTROLLER = new TermsConditionsController();
        this.init();
    }

    private init(): void {
        this.ROUTER.get(RoutesNames.ROOT, (req: Request, res: Response) =>
            res.json({
                message: 'Client Api works!!!',
            })
        );

        this.ROUTER.post(
            RoutesNames.ROOT,
            this.getValidation(TypeValidation.REGISTER),
            validateParams(this.ROUTE_NAME + RoutesNames.ROOT),
            (req: Request, res: Response) => this.CONTROLLER.register(req, res)
        );

        this.ROUTER.get(
            RoutesNames.PARAMS.ID,
            this.getValidation(TypeValidation.GET),
            validateParams(this.ROUTE_NAME + RoutesNames.ROOT),
            (req: Request, res: Response) => this.CONTROLLER.get(req, res)
        );

        this.ROUTER.delete(
            RoutesNames.PARAMS.ID,
            this.getValidation(TypeValidation.DELETE),
            validateParams(this.ROUTE_NAME + RoutesNames.ROOT),
            (req: Request, res: Response) => this.CONTROLLER.delete(req, res)
        );
    }

    private getValidation(method: TypeValidation): ValidationChain[] {
        switch (method) {
            case TypeValidation.REGISTER:
                return [
                    body('uri', 'Formato Incorrecto')
                        .trim()
                        .isString()
                        .isURL(),
                    body('acepta', 'Formato incorrecto').trim().isNumeric(),
                    body('fecha', 'Formato Incorrecto')
                        .trim()
                        .toDate(),
                    body('userId', 'Formato Incorrecto')
                        .trim()
                        .isNumeric(),
                ];
            case TypeValidation.GET:
                return [param('id', 'Formato incorrecto').isNumeric()];
            case TypeValidation.DELETE:
                return [param('id', 'Formato incorrecto').isNumeric()];
            default:
                return [];
        }
    }

    getRoutes(): Router {
        return this.ROUTER;
    }
}
