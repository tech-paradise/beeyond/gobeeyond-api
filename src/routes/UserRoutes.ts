import { Request, Response, Router } from 'express';
import {UserController} from '../controllers/UserController';
import {body, ValidationChain} from 'express-validator';
import PasswordValidator from 'password-validator';
import {API_V1, USERS, ROOT, PARAMS, USER_INFO, USER_ACCOUNT} from './RoutesNames';
import {validateParams} from "../utils/ValidateUtils";
import {validateToken} from "../utils/AuthenticationUtils";

enum TypeValidation {
  REGISTER,
  UPDATE,
  UPDATE_INFO,
  UPDATE_ACCOUNT
}

export class UsersRoutes {
    private readonly ROUTER: Router;
    private readonly USER_CONTROLLER: UserController;
    private readonly SCHEMA: PasswordValidator;
    private readonly ROUTE_NAME: string;

    constructor() {
        this.ROUTER = Router();
        this.USER_CONTROLLER = new UserController();
        this.ROUTE_NAME = API_V1 + USERS;
        this.SCHEMA = new PasswordValidator()
            .is()
            .min(4)
            .is()
            .max(40)
            .has()
            .digits()
            .has()
            .letters();
        this.init();
    }

    private init(): void {

        this.ROUTER.get(ROOT,
            validateToken,
            (req: Request, res: Response) => this.USER_CONTROLLER.getUserId(req, res)
        );

        this.ROUTER.post(
            ROOT,
            this.getValidation(TypeValidation.REGISTER),
            validateParams(this.ROUTE_NAME + ROOT),
            (req: Request, res: Response) => this.USER_CONTROLLER.register(req, res)
        );

        this.ROUTER.put(ROOT,
            validateToken,
            this.getValidation(TypeValidation.UPDATE),
            validateParams(this.ROUTE_NAME+ROOT),
            (req:Request, res: Response) => this.USER_CONTROLLER.updateDataApp(req, res)
        );

        this.ROUTER.put(USER_INFO,
            validateToken,
            this.getValidation(TypeValidation.UPDATE_INFO),
            validateParams(this.ROUTE_NAME+PARAMS.ID + USER_INFO),
            (req:Request, res: Response) => this.USER_CONTROLLER.updateInfo(req, res)
        );

      this.ROUTER.put(USER_ACCOUNT,
          validateToken,
          this.getValidation(TypeValidation.UPDATE_ACCOUNT),
          validateParams(this.ROUTE_NAME+PARAMS.ID + USER_ACCOUNT),
          (req:Request, res: Response) => this.USER_CONTROLLER.updateAccount(req, res)
      );
    }

    private getValidation(method: TypeValidation): ValidationChain[] {
        const infoUser:ValidationChain[] =[
            body('nombreCliente', ' Formato incorrecto')
                .trim()
                .isString().bail()
                .isLength({ min: 2, max: 50 }).bail(),
            body('primerApel', ' Formato incorrecto')
              .trim()
              .isString().bail()
              .isLength({ min: 2, max: 50 }).bail(),
            body('segundoApel', ' Formato incorrecto')
                .trim()
                .optional()
                .isString().bail()
                .isLength({ min: 2, max: 50 }).bail(),
            body('fechaNac', ' Formato incorrecto')
                .trim()
                .isISO8601().bail()
                .isLength({ min: 3, max: 15 }),
            body('telCel', ' Campo obligatorio')
                .trim()
                .isMobilePhone('any'),
            body('pais', 'Formato incorrecto')
                .optional().trim().notEmpty(),
            body('ciudad', 'Formato incorrecto')
                .optional().trim().notEmpty(),
            body('ocupacion', 'Formato incorrecto')
                .optional().trim().notEmpty().bail()
        ];

        const accountUser: ValidationChain[] = [
            body('alias', ' Formato incorrecto')
                .trim()
                .isString()
                .isLength({ min: 3, max: 15 })
                .isAlphanumeric(),
            body('imageUri', 'Formato incorrecto')
                .optional().trim().isDataURI(),
            body('correoAlternativo', 'Formato incorrecto')
                .optional().trim().isEmail()
        ];

        const registerInfo = [
            body('correoElectronico', ' Formato incorrecto')
            .trim()
            .isEmail(),
            body('pin', ' Formato incorrecto')
                .trim()
                .isNumeric().bail()
                .isLength({ min: 4, max: 6 }),
            body('password', ' Formato incorrecto')
                .trim()
                .custom((password: string): boolean => {
                        if (!this.SCHEMA.validate(password)) {
                            throw new Error(
                                'password debe tener entre 8 y 16 caracteres, y algún numero'
                            );
                        }
                        return true;
                    }
                )
        ];

        const modifyPassword: ValidationChain[] = [
            body('oldPassword', ' Formato incorrecto')
                .optional()
                .trim().notEmpty()
        ];


        switch (method) {
            case TypeValidation.REGISTER:
                return infoUser.concat(
                    accountUser,
                    registerInfo
                );
            case TypeValidation.UPDATE:
                return [
                    body('experiencia', 'Valor Invalido').optional().trim().isNumeric(),
                    body('bloqueado','Valor invalido').optional().trim().isBoolean().toBoolean()
                ];
            case TypeValidation.UPDATE_INFO:
                return infoUser.map(v=>v.optional());
            case TypeValidation.UPDATE_ACCOUNT:
                const registerInfoOptional= registerInfo.map(v=>v.optional());
                return accountUser.concat(
                    registerInfoOptional,
                    modifyPassword
                );
          default:
                return [];
        }
    }

    getRoutes(): Router {
        return this.ROUTER;
    }
}
