import { createConnection, getRepository, In } from 'typeorm';
import { config } from 'dotenv';
import axios from 'axios';

import { LigaFootball } from '../entity/LigaFootball';
import { EquipoFootbal } from '../entity/EquipoFootbal';
import { PartidoFootball } from '../entity/PartidoFootball';

if (process.env.NODE_ENV !== 'production') {
    config();
}

const FootballApi = axios.create({
    baseURL: 'https://api-football-v1.p.rapidapi.com/v2',
    headers: {
        'X-RapidAPI-Key': process.env.API_FOOTBALL_KEY,
        'x-rapidapi-host': 'api-football-v1.p.rapidapi.com',
    },
});

async function getActiveLeagues(): Promise<Array<Object>> {
    const res = await FootballApi.get('/leagues/country/mx');
    if (res.status !== 200) {
        throw 'Error al conseguir ligas';
    }

    const listOfLeagues: Array<Object> = res.data.api.leagues;

    return listOfLeagues.filter((liga: any) => liga['is_current'] == 1);
}

async function inserLeaguesInDb(newLeagues: Array<any>) {
    const leaguesRepository = getRepository(LigaFootball);
    const alreadySavedLeagues = await leaguesRepository.find();

    for (const newLeague of newLeagues) {
        if (
            !alreadySavedLeagues.find(
                savedLeague => savedLeague.idApi == newLeague['league_id']
            )
        ) {
            let newLeagueToSave = new LigaFootball();
            newLeagueToSave.idApi = newLeague['league_id'];
            newLeagueToSave.nombre = newLeague['name'];
            newLeagueToSave.pais = newLeague['country'];

            newLeagueToSave.temporada = newLeague['season'];
            newLeagueToSave.fechaInicio = newLeague['season_start'];
            newLeagueToSave.fechaFinal = newLeague['season_end'];
            newLeagueToSave.logo = newLeague['logo'] || '';

            newLeagueToSave.flag = newLeague['flag'];
            newLeagueToSave.activo = newLeague['is_current'];
            newLeagueToSave.tipo = newLeague['type'];

            await leaguesRepository.save(newLeagueToSave);
            console.log('++ Inserting league');
        }
    }
}

async function getFixtures(): Promise<Array<Object>> {
    const leaguesRepository = getRepository(LigaFootball);
    const savedLeagues = await leaguesRepository.find({ activo: 1 });

    let listOfFixtures: Array<Object> = [];

    console.log('Getting fixtures of: ', savedLeagues.length, ' leagues');
    for (const savedLeague of savedLeagues) {
        let res = await FootballApi.get(
            '/fixtures/league/' + savedLeague.idApi
        );
        if (res.status !== 200) {
            throw 'Error al conseguir ligas';
        }

        console.log(
            'Adding: ',
            res.data.api.fixtures.length,
            'partidos encontrados'
        );
        listOfFixtures = listOfFixtures.concat(res.data.api.fixtures);
    }

    return listOfFixtures;
}

async function insertFixturesAndTeams(newFixtures: Array<any>) {
    const leaguesRepository = getRepository(LigaFootball);
    const teamsRepository = getRepository(EquipoFootbal);
    const fixturesRepository = getRepository(PartidoFootball);

    const activeLeagues = await leaguesRepository.find({ activo: 1 });
    const activeLeaguesIds = activeLeagues.map(
        (liga: LigaFootball) => liga.idLiga
    );

    let activeMatches = await fixturesRepository.find({where:{id_liga: In(activeLeaguesIds)}});

    console.log('partidas previas', activeMatches.length);
    let savedTeams = await teamsRepository.find();
	let counter = 0

    for (const newFixture of newFixtures) {
        if (
            !activeMatches.find(
                activeMatches => activeMatches.idApi == newFixture['fixture_id']
            )
        ) {
            let homeTeam = newFixture['homeTeam'];
            let savedHomeTeam = savedTeams.find(
                sT => sT.idApi == homeTeam['team_id']
            );
            if (!savedHomeTeam) {
                savedHomeTeam = new EquipoFootbal({
                    idApi: homeTeam['team_id'],
                    nombre: homeTeam['team_name'],
                    logo: homeTeam['logo'],
                });

                await teamsRepository.save(savedHomeTeam);
                savedTeams.push(savedHomeTeam);
            }

            let awayTeam = newFixture['awayTeam'];
            let savedAwayTeam = savedTeams.find(
                sT => sT.idApi == awayTeam['team_id']
            );
            if (!savedAwayTeam) {
                savedAwayTeam = new EquipoFootbal({
                    idApi: awayTeam['team_id'],
                    nombre: awayTeam['team_name'],
                    logo: awayTeam['logo'],
                });

                await teamsRepository.save(savedAwayTeam);
                savedTeams.push(savedAwayTeam);
            }

            let golesLocal = newFixture['goalsHomeTeam'] || 0;
            let golesRemoto = newFixture['goalsAwayTeam'] || 0;
            let ganador =
                golesLocal > golesRemoto
                    ? 'LOCAL'
                    : golesLocal < golesRemoto
                    ? 'VISITANTE'
                    : '';
            let resultado =
                golesLocal == golesRemoto && newFixture['elapsed'] > 0
                    ? 'EMPATE'
                    : ganador;

            let newFixtureToSave = new PartidoFootball({
                idApi: newFixture['fixture_id'],
                idLigaFootball: activeLeagues.find(
                    liga => liga.idApi == newFixture['league_id']
                )?.idLiga,
                status: newFixture['status'] || '',
                ronda: newFixture['round'] || '',
                idEquipoLocal: savedHomeTeam.idEquipo,
                idEquipoRemoto: savedAwayTeam.idEquipo,
                golesEquipoLocal: golesLocal,
                golesEquipoRemoto: golesRemoto,
                equipoGanador: ganador,
                estadio: newFixture['venue'] || '',
                tiempoTranscurrido: newFixture['elapsed'],
                resultado: resultado,
            });

			await fixturesRepository.save(newFixtureToSave);
			counter++;
        }
    }

    console.log('terminado con: ', counter,' inserciones');
}

(async function() {
    console.log('Getting Conection');
    await createConnection();

    console.log('Getting leagues');
    const leagues = await getActiveLeagues();
    console.log('Inserting leagues');
    await inserLeaguesInDb(leagues);

    const fixtures = await getFixtures();
    console.log('Inserting fixtures: ', fixtures.length);
    await insertFixturesAndTeams(fixtures);
})();
