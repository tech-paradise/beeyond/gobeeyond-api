import express from "express";
import {ApiV1} from "../routes/ApiV1";
import {join} from "path";
import {API_V1} from "../routes/RoutesNames"
import HttpError from "http-errors";
import morgan from "morgan";
import {getLogger, Logger} from "log4js";

export class Server {
    private readonly APP: express.Application;
    private readonly ROUTER: express.Router;
    private readonly LOG: Logger;

    static getServer(): Server {
        return new Server();
    }

    constructor() {
        this.APP = express();
        this.ROUTER = new ApiV1().getRouter();
        this.LOG = getLogger("Server");
        this.init();
    }

    private init(): void {

        // middlewares for req
        this.APP.use(express.json());
        this.APP.use(express.urlencoded({ extended: false }));

        // static directory
        this.APP.use(express.static(join(process.cwd(), "public")));

        // index routes
        this.APP.use(API_V1, this.ROUTER);

        // Logguer
        this.APP.use(morgan(':date[web] :remote-addr :method :url :status'));

        // Last middleware, in case not match whit a route, throws a http-error
        this.APP.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
            next(HttpError(404));
        });

        // Error handler, redirect a page with the error
        this.APP.use((err: HttpError.HttpError, req: express.Request, res: express.Response, next: express.NextFunction) => {
           
            // render the error page
            res.status(err.status || 500);
            res.json({error: err, message : err.message});
        });
    }

    getApp(): express.Application {
        return this.APP;
    }

    async start(port:string | undefined ,callback?: (...args:any[])=>void) {
        try {
            this.APP.listen(port,callback);
        }catch (e) {
            this.LOG.error(e);
        }

    }
}
