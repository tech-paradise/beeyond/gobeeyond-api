import {getLogger,Logger} from 'log4js';
import { Login } from '../entity/Login';
import {
    getManager,
    getRepository,
    EntityRepository,
    Repository,
} from 'typeorm';

@EntityRepository(Login)
export class LoginService extends Repository<Login>{
    private readonly LOG: Logger= getLogger('UserService');

    async findLoginByCorreo(email: string): Promise<Login | undefined> {
        const loginRepository: Repository<Login> = getRepository(Login);

        return loginRepository.findOne({
            where:{ correoElectronico: email },
        });
    }


}
