import { getLogger, Logger } from 'log4js';
import { Usuario } from '../entity/Usuario';
import { Login } from '../entity/Login';
import {
    getManager,
    getRepository,
    EntityRepository,
    Repository,
} from 'typeorm';


@EntityRepository(Usuario)
export class UserService extends Repository<Usuario> {
    private readonly LOG: Logger = getLogger('UserService');

    async findUserWithAlias(
        alias: string
    ): Promise<Usuario | undefined> {
        const userRepository: Repository<Usuario> = getRepository(Usuario);

        return userRepository.findOne({
            where: [{ alias }],
        });
    }

    /**
     * saveNewUser
     */
    async saveNewUser(
        userData: Usuario,
        loginData: Login
    ): Promise<Usuario | undefined> {
        this.LOG.debug('usuario', userData);
        this.LOG.debug('login', loginData);
        return getManager().transaction(async manager => {
            const login = await manager.save(loginData);
            userData.idLogin = login.idLogin;
            const user = await manager.save(userData);
            this.LOG.debug('Trans');
            return user;
        });
    }
}
