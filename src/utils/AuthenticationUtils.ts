import {NextFunction, Request, Response} from "express";
import {verify} from "jsonwebtoken";
import {TokenInfo} from "../interfaces/TokenInfo";
import {getRepository} from "typeorm";
import {Usuario} from "../entity/Usuario";

export const validateToken =
    async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        const token = req.get("Authorization") || '';
        const seed = process.env.TOKEN_SEDD || '';
        try {
            const decode = await verify(token, seed) as TokenInfo;
            const user = await getRepository(Usuario).findOne({
                where: {
                    idUsuario: decode.user,
                    idLogin: decode.id
                },
                relations:['idLogin2']
            });

            if (!user){
                res.status(401).json({
                    message: "Autorizacion invalida, inicia sesión de nuevo"
                });
            }

            req.body.userSession = user;
            next();

        } catch (e) {
            res.status(401).json({
                message: "Autorizacion invalida, inicia sesión de nuevo"
            });
        }
    };
