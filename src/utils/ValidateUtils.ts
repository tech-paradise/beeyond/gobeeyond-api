import {Response, Request, NextFunction} from "express";
import {validationResult} from "express-validator";
import {Logger, getLogger} from "log4js";

export const validateParams = (routeName: string):
    (req: Request, res: Response, next: NextFunction)=> Promise<Response | void> =>
    async(req, res, next) => {

        const result = await validationResult (req);
        const log: Logger = getLogger();
        log.info(`Validando parámetros en ${req.method} ${routeName}:`);

        if (!result.isEmpty()) {

            const error = result.array().map((i) => `error en ${i.param} ${i.msg}`).join(", ");
            log.error(error);

            return res.status(400).json({message:error});

        } else {
            return next();
        }
    };
