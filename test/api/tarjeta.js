let chai = require('chai');
let chaiHttp = require('chai-http');
const expect = require('chai').expect;
require('dotenv').config()

chai.use(chaiHttp);
const url = 'http://localhost:3000/api/v1/tarjeta';

let tarjetaId = null;
let open_token = null;
let close_token = null;
let userId = 64
let idCustomer = 'akurt5z6pcz5lwdrd8f8'
let idDeviceSession = 'kR1MiQhz2otdIuUlQkbEyitIqVMiI16f'

describe('Encripting card: ', () => {
    it('should encript a card', async () => {
        let res = await chai.request('https://sandbox-api.openpay.mx')
            .post('/v1/' + process.env.OPENPAY_ID + '/tokens')
            .auth(process.env.OPENPAY_PUBLIC, '', { type: 'basic' })
            .send({
                "card_number": "4111111111111111",
                "holder_name": "Juan Perez Ramirez",
                "expiration_year": "20",
                "expiration_month": "12",
                "cvv2": "110",
                "address": {
                    "city": "Querétaro",
                    "line3": "Queretaro",
                    "postal_code": "76900",
                    "line1": "Av 5 de Febrero",
                    "line2": "Roble 207",
                    "state": "Queretaro",
                    "country_code": "MX"
                }

            })

        expect(res).to.have.status(201);
        open_token = res.body.id
        console.log('OpnToken: ', open_token)
    });
});

describe('Registering card: ', () => {
    it('should insert a card', async () => {
        let data = {
            token: open_token,
            idCliente: userId,
            idCustomer: idCustomer,
            idDeviceSession: idDeviceSession,
        }

        console.log('sending', data)
        let res = await chai.request(url)
            .post('/register')
            .send(data)
        expect(res).to.have.status(200);
        close_token = res.body.id
        console.log('Close_Token: ', open_token)
    });
});

describe('List of cards ', () => {
    it('should insert a card', async () => {
        let res = await chai.request(url)
            .get('/list' + '/' + idCustomer)

        expect(res).to.have.status(200);
        console.log('List of: ' + res.body.length + ' cards')
    });
});

describe('Deleting card: ', () => {
    it('should delete a card using the token', async () => {
        chai.assert(close_token != null, 'token is null')
        console.log('Deleting token', close_token)
        let res = await chai.request(url)
            .delete('')
            .send({
                token: close_token,
                idCustomer: idCustomer
            })

        expect(res).to.have.status(200);
    });
});
