import { Server } from '../../src/server/Server';
import { Application } from 'express';
import { SERVER_INIT } from '../../src/app';
import request from 'supertest';
import axios from 'axios';

require('dotenv').config();
let server: Server;
let app: Application;

beforeAll(async () => {
    server = await SERVER_INIT();
    app = server.getApp();
});

describe('Check Terms and conditions controller: ', () => {
    const url = '/api/v1/terminos-y-condiciones/';
    let user_token: string = '';
	let userId = 64;
	let recordId = 0;

	it('must get a root of terms and conditions', async (done) => {
        let res = await request(app)
            .get(url)
        expect(res.status).toBe(200);
       done()
	});

	it('Must insert a terms & conditions record', async (done) => {
        let res = await request(app)
            .post(url)
			.send({
				uri : 'http://google.com',
				acepta: 1,
				fecha: '2020-02-01',
				userId: 64
			});

		expect(res.status).toBe(200);
		expect(res.body.id).not.toBeNull();
		recordId =  res.body.id
		done()
	});
	
	it('Must get a terms & conditions record', async (done) => {
        let res = await request(app)
			.get(url+recordId);
		console.log(url+recordId)

		expect(res.status).toBe(200);
		done()
    });


	
	it('Must delete an accepted terms & conditions record', async (done) => {
        let res = await request(app)
            .delete(url+recordId)
            .send();
		expect(res.status).toBe(200);
		done()
	});

});

// afterAll(async () => {
// 	// console.log(app)
// 	app = null;
// });
