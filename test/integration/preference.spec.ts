import { Server } from '../../src/server/Server';
import { Application } from 'express';
import { SERVER_INIT } from '../../src/app';
import request from 'supertest';
import axios from 'axios';

// require('dotenv').config();
let server: Server;
let app: Application;

beforeAll(async () => {
    server = await SERVER_INIT();
    app = server.getApp();
});

describe('CRUD of preferences: ', () => {
    const url = '/api/v1/preferencias/';
    let user_token: string = '';
    let userId = 64;
    let preferenceId: number;

    it('must insert a preference', async (done) => {
        let res = await request(app)
            .post(url)
            .send({
                deporte: 'test derpote',
                deportista: 'test deportista',
                idUser: userId,
            });

        expect(res.status).toBe(200);
		preferenceId = res.body.id;
		done()
    });

    it('must get list of preferences', async (done) => {
        let res = await request(app).get(url + userId);
        expect(res.status).toBe(200);
        expect(res.body).not.toHaveLength(0);
		expect(res.body[0].id).not.toBeNull();
		done()
    });

    it('must update a preference', async (done) => {
		let update = {
			deporte: 'test derpote',
			deportista: 'test deportista',
			idPreferencia: preferenceId,
		}
        let res = await request(app)
            .put(url)
            // .set('token', user_token)
            .send(update);
		expect(res.status).toBe(200);
		done()
    });

    it('must delete a preference', async (done) => {
        let res = await request(app)
            .delete(url+preferenceId)
		// console.log(res.body)
		expect(res.status).toBe(200);
		done()
    });
});

// afterAll(async () => {
// 	// console.log(app)
// 	app = null;
// });
