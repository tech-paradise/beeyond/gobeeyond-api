import { Server } from '../../src/server/Server';
import { Application } from 'express';
import { SERVER_INIT } from '../../src/app';
import request from 'supertest';
import axios from 'axios';
import { notDeepEqual } from 'assert';

require('dotenv').config();
let server: Server;
let app: Application;

beforeAll(async () => {
    server = await SERVER_INIT();
    app = server.getApp();
});

describe('Get products from database', () => {
	const url = '/api/v1/productos/';
	let productId :any = null;

    it('must get a list of products', async (done) => {
        let res = await request(app)
            .get(url);
		expect(res.status).toBe(200);
		expect(res.body).not.toHaveLength(0)
		productId = res.body[0].idProducto
		done()
    });

    it('must get a product info', async (done) => {
		let res = await request(app).get(url + productId);
		expect(productId).not.toBeNull();
		expect(res.status).toBe(200);
		expect(res.body.id).not.toBeNull()
		done()
    });

});

// afterAll(async () => {
// 	// console.log(app)
// 	app = null;
// });
