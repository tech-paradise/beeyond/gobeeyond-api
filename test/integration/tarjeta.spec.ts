import { Server } from '../../src/server/Server';
import { Application } from 'express';
import { SERVER_INIT } from '../../src/app';
import request from 'supertest';
import axios from 'axios';
import { API_V1, CARDS, LOGIN, USERS } from '../../src/routes/RoutesNames';

require('dotenv').config();
let server: Server;
let app: Application;

beforeAll(async () => {
    server = await SERVER_INIT();
    app = server.getApp();
});

describe('CRUD A card: ', () => {
    const url = API_V1 + CARDS;
    const urlLogin = API_V1 + USERS + LOGIN;
    let tarjetaId: any = null;
    let card_token: any = null;
    let userToken: string = '';
    let idDeviceSession = 'kR1MiQhz2otdIuUlQkbEyitIqVMiI16f';
    let card = {
        card_number: '4111111111111111',
        holder_name: 'Juan Perez Ramirez',
        expiration_year: '20',
        expiration_month: '12',
        cvv2: '110',
        address: {
            city: 'Querétaro',
            line3: 'Queretaro',
            postal_code: '76900',
            line1: 'Av 5 de Febrero',
            line2: 'Roble 207',
            state: 'Queretaro',
            country_code: 'MX',
        },
    };

    it('should login an user ', async () => {
        const res = await request(app)
            .post(urlLogin)
            .send({
                email: 'test_kevin@mocha2.com',
                password: 'Passwordtest1',
            })
            .set('Accept', 'application/json')
            .set('Content-Type', 'application/x-www-form-urlencoded');

        expect(res.body).toHaveProperty('token');
        userToken = res.body['token'];
    });

    it('should encript a card', async () => {
        let res = await axios.request({
            url:
                'https://sandbox-api.openpay.mx/v1/' +
                process.env.OPENPAY_ID +
                '/tokens',
            method: 'POST',
            data: card,
            auth: {
                username: process.env.OPENPAY_PUBLIC
                    ? process.env.OPENPAY_PUBLIC
                    : '',
                password: '',
            },
        });
        expect(res.status).toBe(201);
        card_token = res.data.id;
        // console.log('OpnToken: ', card_token);
    });

    it('should insert a card', async () => {
        let data = {
            token: card_token,
            idDeviceSession: idDeviceSession,
        };

        let res = await request(app)
            .post(url)
            .send(data)
            .set('Authorization', userToken)
            .set('Accept', 'application/json')
            .set('Content-Type', 'application/x-www-form-urlencoded');
        expect(res.status).toBe(200);
        expect(res.body).toHaveProperty('id');
        tarjetaId = res.body.id;
    });

    it('should list a card', async () => {
        let res = await request(app)
            .get(url)
            .set('Accept', 'application/json')
            .set('Authorization', userToken);
        expect(res.status).toBe(200);
        expect(res.body).not.toHaveLength(0);

		console.log('List of: ' + res.body.length + ' cards');
    });

    it('should delete a card using the token', async () => {
        let res = await request(app)
            .delete(url + '/' + tarjetaId)
            .set('Accept', 'application/json')
            .set('Authorization', userToken);

        expect(res.status).toBe(200);
    });
});
