import { SERVER_INIT } from '../../src/app';
import request from 'supertest';
import { Application } from 'express';
import { Server } from '../../src/server/Server';

let server: Server;
let app: Application;

beforeAll(async () => {
    server = await SERVER_INIT();
    app = server.getApp();
});

describe('Ticket USE CASES: ', () => {
    const url = '/api/v1/tickets/';
    const ticketId = 1;

    it('Should get ticket root', async (done) => {
        const res = await request(app).get(url);
		expect(res.status).toBe(200);
		done()
    });

    it('Should get a ticket info', async (done) => {
        const res = await request(app).get(url + ticketId);
		expect(res.status).toBe(200);
		done()	
    });
});
