import {SERVER_INIT} from "../../src/app";
import request from "supertest";
import {Application} from 'express'
import {Server} from "../../src/server/Server";
import {LOGIN, USERS, API_V1, USER_INFO, USER_ACCOUNT} from "../../src/routes/RoutesNames";

let server : Server;
let app: Application;

beforeAll(async ()=>{
    server = await SERVER_INIT();
    app = server.getApp();
});



describe('Client USE CASES: ', () => {
    const urlUser = API_V1+USERS;
    const urlLogin = urlUser+LOGIN;

    let token: string;
    const userDummy = {
        nombreCliente: 'Mocha',
        primerApel: 'Chia',
        segundoApel: 'Test',
        alias: 'TestUse2',
        fechaNac: '2020-03-14',
        correoElectronico: 'test@mocha2.com',
        telCel: '12345678',
        password: 'Passwordtest1',
        pin: 1234,
    };

    it('should not exist alias', async () => {
        const res = await request(app)
            .get(urlUser + '/alias-exist/');
            expect(res.status).toBe(404);
    });

    it('should insert an user', async () => {
        const res = await request(app).post(urlUser)
            .send(userDummy)
            .set('Accept', 'application/json')
            .set('Content-Type','application/x-www-form-urlencoded');

        expect(res.status).not.toBe(500);
        expect(res.status).not.toBe(202);
    });

    it('should fail while inserting a repited user', async () => {
        const res = await request(app).post(urlUser)
            .send(userDummy)
            .set('Accept', 'application/json')
            .set('Content-Type','application/x-www-form-urlencoded');

        expect(res.status).toBe(200);
    });

    it('should login an user ', async () => {
        const res = await request(app).post(urlLogin)
            .send({
                email: userDummy.correoElectronico,
                password: userDummy.password
            })
            .set('Accept', 'application/json')
            .set('Content-Type','application/x-www-form-urlencoded');

        expect(res.body).toHaveProperty('token');
        token = res.body['token'];
    });

    it('should get an user´s info', async () => {
        const res = await request(app).get(urlUser)
            .set('Authorization',token);
        expect(res.body).toHaveProperty('userInfo.nombre');
    });


    it('should update user´s app data', async () => {
        const res = await request(app).put(urlUser)
            .send({
                experiencia:'4',
                bloqueado:false
            })
            .set('Authorization',token)
            .set('Accept', 'application/json')
            .set('Content-Type','application/x-www-form-urlencoded');

        expect(res.status).toBe(200);
        expect(Number(res.body.data.experiencia)).toBeGreaterThanOrEqual(4)
    });

    it('should update user´s info data ', async () => {
        const res = await request(app).put(urlUser + USER_INFO)
            .send({
                nombreCliente: 'Jest',
                primerApel: 'Supertest',
                segundoApel: 'Test',
                fechaNac: userDummy.fechaNac,
                telCel: userDummy.telCel,
                pais: 'Mexico',
                ciudad: 'Cancun',
                ocupacion: 'Ser crack',
            })
            .set('Authorization',token)
            .set('Accept', 'application/json')
            .set('Content-Type','application/x-www-form-urlencoded');

        expect(res.body.data.nombreCliente).toBe('Jest');
    });

    it('should update user´s account data ', async () => {
        const res = await request(app).put(urlUser + USER_ACCOUNT)
            .send({
                alias: 'jhiu7915',
                telCel: '9876543210',
                password: 'hello123',
                oldPassword: 'Passwordtest1',
                pin: '1234',
                correoElectronico: 'use8rEmail15@mail.com'
            })
            .set('Authorization',token)
            .set('Accept', 'application/json')
            .set('Content-Type','application/x-www-form-urlencoded');

        expect(res.body.data.alias).toBe('jhiu7915');
    });
});
